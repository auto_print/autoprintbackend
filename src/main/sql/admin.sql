INSERT INTO `admins_privileges` (`id`, `name`, `last_updated`, `date_created`) VALUES (NULL, 'ADMIN_CREATE', current_timestamp(), current_timestamp());
INSERT INTO `admins_privileges` (`id`, `name`, `last_updated`, `date_created`) VALUES (NULL, 'ADMIN_UPDATE', current_timestamp(), current_timestamp());
INSERT INTO `admins_privileges` (`id`, `name`, `last_updated`, `date_created`) VALUES (NULL, 'ADMIN_READ', current_timestamp(), current_timestamp());

INSERT INTO `admins_roles` (`id`, `name`, `last_updated`, `date_created`) VALUES (NULL, 'APPS', current_timestamp(), current_timestamp());
INSERT INTO `admins_roles` (`id`, `name`, `last_updated`, `date_created`) VALUES (NULL, 'BASE', current_timestamp(), current_timestamp());

INSERT INTO `admins_privileges_privileges` (`role_id`, `privilege_id`) VALUES ('1', '7');
INSERT INTO `admins_privileges_privileges` (`role_id`, `privilege_id`) VALUES ('1', '8');
INSERT INTO `admins_privileges_privileges` (`role_id`, `privilege_id`) VALUES ('1', '9');

INSERT INTO `admins_roles_roles` (`admin_id`, `role_id`) VALUES ('4', '3');