-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2021 at 05:43 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autoprint`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `reference_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `custom_id` bigint(20) DEFAULT NULL,
  `resolve` bit(1) DEFAULT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `token`, `status`, `last_updated`, `date_created`) VALUES
(1, 'superadmin', '5fd161dc208e46e1c5e9bfd2c139ef10f8ea318f2900a1204323559dc95e4ca4', 'superadmin_daad7933f1b5e3154eb11b1fe22eb07ce1f9f20dfc97a4c3f94ef18af3abb5b3', 'ACTIVE', '2020-12-07 17:33:14', '2020-06-05 10:11:00'),
(2, 'landak', 'e1c28bd7c754496545229902d43a2545de0dbddcb5474964589aa4fa2d0b470e', 'superadmin_c9f27176cb54158c714827d712cd131a46dcfc19501962b0effd3189045ffd09', 'ACTIVE', '2020-06-05 10:11:32', '2020-06-05 10:11:32'),
(3, 'web', '5fd161dc208e46e1c5e9bfd2c139ef10f8ea318f2900a1204323559dc95e4ca4', 'superadmin_576a0a1284a853b56f50e876273cdadbfd8ec13408223f1492617a75399921c4', 'ACTIVE', '2020-06-05 10:12:26', '2020-06-05 10:12:26'),
(4, 'app', '5fd161dc208e46e1c5e9bfd2c139ef10f8ea318f2900a1204323559dc95e4ca4', 'superadmin_0827c36a86463aba469bc76fd25e6a8a663fce20c02f35f8c9afd6410ef7c177', 'ACTIVE', '2020-06-05 17:17:39', '2020-06-05 17:17:39');

-- --------------------------------------------------------

--
-- Table structure for table `admins_privileges`
--

CREATE TABLE `admins_privileges` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins_privileges`
--

INSERT INTO `admins_privileges` (`id`, `name`, `last_updated`, `date_created`) VALUES
(1, 'USER_CREATE', '2020-06-05 18:31:46', '2020-06-05 18:31:46'),
(2, 'USER_UPDATE', '2020-06-05 18:31:56', '2020-06-05 18:31:56'),
(3, 'VENDOR_CREATE', '2020-06-06 00:28:42', '2020-06-06 00:28:42'),
(4, 'VENDOR_UPDATE', '2020-06-06 00:28:58', '2020-06-06 00:28:58'),
(5, 'USER_READ', '2020-06-05 18:31:56', '2020-06-05 18:31:56'),
(6, 'VENDOR_READ', '2020-06-05 18:31:56', '2020-06-05 18:31:56'),
(7, 'ADMIN_CREATE', '2020-12-07 22:51:53', '2020-12-07 22:51:53'),
(8, 'ADMIN_UPDATE', '2020-12-07 22:53:45', '2020-12-07 22:53:45'),
(9, 'ADMIN_READ', '2020-12-07 22:53:45', '2020-12-07 22:53:45'),
(10, 'ORDER_CREATE', '2020-06-06 00:28:58', '2020-06-06 00:28:58'),
(11, 'ORDER_UPDATE', '2020-06-07 05:19:54', '2020-06-07 05:19:54'),
(12, 'ORDER_READ', '2020-06-07 05:20:02', '2020-06-07 05:20:02'),
(13, 'ACTIVITY_CREATE', '2020-06-06 00:28:58', '2020-06-06 00:28:58'),
(14, 'ACTIVITY_UPDATE', '2020-06-07 05:19:54', '2020-06-07 05:19:54'),
(15, 'ACTIVITY_READ', '2020-06-07 05:20:02', '2020-06-07 05:20:02');

-- --------------------------------------------------------

--
-- Table structure for table `admins_privileges_privileges`
--

CREATE TABLE `admins_privileges_privileges` (
  `role_id` bigint(20) NOT NULL,
  `privilege_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins_privileges_privileges`
--

INSERT INTO `admins_privileges_privileges` (`role_id`, `privilege_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(2, 1),
(2, 3),
(2, 5),
(2, 6),
(4, 5),
(4, 6),
(4, 9),
(4, 12),
(4, 15),
(5, 9);

-- --------------------------------------------------------

--
-- Table structure for table `admins_roles`
--

CREATE TABLE `admins_roles` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins_roles`
--

INSERT INTO `admins_roles` (`id`, `name`, `last_updated`, `date_created`) VALUES
(1, 'SUPER_ADMIN', '2020-06-05 18:32:33', '2020-06-05 18:32:33'),
(2, 'WEB', '2020-06-05 18:32:53', '2020-06-05 18:32:53'),
(3, 'APPS', '2020-12-07 22:52:46', '2020-12-07 22:52:46'),
(4, 'BASE', '2020-12-07 22:53:15', '2020-12-07 22:53:15'),
(5, 'TESTER', '2021-01-06 18:00:27', '2021-01-06 18:00:27'),
(7, '123', '2021-01-07 18:01:00', '2021-01-07 18:01:00');

-- --------------------------------------------------------

--
-- Table structure for table `admins_roles_roles`
--

CREATE TABLE `admins_roles_roles` (
  `admin_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins_roles_roles`
--

INSERT INTO `admins_roles_roles` (`admin_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(2, 2),
(3, 2),
(4, 3),
(10, 4),
(10, 5),
(14, 7);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` bigint(20) NOT NULL,
  `ezeep_document_id` varchar(255) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filetype` varchar(255) NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `document_properties`
--

CREATE TABLE `document_properties` (
  `id` bigint(20) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `colour` varchar(255) NOT NULL,
  `duplex` varchar(255) NOT NULL,
  `page_orientation` varchar(255) NOT NULL,
  `pages` bigint(20) NOT NULL,
  `copies` bigint(20) NOT NULL,
  `cover_type` varchar(255) DEFAULT NULL,
  `cover_colour` varchar(255) DEFAULT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `reference_id` varchar(255) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_item`
--

CREATE TABLE `invoice_item` (
  `id` bigint(20) NOT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `document_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `reference_id` varchar(255) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `vendor_id` bigint(20) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `printer`
--

CREATE TABLE `printer` (
  `id` bigint(20) NOT NULL,
  `ezeep_printer_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `print_job`
--

CREATE TABLE `print_job` (
  `id` bigint(20) NOT NULL,
  `ezeep_print_job_id` varchar(255) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `printer_id` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `last_updated`, `date_created`) VALUES
(1, 'GST', '2020-12-26 16:11:54', '2020-12-26 16:11:54'),
(2, 'Service Charge', '2020-12-26 16:11:54', '2020-12-26 16:11:54'),
(100, 'Auto Colour', '2020-10-05 23:54:52', '2020-10-05 23:54:52'),
(101, 'Black White', '2020-10-05 23:54:52', '2020-10-05 23:54:52');

-- --------------------------------------------------------

--
-- Table structure for table `product_vendor`
--

CREATE TABLE `product_vendor` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `active` bit(1) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `avatar_ref` varchar(255) DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `is_vendor` bit(1) NOT NULL,
  `active` bit(1) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `vendorname` varchar(255) NOT NULL,
  `active` bit(1) NOT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins_privileges`
--
ALTER TABLE `admins_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins_privileges_privileges`
--
ALTER TABLE `admins_privileges_privileges`
  ADD PRIMARY KEY (`role_id`,`privilege_id`) USING BTREE;

--
-- Indexes for table `admins_roles`
--
ALTER TABLE `admins_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins_roles_roles`
--
ALTER TABLE `admins_roles_roles`
  ADD PRIMARY KEY (`admin_id`,`role_id`) USING BTREE;

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_properties`
--
ALTER TABLE `document_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_item`
--
ALTER TABLE `invoice_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `printer`
--
ALTER TABLE `printer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `print_job`
--
ALTER TABLE `print_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_vendor`
--
ALTER TABLE `product_vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `admins_privileges`
--
ALTER TABLE `admins_privileges`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `admins_roles`
--
ALTER TABLE `admins_roles`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document_properties`
--
ALTER TABLE `document_properties`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_item`
--
ALTER TABLE `invoice_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `printer`
--
ALTER TABLE `printer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `print_job`
--
ALTER TABLE `print_job`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `product_vendor`
--
ALTER TABLE `product_vendor`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

INSERT INTO `user` (`id`, `username`, `password`, `avatar_ref`, `token`, `fullname`, `email`, `phone_number`, `is_vendor`, `active`, `last_updated`, `date_created`) VALUES
 (1, 'autoprint', '825e6ed1e7e4b00bc9132c5c541ba37511b2334bbf5e157934c221e31157d20a', NULL, 'VENDOR_2b992e2d634f784ec4afacadb3a61141e931c4590dbb4cb711a09787821ef68b', 'Autoprint', 'autoprint.noreply@gmail.com', NULL, b'1', b'1', '2020-12-29 17:09:09', '2020-12-29 17:09:09');

INSERT INTO `wallet` (`id`, `user_id`, `amount`, `last_updated`, `date_created`) VALUES (1, '1', '0.00', '2020-12-30 01:10:20', '2020-12-30 01:10:20');

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

