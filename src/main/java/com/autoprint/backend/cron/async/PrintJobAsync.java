package com.autoprint.backend.cron.async;

import com.autoprint.backend.entity.Document;
import com.autoprint.backend.entity.PrintJob;
import com.autoprint.backend.entity.SystemUser;
import com.autoprint.backend.services.PrintJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class PrintJobAsync {

    @Lazy
    @Autowired
    private PrintJobService printJobService;

    @Async
    public void createPrintJob(Document document) {
        SystemUser.getInstance().runAs();
        PrintJob printJob = new PrintJob();
        printJob.setDocument(document);
        printJobService.create(printJob);
    }

    @Async
    public void cancelPrintJob(Document document) {
        printJobService.cancelJob(document.getId());
    }
}
