package com.autoprint.backend.cron.async;

import com.autoprint.backend.entity.Document;
import com.autoprint.backend.helper.UploadHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class DocumentAsync {

    @Autowired
    private UploadHelper uploadHelper;

    @Async
    public void uploadDocument(Document document) throws Exception {
        uploadHelper.upload(document.getData(), "order/" + document.getOrder().getId(), document.getFilename(), true);
    }
}
