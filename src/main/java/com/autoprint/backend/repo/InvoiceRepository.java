package com.autoprint.backend.repo;

import com.autoprint.backend.entity.Invoice;
import com.autoprint.backend.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    //Page<Invoice> findByUser(User user, Pageable pageable);
}
