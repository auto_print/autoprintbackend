package com.autoprint.backend.repo;

import com.autoprint.backend.entity.AdminRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRoleRepository extends JpaRepository<AdminRole, Long> {

    boolean existsByName(String name);

    AdminRole findByName(String name);

}
