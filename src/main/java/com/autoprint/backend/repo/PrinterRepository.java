package com.autoprint.backend.repo;

import com.autoprint.backend.entity.Document;
import com.autoprint.backend.entity.Printer;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.entity.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PrinterRepository extends JpaRepository<Printer, Long> {

    Page<Printer> findByVendor(Vendor vendor, Pageable pageable);

    List<Printer> findByVendorAndStatus(Vendor vendor, Printer.Status status);
}
