package com.autoprint.backend.repo;

import com.autoprint.backend.entity.DocumentProperties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentPropertiesRepository extends JpaRepository<DocumentProperties, Long> {
}
