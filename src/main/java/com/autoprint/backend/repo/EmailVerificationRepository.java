package com.autoprint.backend.repo;

import com.autoprint.backend.entity.EmailVerification;
import com.autoprint.backend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailVerificationRepository extends JpaRepository<EmailVerification, Long> {


    EmailVerification findFirstByCodeAndVerifiedOrderByDateCreatedDesc(String code, Boolean verified);

    EmailVerification findFirstByUserOrderByDateCreatedDesc(User user);

    // user for testing only
    EmailVerification findFirstByUserAndVerifiedOrderByDateCreatedDesc(User user, Boolean verified);
}
