package com.autoprint.backend.repo.specification;

import com.autoprint.backend.core.entity.IEntity;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.autoprint.backend.repo.specification.SearchCriteria.*;
import static com.google.common.base.Preconditions.*;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class BaseEntitySpecification<E extends IEntity> implements Specification<E> {

    private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private final List<SearchCriteria> criterias = new ArrayList<>();
    private Predicate predicate = null;

    public void addCriteria(SearchCriteria criteria) {
        criterias.add(criteria);
    }

    private Predicate toPredicate(Predicate predicate, SearchCriteria searchCriteria, CriteriaBuilder builder) {
        if (this.predicate != null) {
            if (searchCriteria.isConjunction()) {
                return builder.and(this.predicate, predicate);
            } else {
                return builder.or(this.predicate, predicate);
            }
        }
        return predicate;
    }

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        for (SearchCriteria criteria : criterias) {
            checkArgument(isNotBlank(criteria.getKey()), "key cannot be blank");
            checkArgument(isNotBlank(criteria.getOperation()), "operation cannot be null");
            checkNotNull(criteria.getValue(), "value cannot be null");

            String key = criteria.getKey();
            Object value = criteria.getValue();
            String[] sub_keys = key.split("\\.");
            Path path = null;
            Join join = null;

            for (int i = 0; i < sub_keys.length; i++) {
                // last
                if (i == sub_keys.length - 1) {
                    path = join == null ? root.get(sub_keys[i]) : join.get(sub_keys[i]);
                } else {
                    join = join == null ? root.join(sub_keys[i]) : join.join(sub_keys[i]);
                }
            }
            assert path != null;

            // TODO: support OffsetDateTime string matching
            // TODO: support Enum object matching.
            // TODO: support Date/LocalDatetime/OffsetDateTime object matching

            if (criteria.getOperation().equalsIgnoreCase(OPERATOR_GREATER)) {
                checkArgument(!isCollection(value), format("Unsupported operation '%s' on value '%s'", criteria.getOperation(), value));

                if (path.getJavaType() == Date.class || path.getJavaType() == LocalDateTime.class) {
                    if (value instanceof String) {
                        predicate = toPredicate(builder.greaterThan(path, LocalDateTime.parse(value.toString(), DATETIME_FORMATTER)), criteria, builder);
                    } else {
                        checkArgument(value instanceof Date || value instanceof LocalDateTime);
                        predicate = toPredicate(builder.greaterThan(path, (Comparable) value), criteria, builder);
                    }
                } else {
                    predicate = toPredicate(builder.greaterThan(path, (Comparable) value), criteria, builder);
                }
            } else if (criteria.getOperation().equalsIgnoreCase(OPERATOR_GREATER_OR_EQUAL)) {
                checkArgument(!isCollection(value), format("Unsupported operation '%s' on value '%s'", criteria.getOperation(), value));

                if (path.getJavaType() == Date.class || path.getJavaType() == LocalDateTime.class) {
                    if (value instanceof String) {
                        predicate = toPredicate(builder.greaterThanOrEqualTo(path, LocalDateTime.parse(value.toString(), DATETIME_FORMATTER)), criteria, builder);
                    } else {
                        checkArgument(value instanceof Date || value instanceof LocalDateTime);
                        predicate = toPredicate(builder.greaterThanOrEqualTo(path, (Comparable) value), criteria, builder);
                    }
                } else {
                    predicate = toPredicate(builder.greaterThanOrEqualTo(path, (Comparable) value), criteria, builder);
                }
            } else if (criteria.getOperation().equalsIgnoreCase(OPERATOR_LESSER)) {
                checkArgument(!isCollection(value), format("Unsupported operation '%s' on value '%s'", criteria.getOperation(), value));

                if (path.getJavaType() == Date.class || path.getJavaType() == LocalDateTime.class) {
                    if (value instanceof String) {
                        predicate = toPredicate(builder.lessThan(path, LocalDateTime.parse(value.toString(), DATETIME_FORMATTER)), criteria, builder);
                    } else {
                        checkArgument(value instanceof Date || value instanceof LocalDateTime);
                        predicate = toPredicate(builder.lessThan(path, (Comparable) value), criteria, builder);
                    }
                } else {
                    predicate = builder.lessThan(path, (Comparable) value);
                }
            } else if (criteria.getOperation().equalsIgnoreCase(OPERATOR_LESSER_OR_EQUAL)) {
                checkArgument(!isCollection(value), format("Unsupported operation '%s' on value '%s'", criteria.getOperation(), value));

                if (path.getJavaType() == Date.class || path.getJavaType() == LocalDateTime.class) {
                    if (value instanceof String) {
                        predicate = toPredicate(builder.lessThanOrEqualTo(path, LocalDateTime.parse(value.toString(), DATETIME_FORMATTER)), criteria, builder);
                    } else {
                        checkArgument(value instanceof Date || value instanceof LocalDateTime);
                        predicate = toPredicate(builder.lessThanOrEqualTo(path, (Comparable) value), criteria, builder);
                    }
                } else {
                    predicate = toPredicate(builder.lessThanOrEqualTo(path, (Comparable) value), criteria, builder);
                }
            } else if (criteria.getOperation().equalsIgnoreCase(OPERATOR_RANGE)) {
                if (isCollection(value)) {
                    Object[] objects = toArray(value);
                    checkArgument(objects.length == 2, "Collection must contain only 2 elements");
                    // TODO: not sure if the date working
                    if (objects[0] instanceof Number || objects[0] instanceof Date) {
                        predicate = toPredicate(builder.between(path, (Comparable) objects[0], (Comparable) objects[1]), criteria, builder);
                    } else if (path.getJavaType() == Date.class || path.getJavaType() == LocalDateTime.class) {
                        if (objects[0] instanceof String) {
                            predicate = toPredicate(builder.between(path,
                                    LocalDateTime.parse(objects[0].toString(), DATETIME_FORMATTER),
                                    LocalDateTime.parse(objects[1].toString(), DATETIME_FORMATTER)), criteria, builder);
                        } else {
                            checkArgument(objects[0] instanceof Date || objects[1] instanceof LocalDateTime);
                            predicate = toPredicate(builder.between(path, (Comparable) objects[0], (Comparable) objects[1]), criteria, builder);
                        }
                    } else {
                        throw new UnsupportedOperationException(format("Unsupported operation '%s' on value '%s'", criteria.getOperation(), value));
                    }
                } else if (path.getJavaType() == String.class) {
                    predicate = toPredicate(builder.like(path, "%" + criteria.getValue() + "%"), criteria, builder);
                } else {
                    throw new UnsupportedOperationException(format("Unsupported operation '%s' on value '%s'", criteria.getOperation(), value));
                }
            } else if (criteria.getOperation().equals(OPERATOR_EQUALS)) {
                checkArgument(path.getJavaType() != LocalDateTime.class, format("Unsupported type '%s' operation '%s' on value '%s'",
                        LocalDateTime.class, criteria.getOperation(), value));
                checkArgument(path.getJavaType() != OffsetDateTime.class, format("Unsupported type '%s' operation '%s' on value '%s'",
                        OffsetDateTime.class, criteria.getOperation(), value));

                if (isCollection(value)) {
                    Object[] objects = toArray(value);
                    if (path.getJavaType().isEnum()) {
                        objects = getEnums(path, objects);
                    }

                    CriteriaBuilder.In<Object> in = builder.in(path);
                    for (Object obj : objects) {
                        in.value(obj);
                    }

                    predicate = toPredicate(in, criteria, builder);
                } else if (path.getJavaType().isEnum()) {
                    predicate = toPredicate(builder.equal(path, getEnum(path, value)), criteria, builder);
                } else {
                    if (criteria.getValue() instanceof String) {
                        predicate = toPredicate(builder.like(path, criteria.getValue() + ""), criteria, builder);
                    } else {
                        predicate = toPredicate(builder.equal(path, criteria.getValue()), criteria, builder);
                    }
                }
            } else if (criteria.getOperation().equals(OPERATOR_NOT_EQUALS)) {
                checkArgument(path.getJavaType() != LocalDateTime.class, format("Unsupported type '%s' operation '%s' on value '%s'",
                        LocalDateTime.class, criteria.getOperation(), value));
                checkArgument(path.getJavaType() != OffsetDateTime.class, format("Unsupported type '%s' operation '%s' on value '%s'",
                        OffsetDateTime.class, criteria.getOperation(), value));

                if (isCollection(value)) {
                    Object[] objects = toArray(value);
                    if (path.getJavaType().isEnum()) {
                        objects = getEnums(path, objects);
                    }

                    CriteriaBuilder.In<Object> in = builder.in(path);
                    for (Object obj : objects) {
                        in.value(obj);
                    }

                    predicate = toPredicate(in.not(), criteria, builder);
                } else if (path.getJavaType().isEnum()) {
                    predicate = toPredicate(builder.notEqual(path, getEnum(path, value)), criteria, builder);
                } else {
                    if (criteria.getValue() instanceof String) {
                        predicate = toPredicate(builder.notLike(path, criteria.getValue() + ""), criteria, builder);
                    } else {
                        predicate = toPredicate(builder.notEqual(path, criteria.getValue()), criteria, builder);
                    }
                }
            } else if (criteria.getOperation().equals(OPERATOR_LIKE)) {
                checkArgument(path.getJavaType() != LocalDateTime.class, format("Unsupported type '%s' operation '%s' on value '%s'",
                        LocalDateTime.class, criteria.getOperation(), value));
                checkArgument(path.getJavaType() != OffsetDateTime.class, format("Unsupported type '%s' operation '%s' on value '%s'",
                        OffsetDateTime.class, criteria.getOperation(), value));

                if (isCollection(value)) {
                    Object[] objects = toArray(value);
                    if (path.getJavaType().isEnum()) {
                        objects = getEnums(path, objects);
                    }

                    CriteriaBuilder.In<Object> in = builder.in(path);
                    for (Object obj : objects) {
                        in.value(obj);
                    }

                    predicate = toPredicate(in, criteria, builder);
                } else if (path.getJavaType().isEnum()) {
                    predicate = toPredicate(builder.equal(path, getEnum(path, value)), criteria, builder);
                } else {
                    if (criteria.getValue() instanceof String) {
                        predicate = toPredicate(builder.like(path, "%" + criteria.getValue() + "%"), criteria, builder);
                    } else {
                        predicate = toPredicate(builder.equal(path, criteria.getValue()), criteria, builder);
                    }
                }
            }
        }

        return predicate;
    }

    private Enum[] getEnums(Path path, Object value) {
        checkState(path.getJavaType().isEnum(), format("Expected value '%s' to be a enum type", value));
        Object[] objects = toArray(value);
        Enum[] enums = new Enum[objects.length];

        for (int i = 0; i < objects.length; i++) {
            enums[i] = getEnum(path, objects[i]);
        }

        return enums;
    }

    private Enum getEnum(Path path, Object value) {
        checkState(path.getJavaType().isEnum(), format("Expected value '%s' to be a enum type", value));

        return (Enum) Arrays.asList(path.getJavaType().getEnumConstants()).stream()
                .filter(o -> o.toString().equals(value.toString()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(format("No matching enum value for %s'", value)));
    }

    private Predicate convertCollection(Object[] values, CriteriaBuilder builder, Path path) {
        CriteriaBuilder.In<Object> in = builder.in(path);

        for (Object obj : values) {
            in.value(obj);
        }

        return in;
    }

    private Object[] toArray(Object value) {
        checkState(isCollection(value), format("Expected value '%s' to be collection type", value));
        Object[] objects;

        // Convert to arrays of objects
        if (value instanceof Collection) {
            objects = ((Collection) value).toArray(new Object[0]);
        } else if (value.getClass().isArray()) {
            objects = (Object[]) value;
        } else {
            throw new UnsupportedOperationException();
        }

        return objects;
    }

    private boolean isCollection(Object value) {
        checkNotNull(value, "Value to be null");

        if (value instanceof Collection) {
            checkArgument(!((Collection) value).isEmpty(), "Collection cannot be empty");
            return true;
        } else if (value.getClass().isArray()) {
            checkArgument(((Object[]) value).length > 0, "Array cannot be empty");
            return true;
        }

        return false;
    }
}
