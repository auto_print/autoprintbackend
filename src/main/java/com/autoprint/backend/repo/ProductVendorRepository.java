package com.autoprint.backend.repo;

import com.autoprint.backend.entity.ProductVendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductVendorRepository extends JpaRepository<ProductVendor, Long> {

    ProductVendor findByProductIdAndVendorIdAndActive(Long productId, Long vendorId, boolean active);

    Page<ProductVendor> findByVendorId(Long vendorId, Pageable pageable);
}
