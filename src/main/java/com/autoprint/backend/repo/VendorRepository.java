package com.autoprint.backend.repo;

import com.autoprint.backend.entity.Product;
import com.autoprint.backend.entity.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, Long> {

    Vendor findByUserId(Long userId);

    Page<Vendor> findAll(Specification<Vendor> spec, Pageable pageable);

    @Query("SELECT DISTINCT v FROM Vendor v JOIN Printer p ON p.vendor = v.id WHERE v.longitude IS NOT NULL AND v.latitude IS NOT NULL")
    List<Vendor> findByHaveAtLeastOnePrinter();

    @Query("SELECT DISTINCT v FROM Vendor v JOIN Printer p ON p.vendor = v.id JOIN ProductVendor pv ON pv.vendor = v.id WHERE v.longitude IS NOT NULL AND v.latitude IS NOT NULL AND pv.product IN (?1)")
    List<Vendor> findByHaveAtLeastOnePrinterAndSupportProduct(List<Product> productList);
}
