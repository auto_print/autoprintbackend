package com.autoprint.backend.repo;

import com.autoprint.backend.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByTokenAndActive(String apiKey, boolean active);

    User findByEmailAndActive(String email, boolean active);

    User findByUsernameAndPasswordAndActive(String username, String password, boolean active);

    User findByUsernameAndActive(String username, boolean active);

    User findByEmailOrUsername(String email, String username);

    User findByUsername(String username);

    User findByEmail(String username);

    Page<User> findAll(Specification<User> spec, Pageable pageable);
}
