package com.autoprint.backend.repo;

import com.autoprint.backend.entity.Order;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.entity.Vendor;
import com.autoprint.backend.object.OrderOverallObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    Page<Order> findByUser(User user, Pageable pageable);

    Page<Order> findByUserAndStatus(User user, Order.Status status, Pageable pageable);

    Page<Order> findByVendor(Vendor vendor, Pageable pageable);

    Page<Order> findByVendorAndStatus(Vendor vendor, Order.Status status, Pageable pageable);

    Page<Order> findAll(Specification<Order> spec, Pageable pageable);

    List<Order> findByUserAndStatusAndDateCreatedBetween(User user, Order.Status status, LocalDateTime oneDayBefore, LocalDateTime now);

    List<Order> findByVendorAndStatusAndDateCreatedBetween(Vendor vendor, Order.Status status, LocalDateTime start, LocalDateTime end);

    @Query("SELECT COUNT(id) FROM Order WHERE status = ?1")
    Long countOrder(Order.Status status);

    @Query("SELECT COUNT(id) FROM Order WHERE dateCreated > ?1 AND status = ?2")
    Long countOrderByThisMonth(LocalDateTime c, Order.Status status);

    @Query("SELECT COUNT(id) FROM Order WHERE dateCreated > ?1 AND dateCreated < ?2 AND status = ?3")
    Long countOrderByLastMonths(LocalDateTime lastMonth, LocalDateTime nowMonth, Order.Status status);

    @Query("SELECT new com.autoprint.backend.object.OrderOverallObject(dateCreated, COUNT(id)) FROM Order " +
            "WHERE status = ?1 AND dateCreated BETWEEN ?2 AND ?3 " +
            "GROUP BY YEAR(dateCreated), MONTH(dateCreated)")
    List<OrderOverallObject> getOverallStatistics(Order.Status status, LocalDateTime lastYear, LocalDateTime now);

    @Query("SELECT new com.autoprint.backend.object.OrderOverallObject(dateCreated) FROM Order " +
            "WHERE vendor = ?1 AND status = ?2 " +
            "GROUP BY YEAR(dateCreated), MONTH(dateCreated)")
    List<OrderOverallObject> getVendorOrderStatistic(Vendor vendor, Order.Status status);
}
