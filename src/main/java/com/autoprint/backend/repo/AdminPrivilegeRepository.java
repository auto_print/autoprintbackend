package com.autoprint.backend.repo;

import com.autoprint.backend.entity.AdminPrivilege;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdminPrivilegeRepository extends JpaRepository<AdminPrivilege, Long> {

    Optional<AdminPrivilege> findByName(String name);

    @Query("SELECT p FROM AdminPrivilege p WHERE p.name LIKE ?1")
    Page<AdminPrivilege> findByNameLike(String name, Pageable pageable);
}
