package com.autoprint.backend.repo;

import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long> {

    Admin findByUsernameAndPasswordAndStatus(String username, String password, Admin.Status status);

    Admin findByUsername(String name);

    Admin findByUsernameAndStatus(String username, Admin.Status status);

    Admin findByTokenAndStatus(String token, Admin.Status status);

    Page<Admin> findAll(Specification<Admin> spec, Pageable pageable);
}
