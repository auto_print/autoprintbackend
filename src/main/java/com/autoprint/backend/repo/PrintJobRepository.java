package com.autoprint.backend.repo;

import com.autoprint.backend.entity.PrintJob;
import com.autoprint.backend.entity.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrintJobRepository extends JpaRepository<PrintJob, Long> {
    Page<PrintJob> findByVendor(Vendor vendor, Pageable pageable);

    PrintJob findByDocumentId(Long documentId);
}
