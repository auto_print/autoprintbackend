package com.autoprint.backend.repo;

import com.autoprint.backend.entity.Document;
import com.autoprint.backend.entity.Order;
import com.autoprint.backend.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

    Page<Document> findByUser(User user, Pageable pageable);

    Page<Document> findByOrderAndActive(Order order, boolean active, Pageable pageable);

    List<Document> findByOrderIdAndActive(Long orderId, boolean active);
}
