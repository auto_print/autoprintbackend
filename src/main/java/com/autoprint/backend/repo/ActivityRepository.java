package com.autoprint.backend.repo;

import com.autoprint.backend.entity.Activity;
import com.autoprint.backend.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

    Page<Activity> findByUserIdAndType(Long userId, Activity.Type type, Pageable pageable);

    Page<Activity> findAll(Specification specification, Pageable pageable);

    @Query("SELECT COUNT(id) FROM Activity WHERE dateCreated > ?1 AND dateCreated < ?2 AND type = ?3")
    Long countReportByLastMonths(LocalDateTime lastMonth, LocalDateTime nowMonth, Activity.Type type);

    @Query("SELECT COUNT(id) FROM Activity WHERE resolve = false AND type = ?1")
    Long countresolveReport(Activity.Type type);

}
