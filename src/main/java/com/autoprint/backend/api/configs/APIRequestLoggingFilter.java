package com.autoprint.backend.api.configs;

import com.autoprint.backend.api.security.UserPrincipal;
import com.autoprint.backend.core.security.UserAware;
import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.util.UUID;

import static java.time.Duration.between;
import static org.apache.commons.lang3.StringUtils.EMPTY;

public class APIRequestLoggingFilter extends CommonsRequestLoggingFilter {
    private static final ThreadLocal<Instant> tracker = new ThreadLocal<>();
    private static final Logger logger = LoggerFactory.getLogger(APIRequestLoggingFilter.class);

    /**
     * Unlimited payload length. We don't expect this being invoked.
     *
     * @see #getMessagePayload(HttpServletRequest)
     */
    @Override
    protected int getMaxPayloadLength() {
        throw new UnsupportedOperationException();
    }

    /**
     * Unlimited payload length. We don't expect this being invoked.
     *
     * @see #getMessagePayload(HttpServletRequest)
     */
    @Override
    public void setMaxPayloadLength(int maxPayloadLength) {
        throw new UnsupportedOperationException();
    }

    /**
     * Reason to override:
     * 1. set response status
     * 2. use ContentCachingRequestWrapper with no contentCacheLimit
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        boolean isFirstRequest = !this.isAsyncDispatch(request);

        HttpServletRequest requestToUse = request;
        if (this.isIncludePayload() && isFirstRequest && !(request instanceof ContentCachingRequestWrapper)) {
            requestToUse = new ContentCachingRequestWrapper(request);
        }

        boolean shouldLog = this.shouldLog(requestToUse);
        if (shouldLog && isFirstRequest) {
            beforeRequest(requestToUse, "");
        }

        try {
            filterChain.doFilter(requestToUse, response);
        } finally {
            MDC.put("status", ((Integer) response.getStatus()).toString());
            if (shouldLog && !this.isAsyncStarted(requestToUse)) {
                afterRequest(requestToUse, "");
            }
        }
    }

    /**
     * Note: it's intended not to log during this execution.
     */
    @Override
    protected void beforeRequest(HttpServletRequest request, String message) {
        if (this.shouldLog(request)) {
            String uid = "anonymousUser";
            String role = "nobody";
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.getPrincipal() != null) {
                if (authentication.getPrincipal() instanceof UserPrincipal) {
                    UserAware userRoleAware = ((UserPrincipal) authentication.getPrincipal()).getUser();
                    uid = userRoleAware.getId().toString();
                    role = userRoleAware.isSuper() ? "admin" : "user";
                } else {
                    /**
                     * For public API or endpoint, authentication object will be AnonymousAuthenticationToken. In such case, uid will be set to anonymousUser instead.
                     * For any other type of authentication object, we will set it as *.toString() for now.
                     */
                    uid = authentication.getPrincipal().toString();
                }
            }

            MDC.put("uid", uid);
            MDC.put("role", role);
            MDC.put("req_uuid", UUID.randomUUID().toString());
            MDC.put("method", request.getMethod());
            tracker.set(Instant.now());
        }
    }

    @Override
    protected void afterRequest(HttpServletRequest request, String message) {
        try { // We will clear the MDC at the end of execution
            if (this.shouldLog(request)) {
                MDC.put("time_ms", ((Long) between(tracker.get(), Instant.now()).toMillis()).toString());
                super.afterRequest(request, createMessage(request, "request completed. ", ""));
            }
        } finally {
            MDC.clear();
        }
    }

    /**
     * We override this to create our own payload. Else, AbstractRequestLoggingFilter.getMessagePayload() will
     * only log partial payload (according to the maxPayloadLength) and might lose info!
     */
    @Override
    protected String getMessagePayload(HttpServletRequest request) {
        final ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
        if (wrapper == null) {
            return EMPTY;
        }

        byte[] byteArray = wrapper.getContentAsByteArray();
        if (byteArray == null || byteArray.length == 0) {
            return EMPTY;
        }

        String _payloadString;
        try {
            _payloadString = new String(byteArray, wrapper.getCharacterEncoding());
        } catch (UnsupportedEncodingException var6) {
            return "[unknown]";
        }

        try {
            JsonObject payloadJson = new JsonParser().parse(_payloadString).getAsJsonObject();
            handleJsonObject(payloadJson);
            return new Gson().toJson(payloadJson);
        } catch (JsonParseException e) {
            return _payloadString;
        } catch (IllegalStateException e) {
            return _payloadString;
        } catch (Exception e) {
            return _payloadString;
        }
    }

    private void handleValue(JsonObject object, String key, JsonElement value) {
        if (value.isJsonObject()) {
            handleJsonObject(value.getAsJsonObject());
        } else if (value.isJsonArray()) {
            handleJsonArray(object, key, value.getAsJsonArray());
        } else if (value.isJsonPrimitive()) {
            JsonPrimitive jsonPrimitive = value.getAsJsonPrimitive();
            // Trim string
            if (jsonPrimitive.isString()) {
                if (jsonPrimitive.toString().length() > 50) {
                    object.addProperty(key, jsonPrimitive.toString().substring(0, 50).concat("..."));
                }
            }
        }
    }

    private void handleJsonObject(JsonObject object) {
        object.keySet().forEach(key -> {
            JsonElement value = object.get(key);
            handleValue(object, key, value);
        });
    }

    private void handleJsonArray(JsonObject object, String key, JsonArray array) {
        array.forEach(element -> {
            handleValue(object, key, element);
        });
    }
}
