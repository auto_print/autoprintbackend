package com.autoprint.backend.api.dto;

import java.math.BigDecimal;

public class OrderPreConfirmDto {

    public Long id;
    public BigDecimal amount;
}
