package com.autoprint.backend.api.dto;

import javax.validation.constraints.NotBlank;

public class ForgotPasswordRequestDto {

    @NotBlank
    public String email;
}
