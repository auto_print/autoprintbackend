package com.autoprint.backend.api.dto;

import com.autoprint.backend.object.OrderOverallObject;

import java.util.List;

public class OrderDataDto {

    public Long totalOrderInMonth;
    public Long totalOrderLastMonth;
    public Long biggestCount;
    public Long smallestCount;
    public List<OrderOverallObject> list;
}
