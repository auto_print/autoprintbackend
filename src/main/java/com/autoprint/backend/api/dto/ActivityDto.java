package com.autoprint.backend.api.dto;

import com.autoprint.backend.entity.Activity;

public class ActivityDto {

    public Long id;
    public Long userId;
    public String title;
    public String content;
    public Boolean resolve;
    public Activity.Type type;
    public String referenceId;

}
