package com.autoprint.backend.api.dto;

import com.autoprint.backend.entity.Order;

import java.time.LocalDateTime;
import java.util.List;

public class OrderDto {

    public Long id;
    public Long userId;
    public DocumentDto document;
    public String referenceId;
    public Order.Status status;
    public Double longitude;
    public Double latitude;
    public List<InvoiceDto> invoices;
    public VendorViewDto vendor;
    public LocalDateTime dateCreated;
    public String reason;
}
