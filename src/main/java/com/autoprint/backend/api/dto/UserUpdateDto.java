package com.autoprint.backend.api.dto;

public class UserUpdateDto {

    public String imageBase64;
    public String phoneNumber;
    public String username;
    public String email;
    public String fullname;
    public Boolean active;
}
