package com.autoprint.backend.api.dto;

public class ReportDataDto {

    public Long totalLastMonthReport;
    public Long totalUnresolveReport;

}
