package com.autoprint.backend.api.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class VerifyCodeRequestDto {

    @NotBlank
    public String code;
}
