package com.autoprint.backend.api.dto;

import java.math.BigDecimal;

public class InvoiceItemDto {

    public Long id;
    public OrderDto order;
    public ProductDto product;
    public BigDecimal amount;
}
