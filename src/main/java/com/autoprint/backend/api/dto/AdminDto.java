package com.autoprint.backend.api.dto;

import com.autoprint.backend.entity.Admin;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

public class AdminDto {
    public Long id;
    public Admin.Status status;
    public String username;
    public String password;
    public String token;
    @ApiModelProperty(notes = "Roles")
    public List<AdminRoleDto> roles = new ArrayList<>();
}
