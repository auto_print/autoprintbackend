package com.autoprint.backend.api.dto;

public class StatementRequestDto {

    public Long vendorId;
    public String month;
    public String year;
}
