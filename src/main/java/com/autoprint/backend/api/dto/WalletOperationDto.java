package com.autoprint.backend.api.dto;

import java.math.BigDecimal;

public class WalletOperationDto {

    public BigDecimal amount;
    public String operation;
    public Long addId;
    public Long minusId;
}
