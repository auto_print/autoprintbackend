package com.autoprint.backend.api.dto;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class AdminRoleDto {

    public Long id;
    public String name;
    public Set<AdminDto> users = new HashSet<>();
    public Set<AdminPrivilegeDto> privileges = new HashSet<>();
}
