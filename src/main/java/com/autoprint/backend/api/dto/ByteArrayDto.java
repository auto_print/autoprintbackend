package com.autoprint.backend.api.dto;

public class ByteArrayDto {

    public String content;

    public Integer length;

    public String contentType = "application/pdf";

    public ByteArrayDto() {
    }

    public ByteArrayDto(String content, Integer length) {
        this.content = content;
        this.length = length;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
