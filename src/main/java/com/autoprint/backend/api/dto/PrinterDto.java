package com.autoprint.backend.api.dto;

import com.autoprint.backend.entity.Printer;

public class PrinterDto {

    public Long id;
    public String name;
    public Printer.Status status;
}
