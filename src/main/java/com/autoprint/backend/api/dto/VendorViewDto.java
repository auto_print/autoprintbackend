package com.autoprint.backend.api.dto;

public class VendorViewDto {

    public Long id;
    public String vendorname;
    public Double longitude;
    public Double latitude;
}
