package com.autoprint.backend.api.dto;

import com.autoprint.backend.entity.Printer;

public class PrinterUpdateDto {

    public String name;
    public Printer.Status status;
}
