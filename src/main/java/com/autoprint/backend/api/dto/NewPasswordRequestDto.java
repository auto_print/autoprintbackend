package com.autoprint.backend.api.dto;

import javax.validation.constraints.NotNull;

public class NewPasswordRequestDto {

    @NotNull
    public String newPassword;
}
