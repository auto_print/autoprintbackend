package com.autoprint.backend.api.dto;

import java.math.BigDecimal;

public class WalletDto {

    public Long id;
    public BigDecimal amount;
}
