package com.autoprint.backend.api.dto;

public class VendorCreateDto {

    public String username;
    public String password;
    public String email;
    public String fullname;
    public String vendorname;
    public Double longitude;
    public Double latitude;
}
