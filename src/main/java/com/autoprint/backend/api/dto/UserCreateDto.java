package com.autoprint.backend.api.dto;

public class UserCreateDto {

    public String username;
    public String password;
    public String email;
    public String fullname;
}
