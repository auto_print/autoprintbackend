package com.autoprint.backend.api.dto;

public class DocumentCreateDto {

    public String filename;
    public String data;
    public Long orderId;

    //different section
    public String pages;
    public Long copies;
    public String pageOrientation;
    public String colour;
    public String duplex;

    //extra
    public String coverType;
    public String coverColour;

}
