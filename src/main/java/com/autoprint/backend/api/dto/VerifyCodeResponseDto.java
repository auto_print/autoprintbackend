package com.autoprint.backend.api.dto;

import javax.validation.constraints.NotNull;

public class VerifyCodeResponseDto {

    @NotNull
    public Boolean verified;
    public Long userId;
}
