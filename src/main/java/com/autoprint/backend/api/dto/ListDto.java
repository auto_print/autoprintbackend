package com.autoprint.backend.api.dto;

import com.autoprint.backend.repo.specification.SearchCriteria;

import javax.validation.Valid;
import javax.validation.constraints.Min;

public class ListDto {

    @Valid
    public SearchCriteria[] searchCriterias;
    @Min(0)
    public Integer pageNumber;
    @Min(0)
    public Integer pageSize;
    public String sort;

    public ListDto() {
    }

    public SearchCriteria[] getSearchCriterias() {
        return searchCriterias;
    }

    public void setSearchCriterias(SearchCriteria[] searchCriterias) {
        this.searchCriterias = searchCriterias;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
