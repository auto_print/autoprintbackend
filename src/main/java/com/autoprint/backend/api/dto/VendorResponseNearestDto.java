package com.autoprint.backend.api.dto;

import com.autoprint.backend.entity.Vendor;

import java.util.List;

public class VendorResponseNearestDto {

    public List<VendorDto> list;
}
