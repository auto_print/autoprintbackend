package com.autoprint.backend.api.dto;

public class ActivityResponseDto {

    public Long customId;
    public Long userId;
    public String title;
    public String content;
}
