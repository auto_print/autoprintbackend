package com.autoprint.backend.api.dto;

import com.autoprint.backend.entity.Invoice;
import com.autoprint.backend.entity.InvoiceItem;

import java.math.BigDecimal;
import java.util.List;

public class InvoiceDto {

    public Long id;
    public String referenceId;
    public Invoice.Status status;
    public BigDecimal amount;
    public List<InvoiceItemDto> invoiceItems;
}
