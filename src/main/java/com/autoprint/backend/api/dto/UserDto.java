package com.autoprint.backend.api.dto;

import java.math.BigDecimal;

public class UserDto {
    public Long id;
    public String phoneNumber;
    public String username;
    public String email;
    public String token;
    public String fullname;
    public String imageUrl;
    public BigDecimal amount;
    public Boolean active;
}
