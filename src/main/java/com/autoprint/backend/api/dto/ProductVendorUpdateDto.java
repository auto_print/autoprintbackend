package com.autoprint.backend.api.dto;

import java.math.BigDecimal;

public class ProductVendorUpdateDto {

    public BigDecimal price;
    public Boolean active;
}
