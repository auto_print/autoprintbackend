package com.autoprint.backend.api.dto;

public class LoginRequestDto {
    public String username;
    public String password;
}
