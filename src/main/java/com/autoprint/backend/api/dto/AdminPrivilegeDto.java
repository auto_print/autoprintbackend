package com.autoprint.backend.api.dto;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class AdminPrivilegeDto {

    public Long id;
    public String name;
    public Set<AdminRoleDto> roles = new HashSet();
}
