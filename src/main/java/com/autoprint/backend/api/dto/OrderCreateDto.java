package com.autoprint.backend.api.dto;

public class OrderCreateDto {

    public Double longitude;
    public Double latitude;
    public Long userId;
    public Long vendorId;
}
