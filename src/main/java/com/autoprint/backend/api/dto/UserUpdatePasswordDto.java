package com.autoprint.backend.api.dto;

public class UserUpdatePasswordDto {

    public String currentPassword;
    public String newPassword;
}
