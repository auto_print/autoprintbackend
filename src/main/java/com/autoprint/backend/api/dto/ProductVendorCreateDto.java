package com.autoprint.backend.api.dto;

import java.math.BigDecimal;

public class ProductVendorCreateDto {

    public Long productId;
    public Long vendorId;
    public BigDecimal price;
}
