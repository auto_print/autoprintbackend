package com.autoprint.backend.api.dto;

public class VendorRequestNearestDto {

    public Double longitude;
    public Double latitude;
}
