package com.autoprint.backend.api.dto;

public class LoginResponseDto {

    public String token;
    public String id;
    public String type;
}
