package com.autoprint.backend.api.dto;

import java.math.BigDecimal;

public class ProductVendorDto {

    public Long id;
    public ProductDto product;
    public VendorViewDto vendor;
    public BigDecimal price;
    public boolean active;
}
