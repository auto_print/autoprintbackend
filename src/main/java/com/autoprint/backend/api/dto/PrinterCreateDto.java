package com.autoprint.backend.api.dto;

public class PrinterCreateDto {

    public String name;
    public Long vendorId;
}
