package com.autoprint.backend.api.dto;

public class VendorUpdateDto {

    public String imageBase64;
    public String phoneNumber;
    public String username;
    public String email;
    public String fullname;
    public String vendorname;
    public Double longitude;
    public Double latitude;
}
