package com.autoprint.backend.api.dto;

public class VendorDto {

    public Long id;
    public String vendorname;
    public UserDto user;
    public Double longitude;
    public Double latitude;
}
