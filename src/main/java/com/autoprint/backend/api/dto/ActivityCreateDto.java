package com.autoprint.backend.api.dto;

public class ActivityCreateDto {

    public String title;
    public String content;
}
