package com.autoprint.backend.api.dto;

import com.autoprint.backend.entity.PrintJob;

public class PrintJobDto {

    public Long id;
    public PrintJob.Status status;
}
