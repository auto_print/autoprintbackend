package com.autoprint.backend.api.security;

import com.autoprint.backend.core.security.UserAware;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class UserPrincipal extends User {

    private UserAware user;

    public UserPrincipal(UserAware user, Collection<? extends GrantedAuthority> authorities) {
        super(user.getCredUsername(), "", authorities);
        this.user = user;
    }

    public UserAware getUser() {
        return this.user;
    }
}
