package com.autoprint.backend.api.security;

import com.autoprint.backend.core.security.UserAware;
import com.autoprint.backend.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;

import static com.autoprint.backend.api.security.AuthorityHelper.getAuthorities;

@Service
public class AuthorizationUserDetailsService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    @Autowired
    private LoginService loginService;

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
        UserAware authenticatedUser;
        String authorization = token.getCredentials().toString();
        if (authorization == null) return null;

        if (token.getCredentials() instanceof String) {
            if (authorization.toLowerCase().startsWith("basic")) {
                authenticatedUser = loginService.loginBasicAuthentication(authorization);
            } else {
                authenticatedUser = loginService.login(authorization);
            }
        } else {
            String[] credentials = (String[]) token.getCredentials();
            authenticatedUser = loginService.login(credentials[0], credentials[1]);
        }

        if (authenticatedUser == null) {
            throw new BadCredentialsException("Invalid credential");
        }

        return new UserPrincipal(authenticatedUser, getAuthorities(authenticatedUser));
    }

}
