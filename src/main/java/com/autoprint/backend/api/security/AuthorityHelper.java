package com.autoprint.backend.api.security;

import com.autoprint.backend.core.security.UserAware;
import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.entity.AdminPrivilege;
import com.autoprint.backend.entity.AdminRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;

public class AuthorityHelper {

    private static List<SimpleGrantedAuthority> userAuthorities = Collections.unmodifiableList(Arrays.asList(
            new SimpleGrantedAuthority("USER_READ"),
            new SimpleGrantedAuthority("USER_UPDATE"),
            new SimpleGrantedAuthority("VENDOR_READ"),
            new SimpleGrantedAuthority("VENDOR_UPDATE"))
    );
    public static Collection<? extends GrantedAuthority> getAuthorities(UserAware user) {
        if (!user.isSuper()) {
            return userAuthorities;
        } else {
            return getGrantedAuthorities(getPrivileges((((Admin) user).getRoles())));
        }
    }

    private static List<String> getPrivileges(Collection<AdminRole> roles) {
        List<String> privileges = new ArrayList<>();
        List<AdminPrivilege> collection = new ArrayList<>();
        for (AdminRole role : roles) {
            privileges.add("ROLE_" + role.getName());
            collection.addAll(role.getPrivileges());
        }
        for (AdminPrivilege item : collection) {
            privileges.add(item.getName());
        }
        return privileges;
    }

    private static List<? extends GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
}
