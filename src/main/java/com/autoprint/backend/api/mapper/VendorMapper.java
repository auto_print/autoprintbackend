package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.VendorCreateDto;
import com.autoprint.backend.api.dto.VendorDto;
import com.autoprint.backend.api.dto.VendorUpdateDto;
import com.autoprint.backend.api.dto.VendorViewDto;
import com.autoprint.backend.entity.Vendor;
import org.mapstruct.Mapper;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(uses = {UserMapper.class}, componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public interface VendorMapper {

    Vendor toVendor(VendorDto vendorDto);

    VendorDto toVendorDto(Vendor vendor);

    VendorViewDto toVendorViewDto(Vendor vendor);
}
