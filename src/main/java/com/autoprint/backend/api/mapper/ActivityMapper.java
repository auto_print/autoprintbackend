package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.*;
import com.autoprint.backend.entity.Activity;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.object.OrderOverallObject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public abstract class ActivityMapper {

    @Mappings({
            @Mapping(source = "user.id", target = "userId"),
    })
    public abstract ActivityDto toActivityDto(Activity activity);

    public abstract Activity toActivity(ActivityDto activityDto);

    public abstract Activity toActivity(ActivityCreateDto activityCreateDto);

    public abstract Activity toActivity(ActivityUpdateDto activityUpdateDto);

    @Mappings({
            @Mapping(source = "userId", target = "user.id")
    })
    public abstract Activity toActivity(ActivityResponseDto activityResponseDto);

    public Activity template(Long userId, Activity.Type type, String title, String content) {
        Activity activity = new Activity();
        User user = new User();
        user.setId(userId);
        activity.setUser(user);
        activity.setTitle(title);
        activity.setContent(content);
        activity.setType(type);
        return activity;
    }

    public ReportDataDto mapReportDataDto(Long totalLastMonthReport, Long totalUnresolveReport) {
        ReportDataDto reportDataDto = new ReportDataDto();
        reportDataDto.totalLastMonthReport = totalLastMonthReport;
        reportDataDto.totalUnresolveReport = totalUnresolveReport;
        return reportDataDto;
    }
}
