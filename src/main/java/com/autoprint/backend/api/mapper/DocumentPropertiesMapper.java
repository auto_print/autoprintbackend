package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.DocumentCreateDto;
import com.autoprint.backend.entity.Document;
import com.autoprint.backend.entity.DocumentProperties;
import com.autoprint.backend.services.DocumentPropertiesService;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public class DocumentPropertiesMapper {

    @Autowired
    private DocumentPropertiesService documentPropertiesService;

    public DocumentProperties mapDocumentProperties(DocumentProperties documentProperties, Document document) {
        DocumentProperties newDocumentProperties = new DocumentProperties();
        newDocumentProperties.setDocument(document);
        newDocumentProperties.setPageOrientation(documentProperties.getPageOrientation());
        newDocumentProperties.setPages(documentProperties.getPages());
        newDocumentProperties.setCopies(documentProperties.getCopies());
        newDocumentProperties.setDuplex(documentProperties.getDuplex());
        newDocumentProperties.setColour(documentProperties.getColour());
        documentProperties.setCoverType(documentProperties.getCoverType());
        documentProperties.setCoverColour(documentProperties.getCoverColour());
        return newDocumentProperties;
    }

    public DocumentProperties mapDocumentProperties(DocumentCreateDto documentCreateDto, Document document) {
        DocumentProperties documentProperties = new DocumentProperties();
        documentProperties.setDocument(document);
        documentProperties.setColour(documentCreateDto.colour);
        documentProperties.setDuplex(documentCreateDto.duplex);
        documentProperties.setCoverType(documentCreateDto.coverType);
        documentProperties.setCoverColour(documentCreateDto.coverColour);
        documentProperties.setDuplex(documentCreateDto.duplex);
        documentProperties.setPageOrientation(documentCreateDto.pageOrientation);
        documentProperties.setCopies(documentCreateDto.copies);
        documentProperties.setPages(documentPropertiesService.getPageNumber(documentCreateDto.pages));
        return documentProperties;
    }
}
