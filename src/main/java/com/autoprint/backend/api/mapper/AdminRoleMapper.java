package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.AdminDto;
import com.autoprint.backend.api.dto.AdminPrivilegeDto;
import com.autoprint.backend.api.dto.AdminRoleCreateDto;
import com.autoprint.backend.api.dto.AdminRoleDto;
import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.entity.AdminPrivilege;
import com.autoprint.backend.entity.AdminRole;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public interface AdminRoleMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    AdminRole toAdminRole(AdminRoleDto adminRoleDto);

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    AdminRole toAdminRole(AdminRoleCreateDto adminRoleCreateDto);

    AdminRoleDto fromAdminRole(AdminRole adminRole);

    @Mappings({
            @Mapping(target = "password", ignore = true),
            @Mapping(target = "token", ignore = true),
            @Mapping(target = "roles", ignore = true),
    })
    AdminDto fromAdmin(Admin admin);

    @Mappings({
            @Mapping(target = "roles", ignore = true),
    })
    AdminPrivilegeDto fromAdminPrivilege(AdminPrivilege adminPrivilege);

}
