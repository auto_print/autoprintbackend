package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.PrinterCreateDto;
import com.autoprint.backend.api.dto.PrinterDto;
import com.autoprint.backend.api.dto.PrinterUpdateDto;
import com.autoprint.backend.entity.Printer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public interface PrinterMapper {

    @Mappings({
            @Mapping(source = "vendorId", target = "vendor.id"),
    })
    Printer toPrinter(PrinterCreateDto printerCreateDto);

    Printer toPrinter(PrinterUpdateDto printerUpdateDto);

    Printer toPrinter(PrinterDto printerDto);

    PrinterDto toPrinterDto(Printer printer);
}
