package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.InvoiceDto;
import com.autoprint.backend.entity.Invoice;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(uses = {InvoiceItemMapper.class}, componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public interface InvoiceMapper {

    Invoice toInvoice(InvoiceDto invoiceDto);

    InvoiceDto toInvoiceDto(Invoice invoice);

}
