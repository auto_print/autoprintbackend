package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.PrintJobDto;
import com.autoprint.backend.entity.PrintJob;
import org.mapstruct.Mapper;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public interface PrintJobMapper {

    PrintJob toPrinterJob(PrintJobDto printJobDto);

    PrintJobDto toPrinterJobDto(PrintJob printJob);
}
