package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.ProductVendorCreateDto;
import com.autoprint.backend.api.dto.ProductVendorDto;
import com.autoprint.backend.api.dto.ProductVendorUpdateDto;
import com.autoprint.backend.entity.ProductVendor;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(uses = {ProductMapper.class, VendorMapper.class}, componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public interface ProductVendorMapper {

    @Mappings({
            @Mapping(source = "productId", target = "product.id"),
            @Mapping(source = "vendorId", target = "vendor.id"),
    })
    ProductVendor toProductVendor(ProductVendorCreateDto productVendorCreateDto);

    ProductVendor toProductVendor(ProductVendorUpdateDto productVendorUpdateDto);

    ProductVendorDto toProductVendorDto(ProductVendor productVendor);
}
