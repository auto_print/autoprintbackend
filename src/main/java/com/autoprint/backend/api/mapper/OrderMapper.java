package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.OrderCreateDto;
import com.autoprint.backend.api.dto.OrderDataDto;
import com.autoprint.backend.api.dto.OrderDto;
import com.autoprint.backend.api.dto.OrderPreConfirmDto;
import com.autoprint.backend.entity.Order;
import com.autoprint.backend.object.OrderOverallObject;
import com.autoprint.backend.services.DocumentService;
import com.autoprint.backend.services.OrderService;
import com.autoprint.backend.services.ProductVendorService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(uses = {VendorMapper.class, DocumentMapper.class}, componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public abstract class OrderMapper {

    @Autowired
    private OrderService orderService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private ProductVendorService productVendorService;

    @Mappings({
            @Mapping(source = "userId", target = "user.id"),
    })
    public abstract Order toOrder(OrderCreateDto orderCreateDto);

    @Mappings({
            @Mapping(source = "user.id", target = "userId"),
    })
    public abstract OrderDto toOrderDto(Order order);

    public abstract OrderPreConfirmDto toOrderPreConfirmDto(Order order);

    @AfterMapping
    protected void mapPrice(Order source, @MappingTarget OrderPreConfirmDto dto) {
        dto.amount = orderService.calculateMockTrueTotal(source.getId(), source.getVendor());
    }

    public OrderDataDto mapOrderDataDto(Long totalOrderInMonth, Long totalOrderLastMonth, Long biggestCount, Long smallestCount, List<OrderOverallObject> list) {
        OrderDataDto orderDataDto = new OrderDataDto();
        orderDataDto.totalOrderInMonth = totalOrderInMonth;
        orderDataDto.totalOrderLastMonth = totalOrderLastMonth;
        orderDataDto.biggestCount = biggestCount;
        orderDataDto.smallestCount = smallestCount;
        orderDataDto.list = list;
        return orderDataDto;
    }
}
