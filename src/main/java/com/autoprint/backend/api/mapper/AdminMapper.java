package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.*;
import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.entity.AdminPrivilege;
import com.autoprint.backend.entity.AdminRole;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.ERROR;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public interface AdminMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    Admin toAdmin(AdminCreateDto dto);

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    Admin toAdmin(AdminDto admin);

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    Admin toAdmin(AdminUpdateDto updateDto);

    @Mappings({
            @Mapping(target = "password", ignore = true),
            @Mapping(target = "token", ignore = true),
    })
    AdminDto toAdminDto(Admin admin);

    @Mappings({
            @Mapping(target = "users", ignore = true),
    })
    AdminRoleDto fromAdminRole(AdminRole adminRole);

    @Mappings({
            @Mapping(target = "roles", ignore = true),
    })
    AdminPrivilegeDto fromAdminPrivilege(AdminPrivilege adminPrivilege);

}
