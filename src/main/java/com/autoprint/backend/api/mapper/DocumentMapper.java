package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.DocumentCreateDto;
import com.autoprint.backend.api.dto.DocumentDto;
import com.autoprint.backend.api.dto.OrderPreConfirmDto;
import com.autoprint.backend.api.dto.UserDto;
import com.autoprint.backend.entity.Document;
import com.autoprint.backend.entity.Order;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.helper.UploadHelper;
import com.autoprint.backend.services.ProductService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public abstract class DocumentMapper {

    @Autowired
    private ProductService productService;
    @Autowired
    private UploadHelper uploadHelper;

    @Mappings({
            @Mapping(source = "orderId", target = "order.id"),
    })
    public abstract Document toDocument(DocumentCreateDto documenCreatetDto);

    @AfterMapping
    protected void mapProduct(DocumentCreateDto source, @MappingTarget Document result) {
        result.setProduct(productService.findProduct(source));
    }

    public abstract Document toDocument(DocumentDto documentDto);

    @Mappings({
            @Mapping(source = "documentProperties.pages", target = "pages"),
            @Mapping(source = "documentProperties.copies", target = "copies"),
            @Mapping(source = "documentProperties.pageOrientation", target = "pageOrientation"),
            @Mapping(source = "documentProperties.colour", target = "colour"),
            @Mapping(source = "documentProperties.duplex", target = "duplex"),
    })
    public abstract DocumentDto toDocumentDto(Document document);

    @AfterMapping
    protected void mapDocumentUrl(Document source, @MappingTarget DocumentDto dto) {
        dto.url = uploadHelper.documentHelper(source.getOrder().getId(), source.getFilename());
    }
}
