package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.WalletDto;
import com.autoprint.backend.entity.Wallet;
import org.mapstruct.Mapper;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public interface WalletMapper {

    Wallet toWallet(WalletDto walletDto);

    WalletDto toWalletDto(Wallet wallet);

}
