package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.OrderPreConfirmDto;
import com.autoprint.backend.api.dto.UserCreateDto;
import com.autoprint.backend.api.dto.UserDto;
import com.autoprint.backend.api.dto.UserUpdateDto;
import com.autoprint.backend.entity.Order;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.helper.UploadHelper;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public abstract class UserMapper {

    @Autowired
    private UploadHelper uploadHelper;

    public abstract User toUser(UserDto userDto);

    public abstract User toUser(UserCreateDto userCreateDto);

    public abstract User toUser(UserUpdateDto userUpdateDto);

    @Mappings({
            @Mapping(source = "wallet.amount", target = "amount"),
    })
    public abstract UserDto toUserDto(User user);

    @AfterMapping
    protected void mapImageUrl(User source, @MappingTarget UserDto dto) {
        dto.imageUrl = uploadHelper.avatarHelper(source.getId(), source.getAvatarRef());
    }
}
