package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.AdminPrivilegeDto;
import com.autoprint.backend.api.dto.AdminRoleDto;
import com.autoprint.backend.entity.AdminPrivilege;
import com.autoprint.backend.entity.AdminRole;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public interface AdminPrivilegeMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    AdminPrivilege toAdminPrivilege(AdminPrivilegeDto adminPrivilegeDto);

    AdminPrivilegeDto fromAdminPrivilege(AdminPrivilege adminPrivilege);

    @Mappings({
            @Mapping(target = "privileges", ignore = true),
            @Mapping(target = "users", ignore = true),
    })
    AdminRoleDto fromAdminRole(AdminRole adminRole);
}
