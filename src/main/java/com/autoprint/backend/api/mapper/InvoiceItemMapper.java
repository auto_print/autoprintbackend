package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.InvoiceItemDto;
import com.autoprint.backend.api.dto.OrderDto;
import com.autoprint.backend.entity.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.math.BigDecimal;

import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(uses = {OrderMapper.class, ProductMapper.class}, componentModel = "spring", unmappedTargetPolicy = IGNORE, nullValueMappingStrategy = RETURN_DEFAULT)
public abstract class InvoiceItemMapper {

    public InvoiceItem map(Document document, Invoice invoice, Product product, BigDecimal amount) {
        InvoiceItem invoiceItem = new InvoiceItem();
        invoiceItem.setAmount(amount);
        invoiceItem.setInvoice(invoice);

        if (document != null) {
            invoiceItem.setDocument(document);
        } else {
            invoiceItem.setDocument(null);
        }

        invoiceItem.setProduct(product);
        return invoiceItem;
    }

    @Mappings({
            @Mapping(source = "invoiceItem.document.order", target = "order"),
    })
    public abstract InvoiceItemDto toInvoiceItemDto(InvoiceItem invoiceItem);
}
