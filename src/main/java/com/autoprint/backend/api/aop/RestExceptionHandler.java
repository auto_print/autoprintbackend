package com.autoprint.backend.api.aop;

import com.autoprint.backend.exception.ExternalAPIException;
import com.autoprint.backend.exception.PermissionDeniedException;
import com.autoprint.backend.exception.ResponseErrorCode;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.HttpStatus.NOT_FOUND;

// FIXME: support super
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final List<Class<? extends Exception>> SUPRESSED_EXCEPTIONS = unmodifiableList(asList(
            HttpMediaTypeException.class,
            HttpMessageNotReadableException.class));
    private static Map<String, ResponseErrorCode> errorCodeMap = unmodifiableMap(ResponseErrorCode.getExceptionErrorCodes());

    @ExceptionHandler({Exception.class})
    public ResponseEntity handleAll(Exception ex, WebRequest request) {
        logRestError(ex);
        HttpStatus status = INTERNAL_SERVER_ERROR;

        // In the case of API request from internal, ErrorResponse wrapped the original error. So we would want to reflect the SAME status code.
        if (ex instanceof ErrorResponse) {
            status = HttpStatus.valueOf(((ErrorResponse) ex).getStatus());
        }

        return buildResponseEntity(new APIError(status, ex.getLocalizedMessage(), getRequestURI(request)));
    }

    @ExceptionHandler({PermissionDeniedException.class, IllegalAccessException.class})
    public ResponseEntity handlePermissionDeniedException(Exception ex, WebRequest request) {
        return buildResponseEntity(new APIError(FORBIDDEN, ex.getLocalizedMessage(), new ArrayList(), getRequestURI(request), getResponseErrorCode(ex)));
    }

    @ExceptionHandler({UnsupportedOperationException.class, NotImplementedException.class})
    public ResponseEntity handleUnsupportedOperationException(Exception ex, WebRequest request) {
        return buildResponseEntity(new APIError(NOT_IMPLEMENTED, ex.getLocalizedMessage(), getRequestURI(request)));
    }

    @ExceptionHandler({IllegalArgumentException.class, IllegalStateException.class, IllegalFormatException.class})
    public ResponseEntity handleIllegalArgument(Exception ex, WebRequest request) {

        return buildResponseEntity(new APIError(
                UNPROCESSABLE_ENTITY,
                ex.getLocalizedMessage(),
                getRequestURI(request)));
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity handleConstraintViolation(Exception ex, WebRequest request) {
        final ConstraintViolationException cve = filter(ConstraintViolationException.class, ex);
        final List errors = new ArrayList<>();
        APIError apiError;

        if (cve != null) {
            for (ConstraintViolation<?> violation : cve.getConstraintViolations()) {
                errors.add(new FieldValidationError(
                        violation
                                .getRootBeanClass()
                                .getName() + "." + violation.getPropertyPath(),
                        violation.getMessage()));
            }
            apiError = new APIError(UNPROCESSABLE_ENTITY, "Validation error", errors, getRequestURI(request));
        } else {
            final String message = ex != null ? ex.getLocalizedMessage() : "Validation error";
            apiError = new APIError(UNPROCESSABLE_ENTITY, message, errors, getRequestURI(request));
        }

        return buildResponseEntity(apiError);
    }

    /**
     * handling access denied exception
     */
    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity handleAccessDeniedException(Exception ex, WebRequest request) {
        return buildResponseEntity(new APIError(FORBIDDEN, ex.getLocalizedMessage(), new ArrayList(), getRequestURI(request), getResponseErrorCode(ex)));
    }

    /**
     * Work in conjunction with @{@link javax.validation.Valid} annotation based validations.
     */
    @Override
    protected ResponseEntity handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        List<FieldValidationError> errors = new ArrayList<>();
        for (FieldError fieldError : fieldErrors) {
            errors.add(new FieldValidationError(fieldError.getField(), fieldError.getDefaultMessage()));
        }
        return buildResponseEntity(new APIError(BAD_REQUEST, "Validation error", errors, getRequestURI(request)));
    }

    @Override
    protected ResponseEntity handleExceptionInternal(
            Exception ex,
            Object body,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        super.handleExceptionInternal(ex, body, headers, status, request);
        return buildResponseEntity(new APIError(status, ex.getLocalizedMessage(), getRequestURI(request)));
    }

    @ExceptionHandler({EntityNotFoundException.class})
    protected ResponseEntity handleEntityNotFound(EntityNotFoundException ex, WebRequest request) {
        return buildResponseEntity(new APIError(NOT_FOUND, ex.getMessage(), getRequestURI(request)));
    }

    private <E extends Throwable> E filter(Class<E> target, Throwable exception) {
        if (target.isInstance(exception)) {
            return (E) exception;
        }
        return exception.getCause() == null ? null : filter(target, exception.getCause());
    }

    private ResponseEntity buildResponseEntity(APIError apiError) {
        return new ResponseEntity(apiError, new HttpHeaders(), apiError.getStatus());
    }

    private boolean isSuppressed(Exception ex) {
        return SUPRESSED_EXCEPTIONS
                .stream()
                .anyMatch(aClass -> aClass.isAssignableFrom(ex.getClass()));
    }

    // TODO: move this to dedicated rest error advice!
    private void logRestError(final Exception ex) {
        if (logger.isErrorEnabled() && !isSuppressed(ex)) {
            String msg;
            if (ex instanceof ExternalAPIException) {
                final ExternalAPIException externalAPIException = (ExternalAPIException) ex;
                msg = String.format("%s. External raw API response: %s", externalAPIException.getLocalizedMessage(), externalAPIException.getRawAPIResponse());
            } else {
                msg = ex.getLocalizedMessage();
            }
            logger.error(msg, ex);
        }
    }

    private String getRequestURI(WebRequest request) {
        if (request instanceof ServletWebRequest) {
            HttpServletRequest hsr = ((ServletWebRequest) request).getRequest();
            return hsr.getRequestURI();
        } else {
            logger.warn("Failed to get URI for request: " + request.toString());
        }

        return null;
    }

    /**
     * This helper method is used to get exception error
     *
     * @param exception : exception
     * @return
     */
    private int getResponseErrorCode(final Exception exception) {
        ResponseErrorCode responseErrorCode = errorCodeMap.get(exception
                .getClass()
                .getCanonicalName());
        if (responseErrorCode == null) {
            logger.warn("Failed to get error code for class : " + exception
                    .getClass()
                    .toString());
            return 0;
        }
        return responseErrorCode.getCode();
    }
}
