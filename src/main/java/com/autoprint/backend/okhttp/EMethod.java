package com.autoprint.backend.okhttp;

public enum EMethod {
    GET, POST, PUT, DELETE
}
