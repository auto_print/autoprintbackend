package com.autoprint.backend.okhttp;

import okhttp3.*;
import org.apache.commons.collections4.CollectionUtils;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BuilderHttp {
    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient().newBuilder().connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS).build();
    private OkHttpModel config = null;
    private MediaType mediaType = JSON;

    private BuilderHttp() {
    }

    public static BuilderHttp create() {
        return new BuilderHttp();
    }

    /**
     * Override media type
     *
     * @param mediaType
     * @return
     */
    public BuilderHttp mediaType(MediaType mediaType) {
        this.mediaType = mediaType;
        return this;
    }

    /**
     * Endpoint Configuration
     *
     * @param config
     * @return
     */
    public BuilderHttp endpoint(OkHttpModel config) {
        this.config = config;
        return this;
    }

    public OkHttpResponse execute() throws IOException {
        Request request = buildRequest();
        try (Response response = client.newCall(request).execute()) {
            return OkHttpResponse.builder(response);
        }
    }

    private RequestBody buildRequestBody(String requestEntity, List<KeyValue<String>> list) {
        if (requestEntity != null) {
            return RequestBody.create(mediaType, config.getRequestEntity());
        } else if (CollectionUtils.isNotEmpty(list)) {
            return buildParameters(list);
        } else {
            return RequestBody.create(mediaType, "");
        }
    }

    private FormBody buildParameters(List<KeyValue<String>> list) {
        FormBody.Builder builder = new FormBody.Builder();
        for (KeyValue<String> valuePair : list) {
            builder.add(valuePair.getKey(), valuePair.getValue());
        }
        return builder.build();
    }

    private Request buildRequest() {
        // verifying the request
        config.verify();
        String url = config.getEndpoint();

        HttpUrl.Builder httpBuider = HttpUrl.parse(url).newBuilder();
        Request.Builder builder = new Request.Builder();

        // All type of request can have request parameter
        if (CollectionUtils.isNotEmpty(config.getQueryParameters())) {
            for (KeyValue<String> valuePair : config.getQueryParameters()) {
                httpBuider.addQueryParameter(valuePair.getKey(), valuePair.getValue());
            }
        }
        builder.url(httpBuider.build());
        EMethod method = config.getMethod();


        // Here we add body parameter for the POST, PUT, DELTE
        switch (method) {
            case POST:
                builder.post(buildRequestBody(config.getRequestEntity(), config.getBodyParameters()));
                break;
            case GET:
                builder.get();
                break;
            case PUT:
                builder.put(buildRequestBody(config.getRequestEntity(), config.getBodyParameters()));
                break;
            case DELETE:
                builder.delete(buildRequestBody(config.getRequestEntity(), config.getBodyParameters()));
                break;
        }

        // setup all the required headers
        builder.addHeader("Content-Type", mediaType.toString());

        for (KeyValue<String> valuePair : config.getRequestHeader()) {
            builder.addHeader(valuePair.getKey(), valuePair.getValue());
        }

        return builder.build();
    }
}
