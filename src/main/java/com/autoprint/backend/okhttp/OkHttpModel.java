package com.autoprint.backend.okhttp;

import java.util.ArrayList;
import java.util.List;

public class OkHttpModel {
    // request body entity, null if none
    protected String requestEntity = null;

    // request parameter
    protected List<KeyValue<String>> queryParameters = new ArrayList<>();

    // body parameter
    protected List<KeyValue<String>> bodyParameters = new ArrayList<>();

    // request header
    protected List<KeyValue<String>> requestHeader = new ArrayList<>();

    protected String endpoint;

    protected EMethod method;

    // public
    public static OkHttpModel create() {
        return new OkHttpModel();
    }

    public OkHttpModel requestEntity(String requestEntity) {
        this.requestEntity = requestEntity;
        return this;
    }

    public OkHttpModel queryParameters(List<KeyValue<String>> queryParameters) {
        this.queryParameters = queryParameters;
        return this;
    }

    public OkHttpModel bodyParameter(List<KeyValue<String>> bodyParameters) {
        this.bodyParameters = bodyParameters;
        return this;
    }

    public OkHttpModel requestHeader(List<KeyValue<String>> requestHeader) {
        this.requestHeader = requestHeader;
        return this;
    }

    public OkHttpModel endpoint(String endpoint) {
        this.endpoint = endpoint;
        return this;
    }

    public OkHttpModel method(EMethod method) {
        this.method = method;
        return this;
    }

    public String getRequestEntity() {
        return requestEntity;
    }

    public List<KeyValue<String>> getQueryParameters() {
        return queryParameters;
    }

    public List<KeyValue<String>> getBodyParameters() {
        return bodyParameters;
    }

    public List<KeyValue<String>> getRequestHeader() {
        return requestHeader;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public EMethod getMethod() {
        return method;
    }

    public OkHttpModel verify() {
        if (method == null) {
            throw new IllegalArgumentException("method cannot be null");
        }

        if (endpoint == null) {
            throw new IllegalArgumentException("endpoint cannot be null");
        }

        return this;
    }

    /**
     * This function will set the endpoint with path param, method
     *
     * @param apiPath
     * @param pathParams
     * @return
     */
    public OkHttpModel pathParam(String url, Object... pathParams) {
        if (pathParams != null)
            this.endpoint = String.format(url, pathParams);
        return this;
    }
}
