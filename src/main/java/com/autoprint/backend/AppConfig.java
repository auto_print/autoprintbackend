package com.autoprint.backend;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Value("${app.mode}")
    private String appMode;

    @Value("${app.mode.dev}")
    private String appModeDev;
    @Value("${app.mode.prod}")
    private String appModeProd;

    @Value("${app.mode.uat}")
    private String appModeUat;

    @Value("${app.email.retriver.email}")
    private String emailRetriverEmail;

    @Value("${app.email.send.allow}")
    private String emailSendAllow;

    @Value("${app.email.sender.username}")
    private String emailSenderUsername;
    @Value("${app.email.sender.password}")
    private String emailSenderPassword;

    @Value("${app.tax.rate}")
    private String taxRate;
    @Value("${app.service.rate}")
    private String serviceRate;

    @Value("${app.ezeep.admin.email}")
    private String ezeepAdminEmail;
    @Value("${app.ezeep.admin.email.password}")
    private String ezeepAdminEmailPassword;
    @Value("${app.ezeep.base64_encoded_secret}")
    private String ezeepBase64EncodedSecret;
    @Value("${app.ezeep.client_id}")
    private String ezeepClientId;
    @Value("${app.ezeep.client_secret}")
    private String ezeepClientSecret;
    @Value("${app.ezeep.scope}")
    private String ezeepScope;
    @Value("${app.ezeep.portal.baseurl}")
    private String ezeepPortalBaseUrl;

    @Value("${aws.s3.key.access}")
    private String awsS3KeyAccess;
    @Value("${aws.s3.key.secret}")
    private String awsS3KeySecret;
    @Value("${aws.s3.region}")
    private String awsS3Region;
    @Value("${aws.s3.max-retries}")
    private String awsS3Retries;

    @Value("${aws.mode.base.folder}")
    private String awsModeBaseFolder;
    @Value("${aws.public.bucket}")
    private String awsPublicBucket;
    @Value("${aws.private.bucket}")
    private String awsPrivateBucket;

    public String getAppMode() {
        return appMode;
    }

    public String getAppModeDev() {
        return appModeDev;
    }

    public String getAppModeProd() {
        return appModeProd;
    }

    public String getAppModeUat() {
        return appModeUat;
    }

    public String getEmailRetriverEmail() {
        return emailRetriverEmail;
    }

    public String getEmailSendAllow() {
        return emailSendAllow;
    }

    public String getEmailSenderUsername() {
        return emailSenderUsername;
    }

    public String getEmailSenderPassword() {
        return emailSenderPassword;
    }

    public String getTaxRate() {
        return taxRate;
    }

    public String getServiceRate() {
        return serviceRate;
    }

    public String getEzeepAdminEmail() {
        return ezeepAdminEmail;
    }

    public void setEzeepAdminEmail(String ezeepAdminEmail) {
        this.ezeepAdminEmail = ezeepAdminEmail;
    }

    public String getEzeepAdminEmailPassword() {
        return ezeepAdminEmailPassword;
    }

    public void setEzeepAdminEmailPassword(String ezeepAdminEmailPassword) {
        this.ezeepAdminEmailPassword = ezeepAdminEmailPassword;
    }

    public String getEzeepBase64EncodedSecret() {
        return ezeepBase64EncodedSecret;
    }

    public String getEzeepClientId() {
        return ezeepClientId;
    }

    public String getEzeepClientSecret() {
        return ezeepClientSecret;
    }

    public String getEzeepScope() {
        return ezeepScope;
    }

    public String getEzeepPortalBaseUrl() {
        return ezeepPortalBaseUrl;
    }

    public String getAwsS3KeyAccess() {
        return awsS3KeyAccess;
    }

    public String getAwsS3KeySecret() {
        return awsS3KeySecret;
    }

    public String getAwsS3Region() {
        return awsS3Region;
    }

    public String getAwsS3Retries() {
        return awsS3Retries;
    }

    public String getAwsModeBaseFolder() {
        return awsModeBaseFolder;
    }

    public String getAwsPublicBucket() {
        return awsPublicBucket;
    }

    public String getAwsPrivateBucket() {
        return awsPrivateBucket;
    }
}
