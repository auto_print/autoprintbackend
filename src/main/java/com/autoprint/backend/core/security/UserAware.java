package com.autoprint.backend.core.security;

import com.autoprint.backend.core.entity.IEntity;

import java.io.Serializable;

public interface UserAware<E extends Serializable> extends IEntity<E>, AccessTokenAware{
    boolean isUser();

    boolean isVendor();

    boolean isSuper();

    boolean isSystem();

    String getCredUsername();
}
