package com.autoprint.backend.core.security;

public interface AccessTokenAware {
    String getAuthToken();
}
