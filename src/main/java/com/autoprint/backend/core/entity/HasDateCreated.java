package com.autoprint.backend.core.entity;

import java.time.LocalDateTime;

public interface HasDateCreated {

    LocalDateTime getDateCreated();

    void setDateCreated(LocalDateTime dateCreated);
}
