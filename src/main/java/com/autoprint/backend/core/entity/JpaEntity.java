package com.autoprint.backend.core.entity;

import java.io.Serializable;

public interface JpaEntity <ID extends Serializable> extends IEntity<ID>, HasDateCreated, HasDateUpdated{
}
