package com.autoprint.backend.core.entity;

import java.time.LocalDateTime;

public interface HasDateUpdated {

    LocalDateTime getLastUpdated();

    void setLastUpdated(LocalDateTime lastUpdated);
}
