package com.autoprint.backend.core.entity;

import java.io.Serializable;

public interface IEntity<ID extends Serializable> extends Serializable {

    ID getId();

    String friendlyName();
}
