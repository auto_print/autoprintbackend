package com.autoprint.backend.helper;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.entity.Document;
import com.autoprint.backend.ezeep.NotificationUtils;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Base64;

import static java.lang.String.format;

@Component
public class UploadHelper {

    @Autowired
    private AppConfig appConfig;
    @Autowired
    private AwsS3Helper awsS3Helper;

    public String avatarHelper(Long userId, String ref) {
        if (StringUtils.isBlank(ref)) {
            return null;
        }
        String dir = String.format("avatar/%s", userId);
        return urlConstructor(appConfig.getAwsPublicBucket(), dir, ref);
    }

    public String documentHelper(Long orderId, String ref) {
        if (StringUtils.isBlank(ref)) {
            return null;
        }
        String dir = String.format("order/%s", orderId);
        return urlConstructor(appConfig.getAwsPublicBucket(), dir, String.format("%s", ref));
    }

    public String urlConstructor(String bucket, String folder, String filename) {
        if (StringUtils.isNotBlank(folder)) {
            return String.format("https://%s.s3-%s.amazonaws.com/%s/%s/%s",
                    bucket,
                    appConfig.getAwsS3Region(),
                    appConfig.getAwsModeBaseFolder(),
                    folder,
                    filename);
        } else {
            return String.format("https://%s.s3-%s.amazonaws.com/%s/%s",
                    bucket,
                    appConfig.getAwsS3Region(),
                    appConfig.getAwsModeBaseFolder(),
                    filename);
        }
    }

    public String upload(String base64, String folder, String filename) throws Exception {
        // default false
        return upload(base64, folder, filename, false);
    }

    public String upload(String base64, String folder, String filename, boolean includeExtension) throws Exception {
        if (appConfig.getAppModeDev().equals(appConfig.getAppMode())) {
            NotificationUtils.S3DisableNotification(this.getClass());
            return null;
        }

        base64 = removeMimeType(base64);

        byte[] fileDecoded = Base64
                .getDecoder()
                .decode(base64);

        if (!includeExtension) {
            String extension = getExtension(getContentType(filename, fileDecoded));
            filename = String.format("%s%s", filename, extension);
        }

        String dir;
        if (StringUtils.isNotBlank(folder)) {
            dir = String.format("%s/%s", appConfig.getAwsModeBaseFolder(), folder);
        } else {
            dir = appConfig.getAwsModeBaseFolder();
        }
        awsS3Helper.save(appConfig.getAwsPublicBucket(), dir, filename, fileDecoded);
        return filename;
    }

    // maybe we want to allow certain file only
    public String extractNaiveFileType(String name) {
        try {
            String[] list = name.split("[.]");
            return list[list.length - 1];
        } catch (Exception e) {
            throw new RuntimeException(String.format("Something wrong occured when want to extract filetype naively. Filename: %s", name));
        }
    }

    public String removeMimeType(String base64) {
        String delims = "[,]";
        String[] parts = base64.split(delims);
        if (parts.length > 1)
            return parts[1];
        else
            return parts[0];
    }

    // hack for image
    private String getExtension(String mimeType) throws Exception {
        if ("image/png".equals(mimeType)) {
            return ".png";
        } else if ("image/jpeg".equals(mimeType)) {
            return ".jpg";
        } else if ("application/pdf".equals(mimeType)) {
            return ".pdf";
        } else {
            // not supported type
            throw new UnsupportedOperationException("File not supported!");
        }
    }

    private String getContentType(String filename, byte[] source) {
        try {
            MagicMatch match = Magic.getMagicMatch(source);
            String extension = match.getMimeType();
            if (extension != null) {
                return extension;
            } else {
                String msg = format("Cannot decode filetype! Filename:%s", filename);
                throw new Exception(msg);
            }
        } catch (Exception e) {
            String msg = format("Unable to determine MIME type for %s. Msg: %s", filename, e.getLocalizedMessage());
            throw new RuntimeException(msg);
        }
    }

    public void copyDocument(Document oldDocument, Document newDocument) {
        if (appConfig.getAppModeDev().equals(appConfig.getAppMode())) {
            NotificationUtils.S3DisableNotification(this.getClass());
            return;
        }
        String oldPath = appConfig.getAwsModeBaseFolder() + String.format("/order/%s/%s", oldDocument.getOrder().getId(), oldDocument.getFilename());
        String newPath = appConfig.getAwsModeBaseFolder() + String.format("/order/%s/%s", newDocument.getOrder().getId(), newDocument.getFilename());
        awsS3Helper.copy(appConfig.getAwsPublicBucket(), oldPath, newPath);
    }
}
