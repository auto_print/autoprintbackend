package com.autoprint.backend.helper;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import org.apache.http.entity.ContentType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AwsS3Helper {

    private AmazonS3 s3;

    public AwsS3Helper(AmazonS3 s3Client) {
        this.s3 = s3Client;
    }

    public S3Object get(String bucket, String path, String filename) {
        String dir = path + "/" + filename;
        return get(bucket, dir);
    }

    public S3Object get(String bucket, String path) {
        return s3.getObject(bucket, path);
    }

    public URL getUrl(String bucket, String path, String filename) {
        String dir = path + "/" + filename;
        return getUrl(bucket, dir);
    }

    public URL getUrl(String bucket, String path) {
        return s3.getUrl(bucket, path);
    }

    public ListObjectsV2Result list(String bucket, String path) {
        return s3.listObjectsV2(bucket, path);
    }

    public byte[] download(String bucket, String path, String filename) throws IOException {
        String dir = path + "/" + filename;
        return download(bucket, dir);
    }

    public byte[] download(String bucket, String path) throws IOException {
        S3Object obj = get(bucket, path);
        S3ObjectInputStream inputStream = obj.getObjectContent();
        byte[] data = IOUtils.toByteArray(inputStream);
        return data;
    }

    public void save(String bucket, String path, String filename, byte[] data) {
        save(bucket, path, filename, data, ContentType.TEXT_PLAIN.toString());
    }

    public void save(String bucket, String path, String filename, byte[] data, String contentType) {
        String dir = path + "/" + filename;
        save(bucket, dir, data, contentType);
    }

    public void save(String bucket, String path, byte[] data) {
        save(bucket, path, data, ContentType.TEXT_PLAIN.toString());
    }

    public void save(String bucket, String path, byte[] data, String contentType) {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);

        // Error code: EntityTooSmall
        // Description: Your proposed upload is smaller than the minimum allowed object size. Each part must be at least 5 MB in size, except the last part.
        byte[] bytes = new byte[5242880];

        final String uploadId = s3
                .initiateMultipartUpload(new InitiateMultipartUploadRequest(bucket, path))
                .getUploadId();

        try {
            int bytesRead = 0;
            int partNumber = 1;
            List<UploadPartResult> results = new ArrayList<>();
            bytesRead = byteArrayInputStream.read(bytes);
            while (bytesRead >= 0) {
                UploadPartRequest part = new UploadPartRequest()
                        .withBucketName(bucket)
                        .withKey(path)
                        .withUploadId(uploadId)
                        .withPartNumber(partNumber)
                        .withInputStream(new ByteArrayInputStream(bytes, 0, bytesRead))
                        .withPartSize(bytesRead);
                results.add(s3.uploadPart(part));
                bytesRead = byteArrayInputStream.read(bytes);
                partNumber++;
            }
            CompleteMultipartUploadRequest completeRequest = new CompleteMultipartUploadRequest()
                    .withBucketName(bucket)
                    .withKey(path)
                    .withUploadId(uploadId)
                    .withPartETags(results);
            s3.completeMultipartUpload(completeRequest);
        } catch (IOException e) {
            throw new RuntimeException(e.getLocalizedMessage(), e);
        }
    }

    public void copy(String bucketName, String sourceKey, String destinationKey) {
        CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, sourceKey, bucketName, destinationKey);
        s3.copyObject(copyObjRequest);
    }
}
