package com.autoprint.backend.config;

import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.entity.SystemUser;
import com.autoprint.backend.repo.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class SystemUserConfig {

    @Autowired
    private AdminRepository adminRepository;

    @PostConstruct
    public void init() {
        Admin admin = adminRepository.findById(1L)
                .orElseThrow(() -> new IllegalStateException("System user not found, it should be pre-populated into database!"));

        SystemUser.setInstance(admin);

        System.out.println("SYSTEM USER INITIALIZE");
    }
}
