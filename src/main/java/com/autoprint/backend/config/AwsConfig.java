package com.autoprint.backend.config;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.ClientConfigurationFactory;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.retry.PredefinedRetryPolicies;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.autoprint.backend.AppConfig;
import com.autoprint.backend.helper.AwsS3Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AwsConfig {

    AWSCredentials credentials;
    AmazonS3 s3;
    @Autowired
    private AppConfig appConfig;

    @PostConstruct
    protected void init() {
        credentials = new BasicAWSCredentials(
                appConfig.getAwsS3KeyAccess(),
                appConfig.getAwsS3KeySecret()
        );

        s3 = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withClientConfiguration(getConfiguration())
                .withRegion(Regions.fromName(appConfig.getAwsS3Region()))
                .build();
    }

    private ClientConfiguration getConfiguration() {
        ClientConfiguration config = new ClientConfigurationFactory().getConfig();
        config.setRetryPolicy(PredefinedRetryPolicies.getDefaultRetryPolicyWithCustomMaxRetries(Integer.parseInt(appConfig.getAwsS3Retries())));
        return config;
    }

    @Bean
    public AwsS3Helper getAWSS3Helper() {
        return new AwsS3Helper(s3);
    }
}
