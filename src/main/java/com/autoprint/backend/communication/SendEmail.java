package com.autoprint.backend.communication;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.object.EmailObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

import static java.lang.String.format;

@Service
public class SendEmail {

    @Autowired
    private AppConfig appConfig;

    final String host = "smtp.gmail.com";

    // Get system properties
    Properties properties = System.getProperties();

    // Setup mail server
    public void sendEmail(EmailObject emailObject) {

        // email ID of Sender.
        final String sender = appConfig.getEmailSenderUsername();
        final String username = appConfig.getEmailSenderUsername();
        final String password = appConfig.getEmailSenderPassword();

        // Setting up mail server
        properties.setProperty("mail.imap.host", host);

        try {
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", host);
            properties.put("mail.smtp.port", "587");

            Session session = Session.getInstance(properties,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });
            // MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From Field: adding senders email to from field.
            message.setFrom(new InternetAddress(sender));

            // Set Subject: subject of the email
            if (emailObject.getSubject() != null) {
                message.setSubject(emailObject.getSubject());
            } else {
                message.setSubject("You got an email!");
            }

            // set body of the email.
            if (emailObject.getText() != null) {
                message.setContent(emailObject.getText(), "text/html");
            }

            // Set To Field: adding recipient's email to from field.
            if (appConfig.getAppMode().equals(appConfig.getAppModeDev())) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(appConfig.getEmailRetriverEmail()));
                if ("true".equals(appConfig.getEmailSendAllow())) {
                    // Send email.
                    Transport.send(message);
                    // TODO: log
                    System.out.println("Mail successfully sent");
                } else {
                    System.out.println(format("Email is not send. Allow send are disable. Recipient:%s, Subject:%s, Text:%s",
                            emailObject.getRecipient(), emailObject.getSubject(), emailObject.getText()));
                }
            } else if (appConfig.getAppMode().equals(appConfig.getAppModeUat())) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailObject.getRecipient()));
            }
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
}

