package com.autoprint.backend.utils;

import org.springframework.security.crypto.codec.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class SecureUtils {

    final static SecureRandom rnd = new SecureRandom();

    public static String generateToken(int size) {
        String ALPHA_NUMERIC = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            sb.append(ALPHA_NUMERIC.charAt(rnd.nextInt(ALPHA_NUMERIC.length())));
        }
        return sb.toString();
    }

    public static String generateTokenAlphaAlphabet(int size) {
        String ALPHA_NUMERIC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            sb.append(ALPHA_NUMERIC.charAt(rnd.nextInt(ALPHA_NUMERIC.length())));
        }
        return sb.toString();
    }

    public static String generateTokenAlphabet(int size) {
        String ALPHA_NUMERIC = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            sb.append(ALPHA_NUMERIC.charAt(rnd.nextInt(ALPHA_NUMERIC.length())));
        }
        return sb.toString();
    }

    public static String generateTokenDigit(int size) {
        String ALPHA_NUMERIC = "0123456789";
        StringBuilder sb = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            sb.append(ALPHA_NUMERIC.charAt(rnd.nextInt(ALPHA_NUMERIC.length())));
        }
        return sb.toString();
    }

    public static String generateHmacSHA256(final String key, final String data) throws NoSuchAlgorithmException, java.security.InvalidKeyException, IllegalStateException {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        return new String(Hex.encode((sha256_HMAC.doFinal(data.getBytes(StandardCharsets.UTF_8)))));
    }

    // ---- rand location ----
    public static Double generateDecimal(int mode) {
        Random r = new Random();
        int randSeed = r.nextInt(4);
        double seed = 0d;
        /*
        0 - longitude
        1 - latitude
         */
        if (mode == 0) {
            seed = 100.00;
        } else if (mode == 1) {
            seed = 1.00;
        } else {
            throw new IllegalArgumentException("No mode found!");
        }

        seed += randSeed;
        double randOffset = r.nextInt(10) / 100.0;
        seed += randOffset;
        return seed;
    }
}
