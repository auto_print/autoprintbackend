package com.autoprint.backend.utils;

import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Map;

@Service
public class ExportPDFUtils {

    @Autowired
    SpringTemplateEngine templateEngine;

    public File exportToPdfBox(Map<String, Object> variables, String templatePath, String out) {
        File outFile = new File(out);
        try {
            if (!outFile.exists()) {
                outFile.createNewFile();
            }
            OutputStream os = new FileOutputStream(out);
            // There are more options on the builder than shown below.
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.withHtmlContent(getHtmlString(variables, templatePath), "file:");
            builder.toStream(os);
            builder.run();
        } catch (Exception e) {
        }
        return outFile;
    }

    private String getHtmlString(Map<String, Object> variables, String templatePath) {
        try {
            final Context ctx = new Context();
            ctx.setVariables(variables);
            return templateEngine.process(templatePath, ctx);
        } catch (Exception e) {
            return null;
        }
    }
}
