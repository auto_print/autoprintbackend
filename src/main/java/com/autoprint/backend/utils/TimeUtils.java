package com.autoprint.backend.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class TimeUtils {

    public static LocalDateTime getStartOfMonth() {
        Calendar c = Calendar.getInstance();
        return getCalendar(c);
    }

    public static LocalDateTime getLastMonth() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        return getCalendar(c);
    }

    public static LocalDateTime getTime(int month, int year) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, 1);
        return getCalendar(c);
    }

    public static LocalDateTime getLastYear() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -1);
        return getCalendar(c);
    }

    private static LocalDateTime getCalendar(Calendar c) {
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        return toLocalDateTime(c);
    }

    public static LocalDateTime toLocalDateTime(Calendar calendar) {
        if (calendar == null) {
            return null;
        }
        TimeZone tz = calendar.getTimeZone();
        ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
        return LocalDateTime.ofInstant(calendar.toInstant(), zid);
    }

    public static String getMonthName(LocalDateTime date) {
        return date.getMonth().name().substring(0, 3);
    }

    public static ArrayList<String> monthListInit() {
        ArrayList<String> list = new ArrayList<>();
        list.add("JAN");
        list.add("FEB");
        list.add("MAC");
        list.add("APR");
        list.add("MAY");
        list.add("JUN");
        list.add("JUL");
        list.add("AUG");
        list.add("SEP");
        list.add("OCT");
        list.add("NOV");
        list.add("DEC");
        return list;
    }
}
