package com.autoprint.backend.utils;

public class Utils {

    public static String parseSpace(String x) {
        return x.replaceAll("\\s{2,}", " ").replaceAll(" ", "_").trim();
    }
}
