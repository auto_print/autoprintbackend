package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.PrinterCreateDto;
import com.autoprint.backend.api.dto.PrinterDto;
import com.autoprint.backend.api.dto.PrinterUpdateDto;
import com.autoprint.backend.api.mapper.PrinterMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.services.PrinterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/printer")
public class PrinterController {

    @Autowired
    private PrinterService printerService;
    @Autowired
    private PrinterMapper printerMapper;

    @PostMapping(value = "")
    public ResponseEntity<PrinterDto> create(@RequestBody PrinterCreateDto printerCreateDto) {
        return ResponseEntity.ok(printerMapper.toPrinterDto(printerService.create(printerMapper.toPrinter(printerCreateDto))));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PrinterDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(printerMapper.toPrinterDto(printerService.getById(id)));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<PrinterDto> update(@PathVariable Long id, @RequestBody @Valid PrinterUpdateDto dto) {
        return ResponseEntity.ok(printerMapper.toPrinterDto(printerService.update(printerMapper.toPrinter(dto), id)));
    }

    @GetMapping(value = "/vendor/{vendorId}")
    public ResponseEntity<Page<PrinterDto>> findAllByVendorId(
            @PathVariable Long vendorId,
            @RequestParam(required = false) Integer pageNumber,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort
    ) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(pageSize, pageNumber, sort);
        return ResponseEntity.ok(printerService.findAllByVendorId(vendorId, pageRequest)
                .map(printer -> printerMapper.toPrinterDto(printer)));
    }
}
