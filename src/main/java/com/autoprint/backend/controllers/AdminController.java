package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.*;
import com.autoprint.backend.api.mapper.AdminMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.repo.specification.GenericEntitySpecification;
import com.autoprint.backend.repo.specification.SearchCriteria;
import com.autoprint.backend.services.AdminService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;
    @Autowired
    private AdminMapper adminMapper;

    @PostMapping(value = "")
    public ResponseEntity<AdminDto> create(@RequestBody AdminCreateDto admin) {
        return ResponseEntity.ok(adminMapper.toAdminDto(adminService.create(adminMapper.toAdmin(admin))));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<AdminDto> update(@PathVariable Long id, @RequestBody AdminUpdateDto admin) {
        return ResponseEntity.ok(adminMapper.toAdminDto(adminService.update(adminMapper.toAdmin(admin), id)));
    }

    @PostMapping(value = "/{id}/revoke")
    public ResponseEntity<AdminDto> revoke(@PathVariable Long id) {
        return ResponseEntity.ok(adminMapper.toAdminDto(adminService.revoke(id)));
    }

    @PostMapping(value = "/{id}/reactivate")
    public ResponseEntity<AdminDto> reactivate(@PathVariable Long id) {
        return ResponseEntity.ok(adminMapper.toAdminDto(adminService.reactivate(id)));
    }

    @GetMapping(value = "")
    public ResponseEntity<AdminDto> get() {
        return ResponseEntity.ok(adminMapper.toAdminDto(adminService.get()));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AdminDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(adminMapper.toAdminDto(adminService.getById(id)));
    }

    @PostMapping(value = "/list")
    public ResponseEntity<Page<AdminDto>> list(@Valid @RequestBody ListDto dto) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(dto.pageSize, dto.pageNumber, dto.sort);
        GenericEntitySpecification<Admin> specification = new GenericEntitySpecification<>();
        for (SearchCriteria searchCriteria : dto.searchCriterias) {
            specification.addCriteria(searchCriteria);
        }
        return ResponseEntity.ok().body(adminService.get(specification, pageRequest)
                .map(user -> adminMapper.toAdminDto(user)));
    }

    @PostMapping(value = {
            "/{id}/role/{roleId}"
    })
    public ResponseEntity<AdminDto> addRoleToAdmin(@PathVariable Long id, @PathVariable Long roleId) {
        return ResponseEntity.ok().body(adminMapper.toAdminDto(adminService.addRoleToAdmin(id, roleId)));
    }

    @DeleteMapping(value = {
            "/{id}/role/{roleId}"
    })
    public ResponseEntity<AdminDto> removeRoleFromAdmin(@PathVariable Long id, @PathVariable Long roleId) {
        return ResponseEntity.ok().body(adminMapper.toAdminDto(adminService.removeRoleFromAdmin(id, roleId)));
    }

}
