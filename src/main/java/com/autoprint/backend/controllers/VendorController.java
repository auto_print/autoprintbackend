package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.*;
import com.autoprint.backend.api.mapper.VendorMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.repo.specification.GenericEntitySpecification;
import com.autoprint.backend.repo.specification.SearchCriteria;
import com.autoprint.backend.services.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/vendor")
public class VendorController {

    @Autowired
    private VendorService vendorService;
    @Autowired
    private VendorMapper vendorMapper;

    @PostMapping(value = "")
    public ResponseEntity<VendorDto> create(@RequestBody VendorCreateDto vendorCreateDto) {
        return ResponseEntity.ok(vendorMapper.toVendorDto(vendorService.create(vendorCreateDto)));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<VendorDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(vendorMapper.toVendorDto(vendorService.getById(id)));
    }

    @GetMapping(value = "/users/{userId}")
    public ResponseEntity<VendorDto> getByUserId(@PathVariable Long userId) {
        return ResponseEntity.ok(vendorMapper.toVendorDto(vendorService.getByUserId(userId)));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<VendorDto> update(@PathVariable Long id, @RequestBody VendorUpdateDto vendorUpdateDto) {
        return ResponseEntity.ok(vendorMapper.toVendorDto(vendorService.update(vendorUpdateDto, id)));
    }

    @PostMapping(value = "/list")
    public ResponseEntity<Page<VendorDto>> list(@Valid @RequestBody ListDto dto) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(dto.pageSize, dto.pageNumber, dto.sort);
        GenericEntitySpecification<User> specification = new GenericEntitySpecification<>();
        for (SearchCriteria searchCriteria : dto.searchCriterias) {
            specification.addCriteria(searchCriteria);
        }
        return ResponseEntity.ok().body(vendorService.get(specification, pageRequest)
                .map(vendor -> vendorMapper.toVendorDto(vendor)));
    }

    @PostMapping(value = "/nearest")
    public ResponseEntity<VendorResponseNearestDto> getNearestVendor(@Valid @RequestBody VendorRequestNearestDto dto, @RequestParam(required = false) Long orderId) {
        VendorResponseNearestDto responseNearestDto = new VendorResponseNearestDto();
        if (orderId != null) {
            responseNearestDto.list = vendorService.getListNearestVendor(dto.latitude, dto.longitude, orderId).stream()
                    .map(vendor -> vendorMapper.toVendorDto(vendor)).collect(Collectors.toList());
        } else {
            responseNearestDto.list = vendorService.getListNearestVendor(dto.latitude, dto.longitude, null).stream()
                    .map(vendor -> vendorMapper.toVendorDto(vendor)).collect(Collectors.toList());
        }
        return ResponseEntity.ok().body(responseNearestDto);
    }
}
