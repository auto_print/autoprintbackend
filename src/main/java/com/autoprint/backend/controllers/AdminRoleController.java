package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.AdminRoleCreateDto;
import com.autoprint.backend.api.dto.AdminRoleDto;
import com.autoprint.backend.api.mapper.AdminRoleMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.entity.AdminRole;
import com.autoprint.backend.services.AdminRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/admin/role")
public class AdminRoleController {

    @Autowired
    private AdminRoleService adminRoleService;
    @Autowired
    private AdminRoleMapper adminRoleMapper;

    @PostMapping(value = {
            ""
    })
    public ResponseEntity<AdminRoleDto> create(@RequestBody AdminRoleCreateDto adminRoleCreateDto) {
        return ResponseEntity.ok().body(adminRoleMapper.fromAdminRole(adminRoleService.create(adminRoleMapper.toAdminRole(adminRoleCreateDto))));
    }

    @GetMapping(value = {
            "/{id}"
    })
    public ResponseEntity<AdminRoleDto> get(@PathVariable Long id) {
        return ResponseEntity.ok().body(adminRoleMapper.fromAdminRole(adminRoleService.getById(id)));
    }

    @GetMapping(value = {
            "/list"
    })
    public ResponseEntity<Page<AdminRoleDto>> get(
            @RequestParam(required = false) Integer pageNumber,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort) {
        return ResponseEntity.ok().body(adminRoleService.get(PageRequestBuilder.getPageRequest(pageSize, pageNumber, sort))
                .map(adminRoleMapper::fromAdminRole));
    }

    @PutMapping(value = {
            "/{id}"
    })
    public ResponseEntity<AdminRoleDto> update(@RequestBody AdminRoleDto adminRoleDto, @PathVariable Long id) {
        AdminRole adminRole = adminRoleMapper.toAdminRole(adminRoleDto);
        adminRole.setId(id);
        return ResponseEntity.ok().body(adminRoleMapper.fromAdminRole(adminRoleService.update(adminRole)));
    }

    @PostMapping(value = {
            "/{roleId}/privilege/{privilegeId}"
    })
    public ResponseEntity<AdminRoleDto> addPrivilegeFromRole(@PathVariable Long roleId, @PathVariable Long privilegeId) {
        return ResponseEntity.ok().body(adminRoleMapper.fromAdminRole(adminRoleService.addPrivilegeToRole(roleId, privilegeId)));
    }

    @DeleteMapping(value = {
            "/{roleId}/privilege/{privilegeId}"
    })
    public ResponseEntity<AdminRoleDto> removePrivilegeFromRole(@PathVariable Long roleId, @PathVariable Long privilegeId) {
        return ResponseEntity.ok().body(adminRoleMapper.fromAdminRole(adminRoleService.removePrivilegeFromRole(roleId, privilegeId)));
    }

}
