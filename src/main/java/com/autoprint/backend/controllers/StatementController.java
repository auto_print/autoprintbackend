package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.ByteArrayDto;
import com.autoprint.backend.api.dto.StatementListDto;
import com.autoprint.backend.api.dto.StatementRequestDto;
import com.autoprint.backend.api.dto.VendorDto;
import com.autoprint.backend.services.StatementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/statement")
public class StatementController {

    @Autowired
    private StatementService statementService;

    @GetMapping(value = "/vendor/{vendorId}/list")
    public ResponseEntity<StatementListDto> get(@PathVariable Long vendorId) {
        return ResponseEntity.ok().body(statementService.getStatementList(vendorId));
    }

    @PostMapping("/account")
    public ResponseEntity<?> statementOfAccount(@RequestBody @Valid StatementRequestDto dto) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=statement.pdf");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        ByteArrayDto contents = statementService.getStatementOfAccount(dto);
        return new ResponseEntity<>(contents, headers, HttpStatus.OK);
    }
}
