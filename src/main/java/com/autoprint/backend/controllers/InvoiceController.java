package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.InvoiceDto;
import com.autoprint.backend.api.mapper.InvoiceMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.services.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private InvoiceMapper invoiceMapper;

    @GetMapping(value = "/{id}")
    public ResponseEntity<InvoiceDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(invoiceMapper.toInvoiceDto(invoiceService.getById(id)));
    }

    @PostMapping(value = "refund/{id}")
    public ResponseEntity<InvoiceDto> refund(@PathVariable Long id) {
        return ResponseEntity.ok(invoiceMapper.toInvoiceDto(invoiceService.refund(id)));
    }

    /*@GetMapping(value = "/user/{userId}")
    public ResponseEntity<Page<InvoiceDto>> getByUserId(
            @PathVariable Long userId,
            @RequestParam(required = false) Integer pageNumber,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort
    ) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(pageSize, pageNumber, sort);
        return ResponseEntity.ok(invoiceService.getByUserId(userId, pageRequest)
                .map(invoice -> invoiceMapper.toInvoiceDto(invoice)));
    }*/
}
