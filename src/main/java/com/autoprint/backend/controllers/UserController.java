package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.*;
import com.autoprint.backend.api.mapper.UserMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.repo.specification.GenericEntitySpecification;
import com.autoprint.backend.repo.specification.SearchCriteria;
import com.autoprint.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    @PostMapping(value = "")
    public ResponseEntity<UserDto> create(@RequestBody UserCreateDto userCreateDto) {
        return ResponseEntity.ok(userMapper.toUserDto(userService.create(userMapper.toUser(userCreateDto))));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(userMapper.toUserDto(userService.getById(id)));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UserDto> update(@PathVariable Long id, @RequestBody UserUpdateDto userUpdateDto) {
        return ResponseEntity.ok(userMapper.toUserDto(userService.update(userMapper.toUser(userUpdateDto), id)));
    }

    @PutMapping(value = "/update/password")
    public ResponseEntity<UserDto> updatePassword(@RequestBody UserUpdatePasswordDto userUpdatePasswordDto) {
        return ResponseEntity.ok(userMapper.toUserDto(userService.updatePassword(userUpdatePasswordDto.currentPassword, userUpdatePasswordDto.newPassword)));
    }

    @PostMapping(value = "/list")
    public ResponseEntity<Page<UserDto>> list(@Valid @RequestBody ListDto dto) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(dto.pageSize, dto.pageNumber, dto.sort);
        GenericEntitySpecification<User> specification = new GenericEntitySpecification<>();
        for (SearchCriteria searchCriteria : dto.searchCriterias) {
            specification.addCriteria(searchCriteria);
        }
        return ResponseEntity.ok().body(userService.get(specification, pageRequest)
                .map(user -> userMapper.toUserDto(user)));
    }

    @PostMapping(value = "/forget/password")
    public void forgetPassword(@Valid @RequestBody ForgotPasswordRequestDto dto) {
        userService.forgetPassword(dto.email);
    }

    //FIXME:
    @PostMapping(value = "/verify/code")
    public ResponseEntity<VerifyCodeResponseDto> verifyCode(@Valid @RequestBody VerifyCodeRequestDto dto) {
        return ResponseEntity.ok(userService.verifyCode(dto.code));
    }

    @PostMapping(value = "/{id}/new/password")
    public ResponseEntity<UserDto> newPassword(@PathVariable Long id, @Valid @RequestBody NewPasswordRequestDto dto) {
        return ResponseEntity.ok(userMapper.toUserDto(userService.setNewPassword(id, dto.newPassword)));
    }

}
