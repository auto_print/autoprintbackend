package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.LoginResponseDto;
import com.autoprint.backend.api.security.UserPrincipal;
import com.autoprint.backend.core.security.UserAware;
import com.autoprint.backend.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;


    @PostMapping(value = "/login")
    public ResponseEntity<LoginResponseDto> get(Authentication authentication) {
        UserAware userAware = ((UserPrincipal) authentication.getPrincipal()).getUser();
        if (userAware != null) {
            LoginResponseDto loginResponseDto = new LoginResponseDto();
            loginResponseDto.id = userAware.getId().toString();
            loginResponseDto.token = userAware.getAuthToken();
            if (userAware.isUser()) {
                loginResponseDto.type = "USER";
            } else if (userAware.isVendor()) {
                loginResponseDto.type = "VENDOR";
            } else {
                loginResponseDto.type = "ADMIN";
            }
            return ResponseEntity.ok(loginResponseDto);
        }

        return null;
    }
}
