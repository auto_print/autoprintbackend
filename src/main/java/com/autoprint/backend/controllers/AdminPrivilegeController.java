package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.AdminDto;
import com.autoprint.backend.api.dto.AdminPrivilegeDto;
import com.autoprint.backend.api.dto.AdminRoleDto;
import com.autoprint.backend.api.dto.ListDto;
import com.autoprint.backend.api.mapper.AdminPrivilegeMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.repo.specification.GenericEntitySpecification;
import com.autoprint.backend.repo.specification.SearchCriteria;
import com.autoprint.backend.services.AdminPrivilegeService;
import com.autoprint.backend.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/admin/privilege")
public class AdminPrivilegeController {

    @Autowired
    private AdminPrivilegeService adminPrivilegeService;
    @Autowired
    private AdminPrivilegeMapper adminPrivilegeMapper;

    @GetMapping(value = {
            "/list"
    })
    public ResponseEntity<Page<AdminPrivilegeDto>> get(
            @RequestParam(required = false) Integer pageNumber,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort) {
        return ResponseEntity.ok().body(adminPrivilegeService.getAll(PageRequestBuilder.getPageRequest(pageSize, pageNumber, sort))
                .map(adminPrivilegeMapper::fromAdminPrivilege));
    }
}
