package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.DocumentDto;
import com.autoprint.backend.api.dto.ProductVendorCreateDto;
import com.autoprint.backend.api.dto.ProductVendorDto;
import com.autoprint.backend.api.dto.ProductVendorUpdateDto;
import com.autoprint.backend.api.mapper.ProductVendorMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.services.ProductVendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/product/vendor")
public class ProductVendorController {

    @Autowired
    private ProductVendorService productVendorService;
    @Autowired
    private ProductVendorMapper productVendorMapper;

    @PostMapping(value = "")
    public ResponseEntity<ProductVendorDto> create(@RequestBody ProductVendorCreateDto productVendorCreateDto) {
        return ResponseEntity.ok(productVendorMapper.toProductVendorDto(productVendorService.create(productVendorMapper.toProductVendor(productVendorCreateDto))));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<ProductVendorDto> update(@PathVariable Long id, @RequestBody ProductVendorUpdateDto productVendorUpdateDto) {
        return ResponseEntity.ok(productVendorMapper.toProductVendorDto(productVendorService.update(productVendorMapper.toProductVendor(productVendorUpdateDto), id)));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ProductVendorDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(productVendorMapper.toProductVendorDto(productVendorService.getById(id)));
    }

    @GetMapping(value = "/vendors/{vendorId}")
    public ResponseEntity<Page<ProductVendorDto>> getByVendorId(
            @PathVariable Long vendorId,
            @RequestParam(required = false) Integer pageNumber,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort
    ) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(pageSize, pageNumber, sort);
        return ResponseEntity.ok(productVendorService.getByVendorId(vendorId, pageRequest)
                .map(productVendor -> productVendorMapper.toProductVendorDto(productVendor)));
    }
}
