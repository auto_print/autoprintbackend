package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.WalletOperationDto;
import com.autoprint.backend.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/wallet")
public class WalletController {

    @Autowired
    private WalletService walletService;

    @PostMapping(value = "/operation")
    public void operation(@RequestBody WalletOperationDto walletOperationDto) {
        walletService.operation(walletOperationDto);
    }
}
