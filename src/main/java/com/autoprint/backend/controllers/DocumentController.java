package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.DocumentCreateDto;
import com.autoprint.backend.api.dto.DocumentDto;
import com.autoprint.backend.api.mapper.DocumentMapper;
import com.autoprint.backend.api.mapper.DocumentPropertiesMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.entity.Document;
import com.autoprint.backend.entity.DocumentProperties;
import com.autoprint.backend.services.DocumentPropertiesService;
import com.autoprint.backend.services.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/document")
public class DocumentController {

    @Autowired
    private DocumentService documentService;
    @Autowired
    private DocumentPropertiesService documentPropertiesService;
    @Autowired
    private DocumentMapper documentMapper;
    @Autowired
    private DocumentPropertiesMapper documentPropertiesMapper;

    @PostMapping(value = "")
    public ResponseEntity<DocumentDto> create(@RequestBody DocumentCreateDto documentCreateDto) {
        documentPropertiesService.checkDocumentPrropertiesValidity(documentCreateDto);
        return ResponseEntity.ok(documentMapper.toDocumentDto(documentService.create(documentCreateDto)));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DocumentDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(documentMapper.toDocumentDto(documentService.getById(id)));
    }

    @GetMapping(value = "/user/{userId}")
    public ResponseEntity<Page<DocumentDto>> getByUserId(
            @PathVariable Long userId,
            @RequestParam(required = false) Integer pageNumber,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort
    ) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(pageSize, pageNumber, sort);
        return ResponseEntity.ok(documentService.getByUserId(userId, pageRequest)
                .map(document -> documentMapper.toDocumentDto(document)));
    }

    @GetMapping(value = "/order/{orderId}")
    public ResponseEntity<Page<DocumentDto>> getByOrderId(
            @PathVariable Long orderId,
            @RequestParam(required = false) Integer pageNumber,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort
    ) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(pageSize, pageNumber, sort);
        return ResponseEntity.ok(documentService.getByOrderId(orderId, pageRequest)
                .map(document -> documentMapper.toDocumentDto(document)));
    }

    @DeleteMapping(value = "/{id}/cancel")
    public void cancel(@PathVariable Long id) {
        documentService.cancel(id);
    }
}
