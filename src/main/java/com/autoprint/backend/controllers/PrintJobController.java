package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.PrintJobDto;
import com.autoprint.backend.api.dto.UserDto;
import com.autoprint.backend.api.mapper.PrintJobMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.services.PrintJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/printjob")
public class PrintJobController {

    @Autowired
    private PrintJobService printJobService;
    @Autowired
    private PrintJobMapper printJobMapper;

    @PostMapping(value = "")
    public ResponseEntity<PrintJobDto> create(@RequestBody PrintJobDto printJobDto) {
        throw new UnsupportedOperationException("Create printjob controller is not supported");
        //return ResponseEntity.ok(printJobMapper.toPrinterJobDto(printJobService.create(printJobMapper.toPrinterJob(printJobDto))));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PrintJobDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(printJobMapper.toPrinterJobDto(printJobService.getById(id)));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<PrintJobDto> update(@PathVariable Long id, @RequestBody PrintJobDto printJobDto) {
        return ResponseEntity.ok(printJobMapper.toPrinterJobDto(printJobService.update(printJobMapper.toPrinterJob(printJobDto), id)));
    }

    @GetMapping(value = "/vendor/{vendorId}")
    public ResponseEntity<Page<PrintJobDto>> getByVendorId(
            @PathVariable Long vendorId,
            @RequestParam(required = false) Integer pageNumber,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort
    ) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(pageSize, pageNumber, sort);
        return ResponseEntity.ok(printJobService.getByVendorId(vendorId, pageRequest)
                .map(printJob -> printJobMapper.toPrinterJobDto(printJob)));
    }
}
