package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.*;
import com.autoprint.backend.api.mapper.ActivityMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.entity.Activity;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.repo.specification.GenericEntitySpecification;
import com.autoprint.backend.repo.specification.SearchCriteria;
import com.autoprint.backend.services.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/activity")
public class ActivityController {

    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityMapper activityMapper;

    @PostMapping(value = "")
    public ResponseEntity<ActivityDto> create(@RequestBody ActivityCreateDto activityCreateDto) {
        return ResponseEntity.ok(activityMapper.toActivityDto(activityService.create(activityMapper.toActivity(activityCreateDto))));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<ActivityDto> update(@PathVariable Long id, @RequestBody ActivityUpdateDto activityUpdateDto) {
        return ResponseEntity.ok(activityMapper.toActivityDto(activityService.update(activityMapper.toActivity(activityUpdateDto), id)));
    }

    @PostMapping(value = "/response")
    public ResponseEntity<ActivityDto> response(@RequestBody ActivityResponseDto activityResponseDto) {
        return ResponseEntity.ok(activityMapper.toActivityDto(activityService.response(activityMapper.toActivity(activityResponseDto))));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ActivityDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(activityMapper.toActivityDto(activityService.getById(id)));
    }

    @GetMapping(value = "/user/{userId}")
    public ResponseEntity<Page<ActivityDto>> getByUserId(@PathVariable Long userId) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(30, 0, "-dateCreated");
        return ResponseEntity.ok().body(activityService.getByUserId(userId, pageRequest)
                .map(activity -> activityMapper.toActivityDto(activity)));
    }

    @PostMapping(value = "/list")
    public ResponseEntity<Page<ActivityDto>> list(@Valid @RequestBody ListDto dto) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(dto.pageSize, dto.pageNumber, dto.sort);
        GenericEntitySpecification<Activity> specification = new GenericEntitySpecification<>();
        for (SearchCriteria searchCriteria : dto.searchCriterias) {
            specification.addCriteria(searchCriteria);
        }
        return ResponseEntity.ok().body(activityService.get(specification, pageRequest)
                .map(activity -> activityMapper.toActivityDto(activity)));
    }

    @GetMapping(value = "/data")
    public ResponseEntity<ReportDataDto> getData() {
        return ResponseEntity.ok(activityService.getReportData());
    }
}
