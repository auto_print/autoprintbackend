package com.autoprint.backend.controllers;

import com.autoprint.backend.api.security.UserPrincipal;
import com.autoprint.backend.api.security.IAuthenticationFacade;
import com.autoprint.backend.core.security.UserAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

public class BaseController {

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    public UserAware getAuthenticatedUser() {
        Authentication authentication = authenticationFacade.getAuthentication();
        if (authentication != null) {
            Object principalObj = authentication.getPrincipal();

            if (principalObj == null) {
                throw new IllegalStateException("Principal is unexpectedly undefined!");
            }

            if (principalObj instanceof UserPrincipal) {
                return ((UserPrincipal) principalObj).getUser();
            } else {
                //log
            }
        }
        return null;
    }
}
