package com.autoprint.backend.controllers;

import com.autoprint.backend.api.dto.*;
import com.autoprint.backend.api.mapper.OrderMapper;
import com.autoprint.backend.core.api.helper.PageRequestBuilder;
import com.autoprint.backend.entity.Order;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.repo.specification.GenericEntitySpecification;
import com.autoprint.backend.repo.specification.SearchCriteria;
import com.autoprint.backend.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;

@RestController
@RequestMapping(value = "/order")
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderMapper orderMapper;

    @PostMapping(value = "")
    public ResponseEntity<OrderDto> create(@RequestBody OrderCreateDto orderCreateDto) {
        return ResponseEntity.ok(orderMapper.toOrderDto(orderService.create(orderMapper.toOrder(orderCreateDto))));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<OrderDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(orderMapper.toOrderDto(orderService.getById(id)));
    }

    @GetMapping(value = "/vendor/{vendorId}")
    public ResponseEntity<Page<OrderDto>> getByVendorId(
            @PathVariable Long vendorId,
            @RequestParam(required = false) Order.Status status,
            @RequestParam(required = false) Integer pageNumber,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort
    ) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(pageSize, pageNumber, sort);
        return ResponseEntity.ok(orderService.getByVendorId(vendorId, status, pageRequest)
                .map(order -> orderMapper.toOrderDto(order)));
    }

    @GetMapping(value = "/user/{userId}")
    public ResponseEntity<Page<OrderDto>> getByUserId(
            @PathVariable Long userId,
            @RequestParam(required = false) Order.Status status,
            @RequestParam(required = false) Integer pageNumber,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort
    ) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(pageSize, pageNumber, sort);
        return ResponseEntity.ok(orderService.getByUserId(userId, status, pageRequest)
                .map(order -> orderMapper.toOrderDto(order)));
    }

    @PostMapping(value = "/{id}/pre/confirm")
    public ResponseEntity<OrderPreConfirmDto> preConfirm(@PathVariable Long id, @RequestBody(required = false) OrderPreConfirmRequestDto requestDto) {
        if (requestDto != null && requestDto.vendorId != null) {
            return ResponseEntity.ok(orderMapper.toOrderPreConfirmDto(orderService.preConfirmOrder(id, requestDto.vendorId)));
        } else {
            return ResponseEntity.ok(orderMapper.toOrderPreConfirmDto(orderService.preConfirmOrder(id, null)));
        }
    }

    @PostMapping(value = "/{id}/confirm")
    public ResponseEntity<OrderDto> confirm(@PathVariable Long id, @RequestParam boolean accept) {
        return ResponseEntity.ok(orderMapper.toOrderDto(orderService.confirmOrder(id, accept)));
    }

    // maybe invoice job?
    @PostMapping(value = "/{id}/pay")
    public void pay(@PathVariable Long id, @RequestParam BigDecimal amount) {
        orderService.pay(id, amount);
    }

    @PostMapping(value = "/list")
    public ResponseEntity<Page<OrderDto>> list(@Valid @RequestBody ListDto dto) {
        PageRequest pageRequest = PageRequestBuilder.getPageRequest(dto.pageSize, dto.pageNumber, dto.sort);
        GenericEntitySpecification<User> specification = new GenericEntitySpecification<>();
        for (SearchCriteria searchCriteria : dto.searchCriterias) {
            specification.addCriteria(searchCriteria);
        }
        return ResponseEntity.ok().body(orderService.get(specification, pageRequest)
                .map(order -> orderMapper.toOrderDto(order)));
    }

    @PostMapping(value = "/{id}/cancel")
    public void cancel(@PathVariable Long id) {
        orderService.cancel(id);
    }

    @PostMapping(value = "/{id}/reorder")
    public ResponseEntity<OrderDto> reorder(@PathVariable Long id, @RequestBody(required = false) OrderPreConfirmRequestDto requestDto) {
        if (requestDto != null && requestDto.vendorId != null) {
            return ResponseEntity.ok(orderMapper.toOrderDto(orderService.reorder(id, requestDto.vendorId)));
        } else {
            return ResponseEntity.ok(orderMapper.toOrderDto(orderService.reorder(id, null)));
        }
    }

    @PostMapping(value = "/{id}/reject")
    public void reject(@PathVariable Long id, @Valid @RequestBody OrderRejectDto dto) {
        orderService.reject(id, dto.reason);
    }

    @PostMapping(value = "/{id}/complete")
    public ResponseEntity<OrderDto> complete(@PathVariable Long id) {
        return ResponseEntity.ok(orderMapper.toOrderDto(orderService.complete(id)));
    }

    @GetMapping(value = "/data")
    public ResponseEntity<OrderDataDto> getData() {
        return ResponseEntity.ok(orderService.getOrderData());
    }
}
