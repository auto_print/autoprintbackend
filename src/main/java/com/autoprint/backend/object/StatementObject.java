package com.autoprint.backend.object;

import java.math.BigDecimal;

public class StatementObject {

    public String item;
    public String type;
    public BigDecimal amount;

    public StatementObject(String item, String type, BigDecimal amount) {
        this.item = item;
        this.type = type;
        this.amount = amount;
    }
}
