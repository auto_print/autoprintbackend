package com.autoprint.backend.object;

import com.autoprint.backend.utils.TimeUtils;

import java.time.LocalDateTime;

public class OrderOverallObject {

    private LocalDateTime date;
    private String month;
    private Long count;

    public OrderOverallObject(LocalDateTime date, Long count) {
        this.date = date;
        this.count = count;
        this.month = TimeUtils.getMonthName(this.date);
    }

    public OrderOverallObject(LocalDateTime date) {
        this.date = date;
        this.month = TimeUtils.getMonthName(this.date);
    }

    public OrderOverallObject(String month) {
        this.month = month;
        this.count = 0L;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
