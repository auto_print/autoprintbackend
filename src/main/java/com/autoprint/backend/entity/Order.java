package com.autoprint.backend.entity;


import com.autoprint.backend.core.entity.JpaEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "orders")
public class Order implements JpaEntity<Long> {

    public enum Status {
        PENDING,
        CONFIRM,
        CANCELLED,
        REJECTED,
        COMPLETED
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(optional = false, fetch = EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = "vendor_id", referencedColumnName = "id")
    private Vendor vendor;

    @NotNull
    private String referenceId;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    private String reason;

    @NotNull
    private Double longitude;

    @NotNull
    private Double latitude;

    private LocalDateTime dateCreated = LocalDateTime.now();

    private LocalDateTime lastUpdated = LocalDateTime.now();

    @OneToMany(mappedBy = "order", fetch = EAGER)
    private Set<Invoice> invoices;

    @Override
    public LocalDateTime getDateCreated() {
        return this.dateCreated;
    }

    @Override
    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public LocalDateTime getLastUpdated() {
        return this.lastUpdated;
    }

    @Override
    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Set<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(Set<Invoice> invoices) {
        this.invoices = invoices;
    }

    public boolean equalAmount(BigDecimal amount) {
        return getTotal().compareTo(amount) == 0;
    }

    public BigDecimal getTotal() {
        BigDecimal total = BigDecimal.ZERO;
        for (Invoice invoice : getInvoices()) {
            total = total.add(invoice.getAmount());
        }
        return total;
    }

    @Override
    public String friendlyName() {
        return "ORDER-" + getId();
    }
}
