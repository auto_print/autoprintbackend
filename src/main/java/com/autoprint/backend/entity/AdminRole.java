package com.autoprint.backend.entity;

import com.autoprint.backend.core.entity.JpaEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "admins_roles")
public class AdminRole  implements JpaEntity<Long> {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotNull
    @NotBlank
    private String name;

    @ManyToMany(fetch = EAGER, cascade = DETACH, mappedBy = "roles")
    private Set<Admin> users = new HashSet();

    @ManyToMany(fetch = EAGER, cascade = ALL)
    @JoinTable(
            name = "admins_privileges_privileges",
            joinColumns = @JoinColumn(
                    name = "role_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "privilege_id"))
    private Set<AdminPrivilege> privileges = new HashSet();

    private LocalDateTime dateCreated = LocalDateTime.now();

    private LocalDateTime lastUpdated = LocalDateTime.now();

    public AdminRole() {
    }

    public AdminRole(String name) {
        setName(name);
    }

    public AdminRole(Long id) {
        this.id = id;
    }

    @Override
    public String friendlyName() {
        return String.format("AdminRoles ID: %d", this.getId());
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Admin> getUsers() {
        return users;
    }

    public void setUsers(Set<Admin> users) {
        this.users = users;
    }

    public Set<AdminPrivilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(Set<AdminPrivilege> privileges) {
        this.privileges = privileges;
    }

    @Override
    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    @Override
    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
