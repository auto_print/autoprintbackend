package com.autoprint.backend.entity;

import com.autoprint.backend.core.entity.JpaEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "print_job")
public class PrintJob implements JpaEntity<Long> {

    public enum Status {
        PENDING,
        DONE,
        CANCELLED,
        ERROR
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotNull
    private String ezeepPrintJobId;

    @NotNull
    @ManyToOne(optional = false, fetch = EAGER)
    @JoinColumn(name = "document_id", referencedColumnName = "id", nullable = false)
    private Document document;

    @NotNull
    @ManyToOne(optional = false, fetch = EAGER)
    @JoinColumn(name = "vendor_id", referencedColumnName = "id", nullable = false)
    private Vendor vendor;

    @NotNull
    @ManyToOne(optional = false, fetch = EAGER)
    @JoinColumn(name = "printer_id", referencedColumnName = "id", nullable = false)
    private Printer printer;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    private LocalDateTime dateCreated = LocalDateTime.now();

    private LocalDateTime lastUpdated = LocalDateTime.now();

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEzeepPrintJobId() {
        return ezeepPrintJobId;
    }

    public void setEzeepPrintJobId(String ezeepPrintJobId) {
        this.ezeepPrintJobId = ezeepPrintJobId;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Printer getPrinter() {
        return printer;
    }

    public void setPrinter(Printer printer) {
        this.printer = printer;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    @Override
    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String friendlyName() {
        return "PrinterJob_" + getId();
    }
}
