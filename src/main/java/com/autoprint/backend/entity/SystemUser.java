package com.autoprint.backend.entity;

import com.autoprint.backend.api.security.UserPrincipal;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

import static com.autoprint.backend.api.security.AuthorityHelper.getAuthorities;

public final class SystemUser extends Admin {

    public static SystemUser systemUser = null;

    private SystemUser() {
    }

    public static SystemUser getInstance() {
        if (systemUser == null) {
            throw new RuntimeException("System user not initialized");
        }

        return systemUser;
    }

    public static void setInstance(Admin admin) {
        systemUser = new SystemUser();
        systemUser.setId(admin.getId());
        systemUser.setStatus(admin.getStatus());
        systemUser.setToken(admin.getAuthToken());
        systemUser.setUsername(admin.getUsername());
        // TODO: deep-copy
        systemUser.setRoles(admin.getRoles());
    }

    public boolean isSystem() {
        return true;
    }

    /**
     * TODO: workaround for spring's runAs.
     * TODO: doesn't concern about original auth object, so use this with care.
     */
    public void runAs() {
        Collection<? extends GrantedAuthority> authorities = getAuthorities(this);
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                new UserPrincipal(this, authorities),
                this.getAuthToken(), authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
