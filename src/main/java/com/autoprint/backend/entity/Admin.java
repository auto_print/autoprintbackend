package com.autoprint.backend.entity;

import com.autoprint.backend.core.entity.JpaEntity;
import com.autoprint.backend.core.security.UserAware;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "admin")
public class Admin implements UserAware<Long>, JpaEntity<Long> {

    public enum Status {
        ACTIVE,
        INACTIVE
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String token;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    @ManyToMany(fetch = EAGER, cascade = ALL)
    @JoinTable(
            name = "admins_roles_roles",
            joinColumns = @JoinColumn(
                    name = "admin_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id"))
    private Set<AdminRole> roles = new HashSet();

    private LocalDateTime dateCreated = LocalDateTime.now();

    private LocalDateTime lastUpdated = LocalDateTime.now();

    public Long getId() {
        return id;
    }

    @Override
    public String friendlyName() {
        return "Admin-" + getId();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<AdminRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<AdminRole> roles) {
        this.roles = roles;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public boolean isUser() {
        return false;
    }

    @Override
    public boolean isVendor() {
        return false;
    }

    @Override
    public boolean isSuper() {
        return true;
    }

    @Override
    public boolean isSystem() {
        return false;
    }

    @Override
    public String getCredUsername() {
        return this.username;
    }

    @Override
    public String getAuthToken() {
        return this.token;
    }
}
