package com.autoprint.backend.entity;

import com.autoprint.backend.core.entity.JpaEntity;
import com.autoprint.backend.core.security.UserAware;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "email_verification")
public class EmailVerification implements JpaEntity<Long> {

    public enum Type {
        FORGOTPASSWORD
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(optional = false, fetch = EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    @NotNull
    private String code;

    @NotNull
    private String email;

    @NotNull
    private Boolean verified;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Type type;

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    private LocalDateTime dateCreated = LocalDateTime.now();

    private LocalDateTime lastUpdated = LocalDateTime.now();

    @Override
    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    @Override
    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String friendlyName() {
        return "EMAILVERIFICATON-" + this.getId();
    }
}
