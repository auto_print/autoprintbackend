package com.autoprint.backend.entity;

import com.autoprint.backend.core.entity.JpaEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "document_properties")
public class DocumentProperties implements JpaEntity<Long> {

    // we dont want to make this as enum for now
    // duplex "NO_DUPLEX""LONG_EDGE""SHORT_EDGE"
    public final static String no_duplex = "NO_DUPLEX";
    public final static String duplex_long_edge = "LONG_EDGE";
    public final static String duplex_short_edge = "SHORT_EDGE";

    // page orientation "PORTRAIT""LANDSCAPE""AUTO"
    public final static String orientation_auto = "AUTO";
    public final static String orientation_landscape = "LANDSCAPE";
    public final static String orientation_portrait = "PORTRAIT";

    // colour "STANDARD_COLOR""STANDARD_MONOCHROME""AUTO"
    public final static String colour_auto = "AUTO";
    public final static String colour_monochrome = "STANDARD_MONOCHROME";

    public final static String colour_white = "WHITE";
    public final static String hardCover = "HARD";
    public final static String softCover = "SOFT";

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NotNull
    @OneToOne
    @JoinColumn(name = "document_id", referencedColumnName = "id", unique = true)
    private Document document;

    @NotNull
    private Long pages;

    @NotNull
    private String colour;

    @NotNull
    private String duplex;

    @NotNull
    private String pageOrientation;

    @NotNull
    private Long copies;

    @NotNull
    private String coverType;

    @NotNull
    private String coverColour;

    private LocalDateTime dateCreated = LocalDateTime.now();

    private LocalDateTime lastUpdated = LocalDateTime.now();

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Long getPages() {
        return pages;
    }

    public BigDecimal getPagesBD() {
        return BigDecimal.valueOf(pages);
    }

    public void setPages(Long pages) {
        this.pages = pages;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getDuplex() {
        return duplex;
    }

    public void setDuplex(String duplex) {
        this.duplex = duplex;
    }

    public String getPageOrientation() {
        return pageOrientation;
    }

    public void setPageOrientation(String pageOrientation) {
        this.pageOrientation = pageOrientation;
    }

    public Long getCopies() {
        return copies;
    }

    public BigDecimal getCopiesBD() {
        return BigDecimal.valueOf(copies);
    }

    public void setCopies(Long copies) {
        this.copies = copies;
    }

    public String getCoverType() {
        return coverType;
    }

    public void setCoverType(String coverType) {
        this.coverType = coverType;
    }

    public String getCoverColour() {
        return coverColour;
    }

    public void setCoverColour(String coverColour) {
        this.coverColour = coverColour;
    }

    @Override
    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    @Override
    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String friendlyName() {
        return "DOCUMENTPROPERTIES-" + getId();
    }
}
