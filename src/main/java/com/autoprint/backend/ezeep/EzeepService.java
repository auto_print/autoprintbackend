package com.autoprint.backend.ezeep;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.okhttp.KeyValue;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

@Service
public class EzeepService {

    @Autowired
    private AppConfig appConfig;

    public void setAccessToken() throws IOException {
        HttpClient client = new DefaultHttpClient();
        String url = "https://accounts.ezeep.com/oauth/access_token/";
        HttpPost post = new HttpPost(url);

        //form data
        StringBody stringBody1 = new StringBody("password", ContentType.MULTIPART_FORM_DATA);
        StringBody stringBody2 = new StringBody(appConfig.getEzeepAdminEmail(), ContentType.MULTIPART_FORM_DATA);
        StringBody stringBody3 = new StringBody(appConfig.getEzeepAdminEmailPassword(), ContentType.MULTIPART_FORM_DATA);
        StringBody stringBody4 = new StringBody(appConfig.getEzeepClientId(), ContentType.MULTIPART_FORM_DATA);
        StringBody stringBody5 = new StringBody(appConfig.getEzeepClientSecret(), ContentType.MULTIPART_FORM_DATA);
        StringBody stringBody6 = new StringBody(appConfig.getEzeepScope(), ContentType.MULTIPART_FORM_DATA);

        HttpEntity reqEntity = MultipartEntityBuilder.create()
                .addPart("grant_type", stringBody1)
                .addPart("username", stringBody2)
                .addPart("password", stringBody3)
                .addPart("client_id", stringBody4)
                .addPart("client_secret", stringBody5)
                .addPart("scope", stringBody6)
                .build();

        //header
        post.setHeader("Authorization", appConfig.getEzeepBase64EncodedSecret());
        post.setEntity(reqEntity);
        HttpResponse response = client.execute(post);

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        JsonObject authObject = new JsonParser().parse(result.toString()).getAsJsonObject();

        EzeepTokenHolder.setAccessToken(authObject.get("access_token").getAsString());
    }

    public ArrayList<KeyValue<String>> defaultHeader() {
        ArrayList<KeyValue<String>> header = new ArrayList<>();
        KeyValue<String> authorization = new KeyValue<>();
        authorization.setKey("Authorization");
        authorization.setValue(EzeepTokenHolder.getAccessToken());
        header.add(authorization);
        return header;
    }
}
