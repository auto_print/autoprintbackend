package com.autoprint.backend.ezeep;

// temporary
public class NotificationUtils {

    public static void EzeepDisableNotification(Class c) {
        System.out.println(String.format("%s: Ezeep is disabled!", c.getSimpleName()));
    }

    public static void S3DisableNotification(Class c) {
        System.out.println(String.format("%s: S3 is disabled!", c.getSimpleName()));
    }
}
