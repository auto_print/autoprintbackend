package com.autoprint.backend.ezeep;

/*
    Todo: cron refresh
 */
public class EzeepTokenHolder {
    private static String accessToken = "";

    public static String getAccessToken() {
        return accessToken;
    }

    public static void setAccessToken(String token) {
        accessToken = "Bearer " + token;
        System.out.println("Current access token:" + accessToken);
    }
}
