package com.autoprint.backend.ezeep.services;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.ezeep.EzeepService;
import com.autoprint.backend.ezeep.EzeepTokenHolder;
import com.autoprint.backend.ezeep.services.dto.EzeepDocumentCreateDto;
import com.autoprint.backend.ezeep.services.dto.EzeepDocumentSubmitPrintjob;
import com.autoprint.backend.okhttp.BuilderHttp;
import com.autoprint.backend.okhttp.KeyValue;
import com.autoprint.backend.okhttp.OkHttpModel;
import com.autoprint.backend.okhttp.OkHttpResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.autoprint.backend.okhttp.EMethod.POST;

@Service
public class EzeepExternalDocumentService {

    @Autowired
    private AppConfig appConfig;
    @Autowired
    private EzeepService ezeepService;

    private String documentServiceUrl;

    @PostConstruct
    public void init() {
        documentServiceUrl = appConfig.getEzeepPortalBaseUrl() + "/documents/";
    }

    public String create(String documentName) throws IOException {

        ArrayList<KeyValue<String>> header = ezeepService.defaultHeader();

        EzeepDocumentCreateDto dto = new EzeepDocumentCreateDto();
        dto.name = documentName;

        OkHttpResponse reponse = BuilderHttp.create()
                .endpoint(OkHttpModel.create()
                        .requestHeader(header)
                        .requestEntity(new Gson().toJson(dto))
                        .endpoint(documentServiceUrl)
                        .method(POST))
                .execute();

        //TODO: log
        System.out.println(reponse.getBody());
        JsonObject reponseObj = new JsonParser().parse(reponse.getBody()).getAsJsonObject();
        return reponseObj.get("id").getAsString();
    }

    public void upload(String documentId) throws IOException {
        //test @ override
        documentId = "93329c36-3d1c-4ed8-b472-f160e39c9707";

        String url = String.format("%s%s/payload/", documentServiceUrl, documentId);

        HttpClient client = new DefaultHttpClient();
        HttpPut post = new HttpPut(url);

        StringBody stringBody = new StringBody("", ContentType.MULTIPART_FORM_DATA);

        HttpEntity reqEntity = MultipartEntityBuilder.create()
                .addPart("file", stringBody)
                .build();

        //header
        post.setHeader("Authorization", EzeepTokenHolder.getAccessToken());
        post.setEntity(reqEntity);
        HttpResponse response = client.execute(post);

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

    }

    public void finishUpload() {

    }

    public String submit(String ezeepPrinterId, String ezeeepDocumentId) throws IOException {

        String url = documentServiceUrl + "%s/print/";

        ArrayList<KeyValue<String>> header = ezeepService.defaultHeader();

        EzeepDocumentSubmitPrintjob dto = new EzeepDocumentSubmitPrintjob();
        dto.printer = ezeepPrinterId;

        OkHttpResponse reponse = BuilderHttp.create()
                .endpoint(OkHttpModel.create()
                        .requestHeader(header)
                        .requestEntity(new Gson().toJson(dto))
                        .pathParam(url, ezeeepDocumentId)
                        .method(POST))
                .execute();

        //TODO: log
        System.out.println(reponse.getBody());
        JsonObject reponseObj = new JsonParser().parse(reponse.getBody()).getAsJsonObject();
        return reponseObj.get("printjob").getAsString();
    }
}
