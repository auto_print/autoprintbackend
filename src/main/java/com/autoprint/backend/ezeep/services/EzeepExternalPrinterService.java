package com.autoprint.backend.ezeep.services;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.ezeep.EzeepService;
import com.autoprint.backend.okhttp.BuilderHttp;
import com.autoprint.backend.okhttp.KeyValue;
import com.autoprint.backend.okhttp.OkHttpModel;
import com.autoprint.backend.okhttp.OkHttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;

import static com.autoprint.backend.okhttp.EMethod.GET;

@Service
public class EzeepExternalPrinterService {

    @Autowired
    private AppConfig appConfig;
    @Autowired
    private EzeepService ezeepService;

    private String printerServiceUrl;

    @PostConstruct
    public void init() {
        printerServiceUrl = appConfig.getEzeepPortalBaseUrl() + "/printers/";
    }

    public void getPrinters() throws IOException {
        ArrayList<KeyValue<String>> header = ezeepService.defaultHeader();

        OkHttpResponse reponse = BuilderHttp.create()
                .endpoint(OkHttpModel.create()
                        .requestHeader(header)
                        .endpoint(printerServiceUrl)
                        .method(GET))
                .execute();

        System.out.println(reponse.getBody());
    }

    public void getPrinterById(String id) throws IOException {

        String url = printerServiceUrl + "%s/";

        ArrayList<KeyValue<String>> header = ezeepService.defaultHeader();

        OkHttpResponse reponse = BuilderHttp.create()
                .endpoint(OkHttpModel.create()
                        .requestHeader(header)
                        .pathParam(url, id)
                        .method(GET))
                .execute();
        System.out.println(reponse.getBody());
    }

}
