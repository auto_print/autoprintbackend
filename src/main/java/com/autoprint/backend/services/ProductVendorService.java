package com.autoprint.backend.services;

import com.autoprint.backend.entity.Product;
import com.autoprint.backend.entity.ProductVendor;
import com.autoprint.backend.entity.Vendor;
import com.autoprint.backend.repo.ProductVendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;

@Service
public class ProductVendorService extends BaseEntityService<ProductVendor, Long> {

    @Autowired
    private ProductVendorRepository productVendorRepository;
    @Autowired
    private VendorService vendorService;

    @Override
    protected PagingAndSortingRepository<ProductVendor, Long> getRepository() {
        return productVendorRepository;
    }

    @Override
    protected void preCreate(ProductVendor entity) {
        failIfNull(entity.getProduct(), "product");
        failIfNull(entity.getPrice(), "price");
        if (getAuthenticatedUser().isSuper()) {
            failIfNull(entity.getVendor(), "vendor");
        } else {
            if (entity.getVendor() == null) {
                Vendor vendor = vendorService.getByUserId((Long) getAuthenticatedUser().getId());
                entity.setVendor(vendor);
            }
        }
        normalize(entity);
    }

    @Override
    public ProductVendor create(ProductVendor productVendor) {
        return super.create(productVendor);
    }

    @Override
    protected void normalize(ProductVendor entity) {
        entity.setActive(true);
    }

    public ProductVendor update(ProductVendor productVendor, Long id) {
        productVendor.setId(id);
        return super.update(productVendor);
    }

    @Override
    protected ProductVendor preUpdate(ProductVendor pEntity, ProductVendor entity) {

        if (isChanged(pEntity.getPrice(), entity.getPrice())) {
            if (entity.getPrice().compareTo(BigDecimal.ZERO) < 0) {
                throw new IllegalArgumentException("Value must be positive!");
            }
            pEntity.setPrice(entity.getPrice());
        }

        if (isChanged(pEntity.isActive(), entity.isActive())) {
            pEntity.setActive(entity.isActive());
        }
        return pEntity;
    }

    public ProductVendor getById(Long id) {
        return super.getById(id);
    }

    public ProductVendor getByProductIdAndVendorId(Long productId, Long vendorId) {
        return productVendorRepository.findByProductIdAndVendorIdAndActive(productId, vendorId, true);
    }

    public Page<ProductVendor> getByVendorId(Long vendorId, Pageable pageable) {
        return productVendorRepository.findByVendorId(vendorId, pageable);
    }
}
