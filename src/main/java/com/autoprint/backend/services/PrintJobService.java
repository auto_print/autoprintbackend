package com.autoprint.backend.services;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.entity.PrintJob;
import com.autoprint.backend.entity.Vendor;
import com.autoprint.backend.exception.EzeepFailedException;
import com.autoprint.backend.ezeep.NotificationUtils;
import com.autoprint.backend.ezeep.services.EzeepExternalDocumentService;
import com.autoprint.backend.repo.PrintJobRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class PrintJobService extends BaseEntityService<PrintJob, Long> {

    @Autowired
    private PrintJobRepository printJobRepository;
    @Autowired
    private VendorService vendorService;
    @Autowired
    private PrinterService printerService;
    @Autowired
    private UserService userService;
    @Autowired
    private EzeepExternalDocumentService ezeepExternalDocumentService;
    @Autowired
    private AppConfig appConfig;
    @Autowired
    private DocumentService documentService;

    @Override
    protected PagingAndSortingRepository<PrintJob, Long> getRepository() {
        return printJobRepository;
    }

    @Override
    protected void preCreate(PrintJob entity) {
        //we want only internal/admin to set this up
        failIfUser();
        failIfVendor();
        entity.setDocument(documentService.getById(entity.getDocument().getId()));
        entity.setVendor(entity.getDocument().getOrder().getVendor());
        // TODO: support checking
        entity.setPrinter(printerService.getAvailablePrinterOfVendor(entity.getVendor()));

        String ezeepPrintJobId = "";
        if (appConfig.getAppModeProd().equals(appConfig.getAppMode())) {

            try {
                ezeepPrintJobId = ezeepExternalDocumentService.submit(entity.getPrinter().getEzeepPrinterId(), entity.getDocument().getEzeepDocumentId());
            } catch (IOException e) {
                // TODO: log
                e.printStackTrace();
            }

            if (StringUtils.isBlank(ezeepPrintJobId)) {
                // TODO: verify rollback
                throw new EzeepFailedException("Failed to save printjob to Ezeep Cloud");
            }
        } else {
            NotificationUtils.EzeepDisableNotification(this.getClass());
        }
        entity.setEzeepPrintJobId(ezeepPrintJobId);
        normalize(entity);
    }

    public PrintJob create(PrintJob printJob) {
        return super.create(printJob);
    }

    @Override
    protected void postCreate(PrintJob printJob) {
        super.postCreate(printJob);
    }

    @Override
    protected void normalize(PrintJob entity) {
        entity.setStatus(PrintJob.Status.PENDING);
    }

    @Override
    protected PrintJob preUpdate(PrintJob pEntity, PrintJob entity) {
        // only update status are allowed!
        if (isChanged(pEntity.getStatus(), entity.getStatus())) {
            pEntity.setStatus(entity.getStatus());
        }
        return pEntity;
    }

    public PrintJob update(PrintJob printJob, Long id) {
        printJob.setId(id);
        return super.update(printJob);
    }

    @Override
    protected void postUpdate(PrintJob printJob) {
        // TODO: if cancel, cancel printjob
    }

    public PrintJob getById(Long id) {
        return super.getById(id);
    }

    public Page<PrintJob> getByVendorId(Long vendorId, Pageable pageable) {
        failIfUser();
        Vendor vendor = vendorService.getById(vendorId);
        return printJobRepository.findByVendor(vendor, pageable);
    }

    public PrintJob getByDocumentId(Long documentId) {
        return printJobRepository.findByDocumentId(documentId);
    }

    public void cancelJob(Long documentId) {
        PrintJob pPrintJob = getByDocumentId(documentId);
        PrintJob printJob = new PrintJob();
        printJob.setStatus(PrintJob.Status.CANCELLED);
        this.update(printJob, pPrintJob.getId());
    }
}
