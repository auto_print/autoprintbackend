package com.autoprint.backend.services;

import com.autoprint.backend.api.dto.DocumentCreateDto;
import com.autoprint.backend.entity.*;
import com.autoprint.backend.repo.ProductRepository;
import com.autoprint.backend.repo.specification.GenericEntitySpecification;
import com.autoprint.backend.repo.specification.SearchCriteria;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.autoprint.backend.entity.Product.*;

@Service
public class ProductService extends BaseEntityService<Product, Long> {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private DocumentService documentService;

    @Override
    protected PagingAndSortingRepository<Product, Long> getRepository() {
        return productRepository;
    }

    @Override
    protected void preCreate(Product entity) {
        failIfNull(entity.getProductName(), "productName");
    }

    @Override
    public Product create(Product product) {
        return super.create(product);
    }

    @Override
    protected void normalize(Product entity) {

    }

    @Override
    protected Product preUpdate(Product pEntity, Product entity) {
        throw new UnsupportedOperationException("Update not supported!");
    }

    public Product findProduct(DocumentCreateDto dto) {
        // FIXME: hackish and hassle to maintain. Write algo for this
        return handleColour(dto);
    }

    // FIXME
    private Product handleColour(DocumentCreateDto dto) {

        if (StringUtils.isBlank(dto.colour)) {
            return getById(100L);
        }

        if (DocumentProperties.colour_auto.equalsIgnoreCase(dto.colour)) {
            return getById(100L);
        } else {
            return getById(101L);
        }
    }

    public Product getTaxProduct() {
        return getById(GST);
    }

    public Product getServiceProduct() {
        return getById(SERVICE_CHARGES);
    }

    public Set<Product> getListOfProductByOrderId(Long orderId) {
        List<Document> listOfDocument = documentService.getByOrderId(orderId);
        Set<Product> result = listOfDocument.stream().map(Document::getProduct).filter(product -> !(GST.equals(product.getId())
                || SERVICE_CHARGES.equals(product.getId()))).collect(Collectors.toSet());
        failIfEmpty(result, "documents");
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Product> get(Pageable pageable) {
        GenericEntitySpecification<User> specification = new GenericEntitySpecification<>();
        specification.addCriteria(getFilter(GST));
        specification.addCriteria(getFilter(SERVICE_CHARGES));
        return productRepository.findAll(specification, pageable);
    }

    private SearchCriteria getFilter(Long id) {
        SearchCriteria criteria = new SearchCriteria();
        criteria.setKey("id");
        criteria.setOperation("!:");
        criteria.setValue(id);
        return criteria;
    }
}
