package com.autoprint.backend.services;

import com.autoprint.backend.entity.AdminPrivilege;
import com.autoprint.backend.entity.AdminRole;
import com.autoprint.backend.repo.AdminPrivilegeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class AdminPrivilegeService extends BaseEntityService<AdminPrivilege, Long> {

    @Autowired
    private AdminPrivilegeRepository adminPrivilegeRepository;

    @Override
    protected JpaRepository<AdminPrivilege, Long> getRepository() {
        return this.adminPrivilegeRepository;
    }

    @Override
    protected void preCreate(AdminPrivilege entity) {
        failIfBlank(entity.getName(), "name");
        entity.setName(entity.getName().toUpperCase());
        // Not allowing bidirectional update
        entity.getRoles().clear();
    }

    @Override
    protected void normalize(AdminPrivilege entity) {

    }

    @PreAuthorize("hasAuthority('ADMIN_CREATE')")
    @Override
    public AdminPrivilege create(AdminPrivilege entity) {
        return super.create(entity);
    }

    @PreAuthorize("hasAuthority('ADMIN_CREATE')")
    public AdminPrivilege createIfNotExist(AdminPrivilege entity) {
        Optional<AdminPrivilege> _adminPrivilege = this.getByName(entity.getName());
        return _adminPrivilege.orElseGet(() -> this.create(entity));
    }

    @Override
    protected AdminPrivilege preUpdate(AdminPrivilege entity, AdminPrivilege pEntity) {
        if (isChanged(pEntity.getName(), entity.getName())) {
            pEntity.setName(entity.getName().toUpperCase());
        }
        return pEntity;
    }

    @PreAuthorize("hasAuthority('ADMIN_UPDATE')")
    @Override
    public AdminPrivilege update(AdminPrivilege changeSet) {
        return super.update(changeSet);
    }

    @PreAuthorize("hasAuthority('ADMIN_READ')")
    @Override
    public AdminPrivilege getById(Long id) {
        return super.getById(id);
    }

    @PreAuthorize("hasAuthority('ADMIN_READ')")
    public Page<AdminPrivilege> get(Pageable pageable, String name) {
        Page<AdminPrivilege> adminPrivilegePage = adminPrivilegeRepository.findByNameLike("%" + name + "%", pageable);
        adminPrivilegePage.forEach(p -> p.setRoles(null));
        return adminPrivilegePage;
    }

    @PreAuthorize("hasAuthority('ADMIN_READ')")
    @Transactional(readOnly = true)
    public Page<AdminPrivilege> getAll(Pageable pageable) {
        return getRepository().findAll(pageable);
    }

    @PreAuthorize("hasAuthority('ADMIN_READ')")
    public Optional<AdminPrivilege> getByName(String name) {
        return adminPrivilegeRepository.findByName(name);
    }

}
