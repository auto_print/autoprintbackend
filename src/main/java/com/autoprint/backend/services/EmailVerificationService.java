package com.autoprint.backend.services;

import com.autoprint.backend.entity.EmailVerification;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.repo.EmailVerificationRepository;
import com.autoprint.backend.utils.SecureUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

@Service
public class EmailVerificationService extends BaseEntityService<EmailVerification, Long> {

    @Autowired
    private EmailVerificationRepository emailVerificationRepository;

    @Override
    protected PagingAndSortingRepository<EmailVerification, Long> getRepository() {
        return emailVerificationRepository;
    }

    @Override
    protected void preCreate(EmailVerification entity) {
        failIfBlank(entity.getEmail(), "email");
        failIfNull(entity.getType(), "type");
        failIfNull(entity.getUser(), "user");

        String code = SecureUtils.generateTokenAlphaAlphabet(6);
        entity.setCode(code);

        normalize(entity);
    }

    @Override
    public EmailVerification create(EmailVerification emailVerification) {
        return super.create(emailVerification);
    }

    @Override
    protected void normalize(EmailVerification entity) {
        entity.setVerified(false);
    }

    @Override
    protected EmailVerification preUpdate(EmailVerification pEntity, EmailVerification entity) {
        return null;
    }

    public EmailVerification getLatestByCode(String code) {
        return emailVerificationRepository.findFirstByCodeAndVerifiedOrderByDateCreatedDesc(code, false);
    }

    public EmailVerification getLatestByUser(User user) {
        return emailVerificationRepository.findFirstByUserOrderByDateCreatedDesc(user);
    }

    public void verified(EmailVerification emailVerification) {
        // fixme
        emailVerification.setVerified(true);
        emailVerificationRepository.save(emailVerification);
    }

    @Override
    public EmailVerification getById(Long id) {
        return super.getById(id);
    }
}
