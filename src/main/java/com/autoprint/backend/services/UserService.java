package com.autoprint.backend.services;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.api.dto.UserDto;
import com.autoprint.backend.api.dto.VerifyCodeResponseDto;
import com.autoprint.backend.communication.SendEmail;
import com.autoprint.backend.core.utils.SHA256;
import com.autoprint.backend.entity.Activity;
import com.autoprint.backend.entity.EmailVerification;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.entity.Wallet;
import com.autoprint.backend.exception.EmailExistException;
import com.autoprint.backend.exception.PermissionDeniedException;
import com.autoprint.backend.exception.UsernameExistException;
import com.autoprint.backend.helper.UploadHelper;
import com.autoprint.backend.object.EmailObject;
import com.autoprint.backend.repo.UserRepository;
import com.autoprint.backend.utils.SecureUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import static java.lang.String.format;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@Service
public class UserService extends BaseEntityService<User, Long> {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private WalletService walletService;
    @Autowired
    private UploadHelper uploadHelper;
    @Autowired
    private SendEmail sendEmail;
    @Autowired
    private EmailVerificationService emailVerificationService;

    @Override
    protected PagingAndSortingRepository<User, Long> getRepository() {
        return userRepository;
    }

    @Override
    protected void preCreate(User user) {
        failIfBlank(user.getUsername(), "username");
        failIfBlank(user.getPassword(), "password");
        failIfBlank(user.getEmail(), "email");
        failIfBlank(user.getFullname(), "fullname");

        try {
            user.setPassword(SHA256.hash(user.getPassword()));
        } catch (Exception e) {
            throw new RuntimeException("Failed to encode password for user");
        }

        normalize(user);

        try {
            if (user.isVendor()) {
                user.setToken("VENDOR_" + SHA256.hash(randomAlphanumeric(32)));
            } else {
                user.setToken("USER_" + SHA256.hash(randomAlphanumeric(32)));
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed to generate token for user");
        }
    }

    @Override
    protected void normalize(User user) {

        if (user.getVendor() == null) {
            user.setVendor(false);
        }

        user.setActive(true);
    }

    @PreAuthorize("hasAuthority('USER_CREATE')")
    @Transactional
    public User create(User user) {
        usernameExist(user.getUsername());
        emailExist(user.getEmail());
        return super.create(user);
    }

    public void postCreate(User user) {
        // initial welcome message
        Activity activity = new Activity();
        activity.setTitle("Welcome to AUTOPRINT!");
        activity.setContent("Hello there. Welcome to AUTOPRINT! Feel free to contact us if any problem occur :)");
        activity.setUser(user);
        activity.setType(Activity.Type.NOTIFICATION);
        activityService.create(activity);

        Wallet wallet = new Wallet();
        wallet.setUser(user);
        wallet = walletService.create(wallet);
        user.setWallet(wallet);

        // send email :P (async)

        super.postCreate(user);
    }

    @Override
    protected User preUpdate(User pEntity, User entity) {

        if (isChanged(pEntity.getUsername(), entity.getUsername())) {
            failIfBlank(entity.getUsername(), "username");
            usernameExist(entity.getUsername());
            pEntity.setUsername(entity.getUsername());
        }

        if (isChanged(pEntity.getEmail(), entity.getEmail())) {
            failIfBlank(entity.getEmail(), "email");
            emailExist(entity.getEmail());
            pEntity.setEmail(entity.getEmail());
        }

        if (isChanged(pEntity.getFullname(), entity.getFullname())) {
            failIfBlank(entity.getFullname(), "fullname");
            pEntity.setFullname(entity.getFullname());
        }

        if (isChanged(pEntity.getPhoneNumber(), entity.getPhoneNumber(), true)) {
            failIfAlphabet(entity.getPhoneNumber(), "phone number", true);
            pEntity.setPhoneNumber(entity.getPhoneNumber());
        }

        if (isChanged(pEntity.isActive(), entity.isActive())) {
            pEntity.setActive(entity.isActive());
        }

        // naive
        if (StringUtils.isNotBlank(entity.getImageBase64())) {
            String dir = String.format("avatar/%s", pEntity.getId());
            String fileName = SecureUtils.generateTokenAlphaAlphabet(12);
            try {
                fileName = uploadHelper.upload(entity.getImageBase64(), dir, fileName);
            } catch (Exception e) {
                throw new IllegalArgumentException("Failed to determine image type!");
            }
            pEntity.setAvatarRef(fileName);
        }

        return pEntity;
    }

    @PreAuthorize("hasAuthority('USER_UPDATE')")
    @Transactional
    public User update(User user, Long id) {
        user.setId(id);
        return super.update(user);
    }

    @Override
    protected void postUpdate(User user) {
        super.postUpdate(user);
    }

    @PreAuthorize("hasAuthority('USER_READ')")
    @Override
    public User getById(Long id) {
        return super.getById(id);
    }


    @PreAuthorize("hasAuthority('USER_UPDATE')")
    @Transactional
    public User updatePassword(String currentPassword, String newPassword) {
        failIfBlank(newPassword, "password");
        Long id = (Long) getAuthenticatedUser().getId();
        User user = getById(id);
        try {
            String currentSha256password = SHA256.hash(currentPassword);
            if (currentSha256password.equals(user.getPassword())) {
                String newSha256password = SHA256.hash(newPassword);
                if (isChanged(user.getPassword(), newSha256password)) {
                    user.setPassword(newSha256password);
                    return userRepository.save(user);
                }
            } else {
                throw new Exception("Password mismatch");
            }
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            throw new PermissionDeniedException(e.getLocalizedMessage());
        }

        return null;
    }

    @Transactional
    public void forgetPassword(String email) {
        User user = userRepository.findByEmail(email);
        // ignore for now... fix later
        if (user == null) {
            return;
        }

        EmailVerification emailVerification = new EmailVerification();
        emailVerification.setUser(user);
        emailVerification.setEmail(user.getEmail());
        emailVerification.setType(EmailVerification.Type.FORGOTPASSWORD);
        emailVerification = emailVerificationService.create(emailVerification);

        // trigger email
        EmailObject emailObject = new EmailObject();
        emailObject.setSubject("Autoprint: Reset Password");
        emailObject.setRecipient(user.getEmail());
        emailObject.setText(String.format("Your forgot password code is %s", emailVerification.getCode()));
        sendEmail.sendEmail(emailObject);
    }

    @Transactional
    public VerifyCodeResponseDto verifyCode(String code) {
        VerifyCodeResponseDto dto = new VerifyCodeResponseDto();
        EmailVerification pEmailVerification = emailVerificationService.getLatestByCode(code);
        if (pEmailVerification == null) {
            dto.verified = false;
        } else {
            // reset password
            User pUser = pEmailVerification.getUser();
            /*pUser.setPassword("");
            userRepository.save(pUser);*/
            dto.userId = pUser.getId();
            dto.verified = true;

            // update to verified
            emailVerificationService.verified(pEmailVerification);
        }
        return dto;
    }

    @Transactional
    public User setNewPassword(Long id, String newPassword) {
        User pUser = this.getById(id);

        EmailVerification pEmailVerification = emailVerificationService.getLatestByUser(pUser);
        if (!pEmailVerification.getVerified()) {
            throw new IllegalStateException("Latest code are not verified!");
        }

        String newSha256password = null;
        try {
            newSha256password = SHA256.hash(newPassword);
            pUser.setPassword(newSha256password);
            return userRepository.save(pUser);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            // fixme
            e.printStackTrace();
        }
        return null;
    }

    @Deprecated
    private void mayExist(User user) {
        User pUser = userRepository.findByEmailOrUsername(user.getEmail(), user.getUsername());
        if (pUser != null) {
            if (pUser.getEmail().equals(user.getEmail())) {
                throw new EntityExistsException("Email already exist!");
            } else {
                throw new EntityExistsException("User username already exist!");
            }
        }
    }

    private void usernameExist(String username) {
        User pUser = userRepository.findByUsername(username);
        if (pUser != null) {
            throw new UsernameExistException("Username already exist!");
        }
    }

    private void emailExist(String email) {
        User pUser = userRepository.findByEmail(email);
        if (pUser != null) {
            throw new EmailExistException("Email already exist!");
        }
    }

    @Transactional(readOnly = true)
    public Page<User> get(Specification specification, Pageable pageable) {
        return userRepository.findAll(specification, pageable);
    }
}
