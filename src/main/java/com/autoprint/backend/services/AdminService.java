package com.autoprint.backend.services;

import com.autoprint.backend.core.utils.SHA256;
import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.entity.AdminRole;
import com.autoprint.backend.repo.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@Service
public class AdminService extends BaseEntityService<Admin, Long> {

    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private AdminRoleService adminRoleService;

    @Override
    protected PagingAndSortingRepository<Admin, Long> getRepository() {
        return adminRepository;
    }

    @Override
    protected void preCreate(Admin entity) {
        failIfBlank(entity.getUsername(), "admin");
        failIfBlank(entity.getPassword(), "admin");

        entity.setStatus(Admin.Status.ACTIVE);
        try {
            entity.setPassword(SHA256.hash(entity.getPassword()));
        } catch (Exception e) {
            throw new RuntimeException("Failed to encode password for admin");
        }

        try {
            entity.setToken("superadmin_" + SHA256.hash(randomAlphanumeric(32)));
        } catch (Exception e) {
            throw new RuntimeException("Failed to generate token for admin");
        }
    }

    @Override
    protected void normalize(Admin entity) {
    }

    @PreAuthorize("hasAuthority('ADMIN_CREATE')")
    @Transactional
    public Admin create(Admin entity) {
        return super.create(entity);
    }

    @PreAuthorize("hasAuthority('ADMIN_UPDATE')")
    @Transactional
    public Admin revoke(Long id) {
        Admin admin = new Admin();
        admin.setStatus(Admin.Status.INACTIVE);
        return this.update(admin, id);
    }

    @Transactional
    public Admin reactivate(Long id) {
        Admin admin = new Admin();
        admin.setStatus(Admin.Status.ACTIVE);
        return this.update(admin, id);
    }

    @PreAuthorize("hasAuthority('ADMIN_UPDATE')")
    @Transactional
    public Admin addRoleToAdmin(Long id, Long roleID) {
        assert id != null;
        failIfIdInvalid(id);
        Admin pEntity = this.getById(id);
        AdminRole role = adminRoleService.getById(roleID);
        pEntity.getRoles().add(role);
        pEntity = getRepository().save(pEntity);

        return pEntity;
    }

    @PreAuthorize("hasAuthority('ADMIN_UPDATE')")
    @Transactional
    public Admin removeRoleFromAdmin(Long id, Long roleID) {
        assert id != null;
        failIfIdInvalid(id);
        Admin pEntity = this.getById(id);
        AdminRole role = adminRoleService.getById(roleID);
        pEntity.getRoles().remove(role);
        pEntity = getRepository().save(pEntity);

        return pEntity;
    }

    @Override
    protected void postCreate(Admin admin) {
        super.postCreate(admin);
    }

    @Override
    protected Admin preUpdate(Admin pEntity, Admin entity) {

        if (isChanged(pEntity.getUsername(), entity.getUsername())) {
            failIfBlank(entity.getUsername(), "admin");
            pEntity.setUsername(entity.getUsername());
        }

        if (isChanged(pEntity.getStatus(), entity.getStatus())) {
            pEntity.setStatus(entity.getStatus());
        }

        return pEntity;
    }

    @PreAuthorize("hasAuthority('ADMIN_UPDATE')")
    public Admin update(Admin changeset, Long id) {
        changeset.setId(id);
        return super.update(changeset);
    }

    @Override
    protected void postUpdate(Admin admin) {
        super.postUpdate(admin);
    }

    @PreAuthorize("hasAuthority('ADMIN_READ')")
    @Override
    public Admin getById(Long id) {
        return super.getById(id);
    }

    public Admin get() {
        Admin admin = getById((Long) getAuthenticatedUser().getId());
        return admin;
    }

    @PreAuthorize("hasAuthority('ADMIN_READ')")
    @Transactional(readOnly = true)
    public Page<Admin> get(Specification specification, Pageable pageable) {
        return adminRepository.findAll(specification, pageable);
    }
}
