package com.autoprint.backend.services;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.core.security.UserAware;
import com.autoprint.backend.core.utils.SHA256;
import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.entity.Vendor;
import com.autoprint.backend.repo.AdminRepository;
import com.autoprint.backend.repo.UserRepository;
import com.autoprint.backend.repo.VendorRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Service
public class LoginService {

    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VendorRepository vendorRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AppConfig appConfig;

    @Transactional(readOnly = true)
    public UserAware login(String apiKey) {
        if (isMeantForAdmin(apiKey)) {
            return adminRepository.findByTokenAndStatus(apiKey, Admin.Status.ACTIVE);
        } else {
            return userRepository.findByTokenAndActive(apiKey, true);
        }
    }

    @Transactional(readOnly = true)
    public UserAware loginBasicAuthentication(String authorization) {
        String base64Credentials = authorization.substring("Basic".length()).trim();
        byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
        String credentials = new String(credDecoded, StandardCharsets.UTF_8);
        final String[] values = credentials.split(":", 2);
        return login(values[0], values[1]);
    }

    @Transactional(readOnly = true)
    public UserAware<Long> login(String username, String password) {

        if (!appConfig.getAppModeDev().equals(appConfig.getAppMode())) {
            String sha256password = "";
            try {
                sha256password = SHA256.hash(password);
            } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            if (StringUtils.isNotBlank(sha256password)) {
                Admin admin = adminRepository.findByUsernameAndPasswordAndStatus(username, sha256password, Admin.Status.ACTIVE);
                if (admin != null) {
                    return admin;
                }

                User user = userRepository.findByUsernameAndPasswordAndActive(username, sha256password, true);
                if (user != null) {
                    return user;
                }
            }
        } else {

            User user = userRepository.findByUsernameAndActive(username, true);
            if (user != null) {
                return user;
            }

            Admin admin = adminRepository.findByUsernameAndStatus(username, Admin.Status.ACTIVE);
            if (admin != null) {
                return admin;
            }
        }
        return null;
    }

    /*// TODO: we may want to support for user as well in future
    @Transactional
    public Admin googleSignIn(GoogleSignInRequest googleSignInRequest) {
        GoogleIdToken idToken = googleService.getPayloadByToken(googleSignInRequest.token);
        if (idToken == null) {
            throw new IllegalStateException(format("Fail to retrieve Google ID token"));
        }

        GoogleIdToken.Payload payload = idToken.getPayload();
        Admin pAdmin = adminRepository.findByUsername(payload.getEmail());
        if (pAdmin == null) {
            Admin admin = new Admin();
            admin.setUsername(payload.getEmail());
            admin.setPassword(randomAlphanumeric(16));
            //admin.setRoles(null);

            // Override auth user (anonymous)
            SystemUser.getInstance().runAs();

            pAdmin = adminService.create(admin);
        } else {
            failAccessUnless(pAdmin.getStatus() == Admin.Status.ACTIVE, "authentication", Admin.class);
        }
        return pAdmin;
    }*/

    private boolean isMeantForAdmin(String apiKey) {
        return apiKey.startsWith("superadmin_");
    }

    private boolean isMeantForUser(String apiKey) {
        return apiKey.startsWith("USER_");
    }

    private boolean isMeantForVendor(String apiKey) {
        return apiKey.startsWith("VENDOR_");
    }
}
