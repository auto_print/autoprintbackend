package com.autoprint.backend.services;

import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.entity.AdminPrivilege;
import com.autoprint.backend.entity.AdminRole;
import com.autoprint.backend.repo.AdminRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
public class AdminRoleService extends BaseEntityService<AdminRole, Long> {

    @Autowired
    private AdminRoleRepository adminRoleRepository;
    @Autowired
    private AdminPrivilegeService adminPrivilegeService;

    @Override
    protected JpaRepository<AdminRole, Long> getRepository() {
        return this.adminRoleRepository;
    }

    @Override
    protected void preCreate(AdminRole entity) {
        failIfBlank(entity.getName(), "name");
        entity.setName(entity.getName().toUpperCase());

        failConstraintUnless(!adminRoleRepository.existsByName(entity.getName()), format("Role name '%s' already exist", entity.getName()));

        if (entity.getPrivileges() != null) {
            Set<AdminPrivilege> _privileges = new HashSet<>();
            entity.getPrivileges().stream()
                    .filter(adminPrivilege -> isNotBlank(adminPrivilege.getName()))
                    .forEach(adminPrivilege -> _privileges.add(adminPrivilegeService.createIfNotExist(adminPrivilege)));
            entity.getPrivileges().clear();
            entity.getPrivileges().addAll(_privileges);
        }
    }

    @Override
    protected void normalize(AdminRole entity) {

    }

    @PreAuthorize("hasAuthority('ADMIN_CREATE')")
    @Override
    public AdminRole create(AdminRole entity) {
        return super.create(entity);
    }

    @Override
    protected AdminRole preUpdate(AdminRole entity, AdminRole pEntity) {
        if (isChanged(pEntity.getName(), entity.getName())) {
            entity.setName(entity.getName().toUpperCase());
            failConstraintUnless(!adminRoleRepository.existsByName(entity.getName()), format("Role name '%s' already exist!", entity.getName()));
            pEntity.setName(entity.getName());
        }
        // TODO: detect collection changes (compare by name with equals)
        if (entity.getPrivileges() != null) {
            Set<AdminPrivilege> _privileges = new HashSet<>();
            entity.getPrivileges().stream()
                    .filter(adminPrivilege -> isNotBlank(adminPrivilege.getName()))
                    .forEach(adminPrivilege -> _privileges.add(adminPrivilegeService.createIfNotExist(adminPrivilege)));
            pEntity.getPrivileges().clear();
            pEntity.getPrivileges().addAll(_privileges);
        }
        return pEntity;
    }

    @PreAuthorize("hasAuthority('ADMIN_UPDATE')")
    @Override
    public AdminRole update(AdminRole changeSet) {
        return super.update(changeSet);
    }

    @PreAuthorize("hasAuthority('ADMIN_READ')")
    @Override
    public AdminRole getById(Long id) {
        return super.getById(id);
    }

    @PreAuthorize("hasAuthority('ADMIN_READ')")
    @Transactional(readOnly = true)
    public Page<AdminRole> get(Pageable pageable) {
        return getRepository().findAll(pageable);
    }

    @PreAuthorize("hasAuthority('ADMIN_UPDATE')")
    @Transactional
    public AdminRole addPrivilegeToRole(Long roleId, Long privilegeId) {
        assert roleId != null;
        failIfIdInvalid(roleId);
        AdminRole pEntity = this.getById(roleId);
        AdminPrivilege privilege = adminPrivilegeService.getById(privilegeId);
        pEntity.getPrivileges().add(privilege);
        pEntity = getRepository().save(pEntity);

        return pEntity;
    }

    @PreAuthorize("hasAuthority('ADMIN_UPDATE')")
    @Transactional
    public AdminRole removePrivilegeFromRole(Long roleId, Long privilegeId) {
        assert roleId != null;
        failIfIdInvalid(roleId);
        AdminRole pEntity = this.getById(roleId);
        AdminPrivilege privilege = adminPrivilegeService.getById(privilegeId);
        pEntity.getPrivileges().remove(privilege);
        pEntity = getRepository().save(pEntity);

        return pEntity;
    }

    @PreAuthorize("hasAuthority('ADMIN_READ')")
    public AdminRole getByName(String roleName) {
        return adminRoleRepository.findByName(roleName);
    }
}
