package com.autoprint.backend.services;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.api.dto.DocumentCreateDto;
import com.autoprint.backend.api.mapper.DocumentMapper;
import com.autoprint.backend.api.mapper.DocumentPropertiesMapper;
import com.autoprint.backend.cron.async.DocumentAsync;
import com.autoprint.backend.entity.Document;
import com.autoprint.backend.entity.DocumentProperties;
import com.autoprint.backend.entity.Order;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.exception.EzeepFailedException;
import com.autoprint.backend.ezeep.NotificationUtils;
import com.autoprint.backend.ezeep.services.EzeepExternalDocumentService;
import com.autoprint.backend.helper.UploadHelper;
import com.autoprint.backend.repo.DocumentRepository;
import com.autoprint.backend.utils.SecureUtils;
import com.autoprint.backend.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

@Service
public class DocumentService extends BaseEntityService<Document, Long> {

    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private EzeepExternalDocumentService ezeepExternalDocumentService;
    @Autowired
    private AppConfig appConfig;
    @Autowired
    private OrderService orderService;
    @Autowired
    private DocumentAsync documentAsync;
    @Autowired
    private UploadHelper uploadHelper;
    @Autowired
    private DocumentMapper documentMapper;
    @Autowired
    private DocumentPropertiesMapper documentPropertiesMapper;
    @Autowired
    private DocumentPropertiesService documentPropertiesService;

    @Override
    protected PagingAndSortingRepository<Document, Long> getRepository() {
        return documentRepository;
    }

    @Override
    protected void preCreate(Document document) {
        failIfVendor();
        failIfNull(document.getOrder(), "order");

        Order order = orderService.getById(document.getOrder().getId());
        if (!Order.Status.PENDING.equals(order.getStatus())) {
            throw new IllegalStateException("Order not in pending state. Cannot add new document!");
        }

        if (document.getUser() == null) {
            if (getAuthenticatedUser().isSuper()) {
                document.setUser(order.getUser());
            } else {
                document.setUser(userService.getById((Long) getAuthenticatedUser().getId()));
            }
        }

        if (document.getFiletype() == null) {
            String type = uploadHelper.extractNaiveFileType(document.getFilename());
            document.setFiletype(type);
        }

        if (document.getFilename() == null) {
            String ref = SecureUtils.generateTokenAlphaAlphabet(8);
            document.setFilename(ref);
        } else {
            String parseName = Utils.parseSpace(document.getFilename());
            document.setFilename(parseName);
        }

        // upload files asyc
        if (document.getData() != null) {
            try {
                documentAsync.uploadDocument(document);
            } catch (Exception e) {
                throw new IllegalArgumentException("Failed to determine image type!");
            }
        } else if (document.getReorderDocument() != null) {
            // copy file
            uploadHelper.copyDocument(document.getReorderDocument(), document);
        }

        String ezeepDocId = "";
        if (appConfig.getAppModeProd().equals(appConfig.getAppMode())) {

            try {
                ezeepDocId = ezeepExternalDocumentService.create(document.getFilename());
            } catch (IOException e) {
                // TODO: log
                e.printStackTrace();
            }

            if (StringUtils.isBlank(ezeepDocId)) {
                // TODO: verify rollback
                throw new EzeepFailedException("Failed to save document to Ezeep Cloud");
            }
        } else {
            NotificationUtils.EzeepDisableNotification(this.getClass());
        }
        document.setEzeepDocumentId(ezeepDocId);
        normalize(document);
    }

    @Override
    protected void normalize(Document document) {
        document.setActive(true);
    }

    @Transactional
    public Document create(DocumentCreateDto documentCreateDto) {
        Document document = this.create(documentMapper.toDocument(documentCreateDto));
        DocumentProperties documentProperties = documentPropertiesMapper.mapDocumentProperties(documentCreateDto, document);
        document.setDocumentProperties(documentPropertiesService.create(documentProperties));
        return document;
    }

    @Override
    public Document create(Document document) {
        return super.create(document);
    }

    @Override
    protected void postCreate(Document document) {
        super.postCreate(document);
    }

    @Override
    protected Document preUpdate(Document pEntity, Document entity) {

        if (isChanged(pEntity.isActive(), entity.isActive())) {
            pEntity.setActive(entity.isActive());
        }

        // HACK
        if (isChanged(pEntity.getDocumentProperties(), entity.getDocumentProperties())) {
            pEntity.setDocumentProperties(entity.getDocumentProperties());
        }

        return pEntity;
    }

    public Document update(Document document, Long id) {
        document.setId(id);
        return super.update(document);
    }

    protected void postUpdate(Document document) {
        super.postUpdate(document);
    }

    public Document getById(Long id) {
        return super.getById(id);
    }

    public Page<Document> getByUserId(Long id, Pageable pageable) {
        failIfVendor();
        User user = userService.getById(id);
        //failAccessUnless(isSame(user), "find");
        return documentRepository.findByUser(user, pageable);
    }

    public Page<Document> getByOrderId(Long orderId, Pageable pageable) {
        Order order = orderService.getById(orderId);
        return documentRepository.findByOrderAndActive(order, true, pageable);
    }

    public List<Document> getByOrderId(Long orderId) {
        return getByOrderId(orderId, true);
    }

    public List<Document> getByOrderId(Long orderId, boolean active) {
        return documentRepository.findByOrderIdAndActive(orderId, active);
    }

    public void cancel(Long id) {
        Document document = new Document();
        document.setActive(false);
        this.update(document, id);
    }
}
