package com.autoprint.backend.services;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.api.dto.ByteArrayDto;
import com.autoprint.backend.api.dto.StatementDto;
import com.autoprint.backend.api.dto.StatementListDto;
import com.autoprint.backend.api.dto.StatementRequestDto;
import com.autoprint.backend.entity.Order;
import com.autoprint.backend.entity.Vendor;
import com.autoprint.backend.object.OrderOverallObject;
import com.autoprint.backend.object.StatementObject;
import com.autoprint.backend.utils.ExportPDFUtils;
import com.autoprint.backend.utils.TimeUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class StatementService extends BaseService {

    @Autowired
    private ExportPDFUtils exportPDFUtils;
    @Autowired
    private VendorService vendorService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private AppConfig appConfig;

    public ByteArrayDto getStatementOfAccount(StatementRequestDto dto) throws IOException {

        File tempFile = null;
        byte[] data = null;

        try {
            String tempFileLoc = System.getProperty("java.io.tmpdir") + "statement_account_temp" + Instant.now().toEpochMilli() + ".pdf";
            Map<String, Object> map = this.mapStatementData(dto);
            tempFile = exportPDFUtils.exportToPdfBox(map, "template_statement_of_account.html", tempFileLoc);
        } catch (Exception e) {
            System.out.println("Failed to convert pdf");
        } finally {
            if (tempFile != null && tempFile.exists()) {
                data = FileUtils.readFileToByteArray(tempFile);
                tempFile.delete();
            }
        }

        if (data != null) {
            return new ByteArrayDto(Base64
                    .getEncoder()
                    .encodeToString(data), data.length);
        }
        throw new RuntimeException("Failed get pdf data!");
    }

    private Map<String, Object> mapStatementData(StatementRequestDto dto) {

        ArrayList<String> monthListInit = TimeUtils.monthListInit();
        int month = monthListInit.indexOf(dto.month);
        int year = Integer.parseInt(dto.year);

        Map<String, Object> map = new HashMap<>();
        Vendor vendor = vendorService.getById(dto.vendorId);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        LocalDateTime start = TimeUtils.getTime(month, year);
        LocalDateTime end;
        if (month == 11) {
            end = TimeUtils.getTime(0, year + 1);
        } else {
            end = TimeUtils.getTime(month + 1, year);
        }

        List<Order> orderList = orderService.getAllVendorOrderInRange(vendor, start, end);

        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal totalDeduction = BigDecimal.ZERO;
        // this might be useful later... refactor
        for (Order order : orderList) {
            // get total amount they get
            amount = amount.add(order.getTotal());
        }
        amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

        // get deduction
        float taxRate = Float.parseFloat(appConfig.getTaxRate());
        float serviceRate = Float.parseFloat(appConfig.getServiceRate());

        BigDecimal tax = amount.multiply(BigDecimal.valueOf(taxRate));
        tax = tax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

        BigDecimal service = amount.multiply(BigDecimal.valueOf(serviceRate));
        service = service.setScale(2, BigDecimal.ROUND_HALF_EVEN);

        totalDeduction = totalDeduction.add(tax).add(service);

        StatementObject obj1 = new StatementObject("Sales", "Credit", amount);
        StatementObject obj2 = new StatementObject("GST", "Debit", tax);
        StatementObject obj3 = new StatementObject("Service Charge", "Debit", service);

        ArrayList<StatementObject> summary = new ArrayList<>();
        summary.add(obj1);
        summary.add(obj2);
        summary.add(obj3);

        map.put("vendorName", vendor.getVendorname());
        map.put("summary", summary);
        map.put("startDate", start.format(dtf));
        map.put("endDate", end.format(dtf));
        map.put("deduction", totalDeduction);
        map.put("net", amount.subtract(totalDeduction));

        return map;
    }

    // TODO: it's own mapper class
    public StatementListDto getStatementList(Long vendorId) {
        StatementListDto statementListDto = new StatementListDto();
        ArrayList<StatementDto> list = new ArrayList<>();

        Vendor vendor = vendorService.getById(vendorId);
        List<OrderOverallObject> overallObjectList = orderService.getVendorOrderStatisticList(vendor);
        for (OrderOverallObject o : overallObjectList) {
            list.add(this.map(o.getMonth(), String.valueOf(o.getDate().getYear())));
        }
        statementListDto.statementList = list;
        return statementListDto;
    }

    // TODO: it's own mapper class
    private StatementDto map(String month, String year) {
        StatementDto statementDto = new StatementDto();
        statementDto.month = month;
        statementDto.year = year;
        return statementDto;
    }

}
