package com.autoprint.backend.services;

import com.autoprint.backend.api.dto.VendorCreateDto;
import com.autoprint.backend.api.dto.VendorUpdateDto;
import com.autoprint.backend.core.utils.DistanceUtil;
import com.autoprint.backend.entity.*;
import com.autoprint.backend.repo.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@Service
public class VendorService extends BaseEntityService<Vendor, Long> {

    @Autowired
    private VendorRepository vendorRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private ProductService productService;

    @Override
    protected PagingAndSortingRepository<Vendor, Long> getRepository() {
        return vendorRepository;
    }

    @Override
    protected void preCreate(Vendor vendor) {
        failIfBlank(vendor.getVendorname(), "vendorname");

        normalize(vendor);
    }

    @PreAuthorize("hasAuthority('VENDOR_CREATE')")
    @Transactional
    public Vendor create(VendorCreateDto vendorCreateDto) {
        User user = new User();
        user.setFullname(vendorCreateDto.fullname);
        user.setEmail(vendorCreateDto.email);
        user.setPassword(vendorCreateDto.password);
        user.setUsername(vendorCreateDto.username);
        user.setVendor(true);
        user = userService.create(user);

        Vendor vendor = new Vendor();
        vendor.setVendorname(vendorCreateDto.vendorname);
        vendor.setLongitude(vendorCreateDto.longitude);
        vendor.setLatitude(vendorCreateDto.latitude);
        vendor.setUser(user);
        return this.create(vendor);
    }

    @PreAuthorize("hasAuthority('VENDOR_CREATE')")
    @Transactional
    public Vendor create(Vendor vendor) {
        return super.create(vendor);
    }

    @Override
    protected void normalize(Vendor entity) {
        entity.setActive(true);
    }

    public void postCreate(Vendor vendor) {
        super.postCreate(vendor);
    }

    @Override
    protected Vendor preUpdate(Vendor pEntity, Vendor entity) {
        if (isChanged(pEntity.getVendorname(), entity.getVendorname())) {
            pEntity.setVendorname(entity.getVendorname());
        }
        if (isChanged(pEntity.getLatitude(), entity.getLatitude())) {
            pEntity.setLatitude(entity.getLatitude());
        }
        if (isChanged(pEntity.getLongitude(), entity.getLongitude())) {
            pEntity.setLongitude(entity.getLongitude());
        }
        // TODO: update location

        return pEntity;
    }

    @PreAuthorize("hasAuthority('VENDOR_UPDATE')")
    @Transactional
    public Vendor update(VendorUpdateDto vendorUpdateDto, Long id) {

        Vendor vendor = null;
        if (getAuthenticatedUser().isSuper()) {
            vendor = getById(id);
        } else {
            vendor = getByUserId((Long) getAuthenticatedUser().getId());
        }

        User user = new User();
        user.setPhoneNumber(vendorUpdateDto.phoneNumber);
        user.setImageBase64(vendorUpdateDto.imageBase64);
        user.setFullname(vendorUpdateDto.fullname);
        user.setEmail(vendorUpdateDto.email);
        user.setUsername(vendorUpdateDto.username);

        userService.update(user, vendor.getUser().getId());

        Vendor pVendor = new Vendor();
        pVendor.setId(id);
        pVendor.setVendorname(vendorUpdateDto.vendorname);
        pVendor.setLatitude(vendorUpdateDto.latitude);
        pVendor.setLongitude(vendorUpdateDto.longitude);

        return super.update(pVendor);
    }

    @PreAuthorize("hasAuthority('VENDOR_UPDATE')")
    @Transactional
    public Vendor update(Vendor vendor, Long id) {
        vendor.setId(id);
        return super.update(vendor);
    }

    @Override
    protected void postUpdate(Vendor vendor) {
        super.postUpdate(vendor);
    }

    @PreAuthorize("hasAuthority('VENDOR_READ')")
    @Override
    public Vendor getById(Long id) {
        return super.getById(id);
    }

    @PreAuthorize("hasAuthority('VENDOR_READ')")
    public Vendor getByUserId(Long userId) {
        return vendorRepository.findByUserId(userId);
    }

    public Vendor getNearestVendor(Order order) {

        Set<Product> setOfProduct = null;

        try {
            setOfProduct = productService.getListOfProductByOrderId(order.getId());
        } catch (IllegalArgumentException e) {

        }

        List<Vendor> listOfVendor;
        if (setOfProduct != null && !setOfProduct.isEmpty()) {
            List<Product> listOfProduct = new ArrayList<>(setOfProduct);
            listOfVendor = findVendorsWithPrinterAndSupportProduct(listOfProduct);
        } else {
            listOfVendor = findVendorsWithPrinter();
        }
        // TODO: if no vendor fits the criteria, notify!

        // blacklist all vendor that reject the order in range 24 hours
        List<Order> orderList = orderService.getByUserIdWithStatusInRangeOneDay(order.getUser(), Order.Status.REJECTED);
        List<Vendor> blackListVendor = orderList.stream().map(Order::getVendor).collect(Collectors.toList());
        List<Vendor> filteredListOfVendor = listOfVendor.stream().filter(v -> !blackListVendor.contains(v)).collect(Collectors.toList());

        if (filteredListOfVendor.size() != 0) {
            // TODO: compare result with top X vendor
            return getBestVendor(filteredListOfVendor, order.getLatitude(), order.getLongitude());
        } else {
            return getBestVendor(listOfVendor, order.getLatitude(), order.getLongitude());
        }
    }

    public List<Vendor> getListNearestVendor(Double latitude, Double longitude, Long orderId) {
        List<Vendor> listOfVendor;
        if (orderId != null) {
            try {
                Set<Product> setOfProduct = productService.getListOfProductByOrderId(orderId);
                List<Product> listOfProduct = new ArrayList<>(setOfProduct);
                listOfVendor = findVendorsWithPrinterAndSupportProduct(listOfProduct);
            } catch (IllegalArgumentException e) {
                listOfVendor = findVendorsWithPrinter();
            }
        } else {
            listOfVendor = findVendorsWithPrinter();
        }
        listOfVendor = sortNearestVendor(listOfVendor, latitude, longitude);
        if (listOfVendor.size() > 5) {
            return new ArrayList<Vendor>(listOfVendor.subList(listOfVendor.size() - 5, listOfVendor.size()));
        } else {
            return listOfVendor;
        }
    }

    public Vendor getBestVendor(List<Vendor> listOfVendor, Double latitude, Double longitude) {
        List<Vendor> nearestVendor = sortNearestVendor(listOfVendor, latitude, longitude);
        return nearestVendor.get(0);
    }

    public List<Vendor> sortNearestVendor(List<Vendor> listOfVendor, Double latitude, Double longitude) {
        for (Vendor vendor : listOfVendor) {
            double distance = DistanceUtil.distance(latitude, longitude, vendor.getLatitude(), vendor.getLongitude(), 'K');
            vendor.setDistance((long) (distance * 1000.0d));
        }
        Collections.sort(listOfVendor, new Comparator<Vendor>() {
            @Override
            public int compare(Vendor v1, Vendor v2) {
                return Double.compare(v1.getDistance(), v2.getDistance());
            }
        });
        return listOfVendor;
    }

    public List<Vendor> findVendorsWithPrinterAndSupportProduct(List<Product> productList) {
        return vendorRepository.findByHaveAtLeastOnePrinterAndSupportProduct(productList);
    }

    @Deprecated
    public List<Vendor> findVendorsWithPrinter() {
        // find if got printer only
        return vendorRepository.findByHaveAtLeastOnePrinter();
    }

    @Transactional(readOnly = true)
    public Page<Vendor> get(Specification specification, Pageable pageable) {
        return vendorRepository.findAll(specification, pageable);
    }
}
