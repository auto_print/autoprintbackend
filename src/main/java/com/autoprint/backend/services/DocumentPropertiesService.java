package com.autoprint.backend.services;

import com.autoprint.backend.api.dto.DocumentCreateDto;
import com.autoprint.backend.entity.Document;
import com.autoprint.backend.entity.DocumentProperties;
import com.autoprint.backend.repo.DocumentPropertiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class DocumentPropertiesService extends BaseEntityService<DocumentProperties, Long> {

    @Autowired
    private DocumentPropertiesRepository documentPropertiesRepository;

    @Override
    protected PagingAndSortingRepository<DocumentProperties, Long> getRepository() {
        return documentPropertiesRepository;
    }

    @Override
    protected void preCreate(DocumentProperties entity) {
        normalize(entity);
    }

    @Override
    public DocumentProperties create(DocumentProperties documentProperties) {
        return super.create(documentProperties);
    }

    @Override
    protected void normalize(DocumentProperties entity) {

        if (entity.getCopies() == null) {
            entity.setCopies(1L);
        }
        if (entity.getDuplex() == null) {
            entity.setDuplex(DocumentProperties.no_duplex);
        }
        if (entity.getPageOrientation() == null) {
            entity.setPageOrientation(DocumentProperties.orientation_auto);
        }
        if (entity.getColour() == null) {
            entity.setColour(DocumentProperties.colour_auto);
        }
        if (entity.getCoverColour() == null) {
            entity.setCoverColour(DocumentProperties.colour_white);
        }
        if (entity.getCoverType() == null) {
            entity.setCoverType(DocumentProperties.softCover);
        }

    }

    @Override
    protected DocumentProperties preUpdate(DocumentProperties pEntity, DocumentProperties entity) {
        throw new UnsupportedOperationException("Update not supported!");
    }

    public Long getPageNumber(String pageNumberString) {
        Long total = 0L;
        List<String> listOfInput = Arrays.asList(pageNumberString.split(","));
        for (String in : listOfInput) {
            if (in.contains("-")) {
                List<String> listOfPage = Arrays.asList(pageNumberString.split("-"));
                // FIXME: do checking here
                int start = Integer.parseInt(listOfPage.get(0));
                int end = Integer.parseInt(listOfPage.get(1));
                int partial = end - start + 1;
                total += partial;
            } else {
                total++;
            }
        }
        return total;
    }

    public void checkDocumentPrropertiesValidity(DocumentCreateDto dto) {
        String args = dto.pages;
        failIfBlank(args, "Page Cannot Blank");
        if ("all".equalsIgnoreCase(dto.pages)) {
            throw new IllegalArgumentException("Page must be specify!");
        }

        if (args.matches("([0-9]+[-,]*)+")) {
            // correct format = do nothing
        } else {
            // bad format
            throw new IllegalArgumentException("Bad format for page!");
        }
    }
}
