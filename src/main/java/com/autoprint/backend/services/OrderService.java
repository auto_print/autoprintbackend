package com.autoprint.backend.services;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.api.dto.OrderDataDto;
import com.autoprint.backend.api.mapper.DocumentPropertiesMapper;
import com.autoprint.backend.api.mapper.OrderMapper;
import com.autoprint.backend.cron.async.PrintJobAsync;
import com.autoprint.backend.entity.*;
import com.autoprint.backend.object.OrderOverallObject;
import com.autoprint.backend.repo.OrderRepository;
import com.autoprint.backend.utils.SecureUtils;
import com.autoprint.backend.utils.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderService extends BaseEntityService<Order, Long> {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private PrintJobAsync printJobAsync;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private VendorService vendorService;
    @Autowired
    private ProductVendorService productVendorService;
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private DocumentPropertiesService documentPropertiesService;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private DocumentPropertiesMapper documentPropertiesMapper;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private AppConfig appConfig;

    @Override
    protected PagingAndSortingRepository<Order, Long> getRepository() {
        return orderRepository;
    }

    @Override
    protected void preCreate(Order entity) {
        if (entity.getUser().getId() == null) {
            if (getAuthenticatedUser().isUser()) {
                User user = userService.getById((Long) getAuthenticatedUser().getId());
                entity.setUser(user);
            } else {
                throw new IllegalStateException("User must be define!!");
            }
        }

        String code = SecureUtils.generateTokenAlphaAlphabet(6);
        entity.setReferenceId(code);
        normalize(entity);
    }

    public Order create(Order order) {
        return super.create(order);
    }

    @Override
    protected void normalize(Order entity) {
        entity.setStatus(Order.Status.PENDING);
    }

    @Override
    protected Order preUpdate(Order pEntity, Order entity) {

        if (isChanged(pEntity.getStatus(), entity.getStatus())) {
            pEntity.setStatus(entity.getStatus());
        }

        if (isChanged(pEntity.getVendor(), entity.getVendor())) {
            pEntity.setVendor(entity.getVendor());
        }
        return pEntity;
    }

    public Order update(Order order, Long id) {
        order.setId(id);
        return super.update(order);
    }

    @Transactional(readOnly = true)
    public Order getById(Long id) {
        return super.getById(id);
    }

    public Page<Order> getByUserId(Long id, Order.Status status, Pageable pageable) {
        failIfVendor();
        User user = userService.getById(id);
        if (status != null) {
            return orderRepository.findByUserAndStatus(user, status, pageable);
        } else {
            return orderRepository.findByUser(user, pageable);
        }
    }

    public List<Order> getByUserIdWithStatusInRangeOneDay(User user, Order.Status status) {
        failIfVendor();
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime oneDayBefore = now.minus(1, ChronoUnit.DAYS);
        return orderRepository.findByUserAndStatusAndDateCreatedBetween(user, status, oneDayBefore, now);
    }

    public Page<Order> getByVendorId(Long id, Order.Status status, Pageable pageable) {
        failIfUser();
        Vendor vendor = vendorService.getById(id);
        if (status != null) {
            return orderRepository.findByVendorAndStatus(vendor, status, pageable);
        } else {
            return orderRepository.findByVendor(vendor, pageable);
        }
    }

    public Order preConfirmOrder(Long id, Long vendorId) {
        failIfVendor();
        Order order = getById(id);
        if (vendorId != null) {
            order.setVendor(vendorService.getById(vendorId));
        } else if (order.getVendor() == null) {
            order.setVendor(vendorService.getNearestVendor(order));
        }
        return this.update(order);
    }

    @Transactional
    public Order confirmOrder(Long id, boolean confirm) {
        failIfVendor();
        Order pOrder = getById(id);
        if (confirm) {
            // create job
            BigDecimal total = calculateMockTrueTotal(id, pOrder.getVendor());
            List<Document> documentList = documentService.getByOrderId(id);
            for (Document document : documentList) {
                printJobAsync.createPrintJob(document);
            }

            // generate invoice
            Invoice invoice = new Invoice();
            invoice.setOrder(pOrder);
            invoice.setAmount(total);
            invoice = invoiceService.create(invoice);

            Order order = new Order();
            order.setStatus(Order.Status.CONFIRM);
            // FIXME: hack
            Set<Invoice> invoiceSet = new HashSet<>();
            invoiceSet.add(invoice);
            pOrder.setInvoices(invoiceSet);
            pOrder = this.update(order, id);

        } else {
            Order order = new Order();
            order.setStatus(Order.Status.CANCELLED);
            pOrder = this.update(order, id);
        }
        return pOrder;
    }

    @Transactional
    public void pay(Long id, BigDecimal amount) {
        Order order = getById(id);

        //KISS
        if (calculateMockTrueTotal(order.getId(), order.getVendor()).compareTo(amount) == 0) {
            // fail if there are invoices paid
            boolean anyPaid = order.getInvoices().stream().anyMatch(invoice -> Invoice.Status.PAID.equals(invoice.getStatus()));
            if (!anyPaid) {
                // proceed
                for (Invoice pInvoice : order.getInvoices()) {
                    invoiceService.pay(pInvoice);
                }
            } else {
                System.out.println("Invoice paid already!");
            }
        } else {
            throw new IllegalArgumentException("Amount not same!");
        }
    }

    @Transactional(readOnly = true)
    public Page<Order> get(Specification specification, Pageable pageable) {
        return orderRepository.findAll(specification, pageable);
    }

    @Transactional
    public void cancel(Long id) {
        Order pOrder = getById(id);

        if (Order.Status.PENDING.equals(pOrder.getStatus()) || Order.Status.CONFIRM.equals(pOrder.getStatus())) {
            revert(pOrder);
            // cancel
            Order order = new Order();
            order.setStatus(Order.Status.CANCELLED);
            pOrder = this.update(order, id);
        }

    }

    @Transactional
    public Order complete(Long id) {
        Order order = new Order();
        order.setStatus(Order.Status.COMPLETED);
        order = this.update(order, id);

        // send notification
        activityService.createActivityTemplate(
                order.getUser().getId(),
                Activity.Type.NOTIFICATION,
                String.format("Complete order %s!", order.getReferenceId()),
                "Your order have been successfully completed! Kindly come and take your documents.");
        return order;
    }

    public void revert(Order order) {
        // TODO: we might want a logic where after vendor accept, cannot cancel
        // cancel job
        List<Document> documentList = documentService.getByOrderId(order.getId());
        for (Document document : documentList) {
            printJobAsync.cancelPrintJob(document);
        }

        // cancel invoices
        for (Invoice invoice : order.getInvoices()) {
            invoiceService.cancelInvoice(invoice.getId());
        }
    }

    public void reject(Long id, String reason) {
        Order pOrder = getById(id);
        if (Order.Status.PENDING.equals(pOrder.getStatus()) || Order.Status.CONFIRM.equals(pOrder.getStatus())) {
            revert(pOrder);

            // cancel
            Order order = new Order();
            order.setStatus(Order.Status.REJECTED);
            order.setReason(reason);
            pOrder = this.update(order, id);


            activityService.createActivityTemplate(
                    pOrder.getUser().getId(),
                    Activity.Type.NOTIFICATION,
                    String.format("Order %s are rejected T-T!", pOrder.getReferenceId()),
                    String.format("We sorry to inform you that your order have been rejected by your vendor due to %s. Perhaps you want to reorder?", reason));
        }
    }

    public Order reorder(Long id, Long vendorId) {
        Order pOrder = getById(id);
        if (Order.Status.CANCELLED == pOrder.getStatus() || Order.Status.REJECTED == pOrder.getStatus()) {
            Order order = clone(id);
            order = preConfirmOrder(order.getId(), vendorId);
            order = confirmOrder(order.getId(), true);
            pay(order.getId(), calculateMockTrueTotal(order.getId(), order.getVendor()));

            return order;
        } else {
            // only allow to reorder if cancelled or rejected
            return pOrder;
        }
    }

    @Transactional
    public Order clone(Long id) {
        // create new order
        Order previousOrder = this.getById(id);
        Order newOrder = new Order();
        newOrder.setUser(previousOrder.getUser());
        // temporary
        newOrder.setLongitude(previousOrder.getLongitude());
        newOrder.setLatitude(previousOrder.getLatitude());
        newOrder = this.create(newOrder);

        // same file
        List<Document> allDocument = documentService.getByOrderId(id);
        for (Document oldDocument : allDocument) {
            Document newDocument = new Document();

            // TODO: verify working
            newDocument.setUser(oldDocument.getUser());
            newDocument.setOrder(newOrder);
            newDocument.setFilename(oldDocument.getFilename());
            newDocument.setFiletype(oldDocument.getFiletype());
            newDocument.setProduct(oldDocument.getProduct());
            newDocument.setReorderDocument(oldDocument);
            newDocument = documentService.create(newDocument);

            DocumentProperties oldDocumentProperties = oldDocument.getDocumentProperties();
            DocumentProperties newDocumentProperties = documentPropertiesMapper.mapDocumentProperties(oldDocumentProperties, newDocument);
            newDocumentProperties = documentPropertiesService.create(newDocumentProperties);

            newDocument.setDocumentProperties(newDocumentProperties);
            // hack
            documentService.update(newDocument);
        }
        return newOrder;
    }

    // TODO: move to invoice service
    @Transactional
    public BigDecimal calculateMockTrueTotal(Long id, Vendor vendor) {
        List<Document> documentList = documentService.getByOrderId(id);

        if (documentList.isEmpty()) {
            throw new IllegalStateException("No document can be found! OrderId:" + id);
        }

        BigDecimal total = BigDecimal.ZERO;
        for (Document document : documentList) {
            ProductVendor pV = productVendorService.getByProductIdAndVendorId(document.getProduct().getId(), vendor.getId());
            BigDecimal partialPrice = pV.getPrice().multiply(document.getDocumentProperties().getPagesBD());
            partialPrice = partialPrice.multiply(document.getDocumentProperties().getCopiesBD());
            total = total.add(partialPrice);
        }

        if (total.compareTo(BigDecimal.valueOf(0.10)) < 0) {
            total = total.add(BigDecimal.valueOf(0.02));
        } else {
            float taxRate = Float.parseFloat(appConfig.getTaxRate());
            float serviceRate = Float.parseFloat(appConfig.getServiceRate());

            BigDecimal tax = total.multiply(BigDecimal.valueOf(taxRate));
            tax = tax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

            BigDecimal service = total.multiply(BigDecimal.valueOf(serviceRate));
            service = service.setScale(2, BigDecimal.ROUND_HALF_EVEN);

            total = total.add(tax).add(service);
        }
        return total.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }

    public List<Order> getAllVendorOrderInRange(Vendor vendor, LocalDateTime start, LocalDateTime end) {
        return orderRepository.findByVendorAndStatusAndDateCreatedBetween(vendor, Order.Status.COMPLETED, start, end);
    }

    public List<OrderOverallObject> getVendorOrderStatisticList(Vendor vendor) {
        return orderRepository.getVendorOrderStatistic(vendor, Order.Status.COMPLETED);
    }

    // TODO: configurable!
    public OrderDataDto getOrderData() {
        failIfUser();
        failIfVendor();
        // TODO: helper!

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime startOfMonth = TimeUtils.getStartOfMonth();
        LocalDateTime lastMonth = TimeUtils.getLastMonth();
        LocalDateTime lastYear = TimeUtils.getLastYear();

        Long totalOrderInMonth = orderRepository.countOrderByThisMonth(startOfMonth, Order.Status.COMPLETED);
        Long totalOrderLastMonth = orderRepository.countOrderByLastMonths(lastMonth, startOfMonth, Order.Status.COMPLETED);
        Long biggestCount = 0L;
        Long smallestCount = 0L;
        boolean first = true;

        List<String> monthList = TimeUtils.monthListInit();

        List<OrderOverallObject> list = orderRepository.getOverallStatistics(Order.Status.COMPLETED, lastYear, now);

        int start;

        if (!list.isEmpty()) {
            start = monthList.indexOf(list.get(0).getMonth());
        } else {
            // get today's month
            start = monthList.indexOf(TimeUtils.getMonthName(LocalDateTime.now()));
        }

        List<OrderOverallObject> finalizedList = new ArrayList<>();

        List<String> nonExistMonthList = monthList.stream().filter(m -> {
            for (OrderOverallObject o : list) {
                if (m.equals(o.getMonth())) {
                    return false;
                }
            }
            return true;
        }).collect(Collectors.toList());

        int it = start;
        do {
            String m = monthList.get(it);
            if (nonExistMonthList.contains(m)) {
                finalizedList.add(defaultCreation(m));
            } else {
                for (OrderOverallObject o : list) {
                    if (m.equalsIgnoreCase(o.getMonth())) {
                        finalizedList.add(o);
                        break;
                    }
                }
            }

            if (it == 11) {
                if (start == 0) {
                    break;
                } else {
                    it = 0;
                    continue;
                }
            }
            it++;
        } while (it != start);

        for (OrderOverallObject o : finalizedList) {

            if (o.getCount() > biggestCount) {
                biggestCount = o.getCount();
            }

            if (first) {
                smallestCount = o.getCount();
                first = false;
            } else if (o.getCount() < smallestCount) {
                smallestCount = o.getCount();
            }
        }
        return orderMapper.mapOrderDataDto(totalOrderInMonth, totalOrderLastMonth, biggestCount, smallestCount, finalizedList);
    }

    private OrderOverallObject defaultCreation(String month) {
        return new OrderOverallObject(month);
    }
}
