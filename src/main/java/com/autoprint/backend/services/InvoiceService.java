package com.autoprint.backend.services;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.api.mapper.InvoiceItemMapper;
import com.autoprint.backend.entity.*;
import com.autoprint.backend.repo.InvoiceRepository;
import com.autoprint.backend.utils.SecureUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class InvoiceService extends BaseEntityService<Invoice, Long> {

    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private WalletService walletService;
    @Autowired
    private InvoiceItemService invoiceItemService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private ProductVendorService productVendorService;
    @Autowired
    private InvoiceItemMapper invoiceItemMapper;
    @Autowired
    private ProductService productService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private AppConfig appConfig;

    @Override
    protected PagingAndSortingRepository<Invoice, Long> getRepository() {
        return invoiceRepository;
    }

    @Override
    public Invoice create(Invoice invoice) {
        return super.create(invoice);
    }

    @Override
    protected void preCreate(Invoice entity) {
        failIfNull(entity.getAmount(), "amount");
        failIfNull(entity.getOrder(), "order");

        String code = SecureUtils.generateTokenAlphaAlphabet(4);
        String ref = String.format("%s-%s", entity.getOrder().getReferenceId(), code);
        entity.setReferenceId(ref);

        normalize(entity);
    }

    @Override
    protected void normalize(Invoice entity) {
        if (entity.getStatus() == null) {
            entity.setStatus(Invoice.Status.UNPAID);
        }
    }

    @Override
    protected void postCreate(Invoice entity) {

        // FIXME: hack!
        List<InvoiceItem> invoiceItemList = new ArrayList<>();

        List<Document> documentList = documentService.getByOrderId(entity.getOrder().getId());

        failIfEmpty(documentList, "document");

        BigDecimal total = BigDecimal.ZERO;
        for (Document document : documentList) {
            ProductVendor pV = productVendorService.getByProductIdAndVendorId(document.getProduct().getId(), entity.getOrder().getVendor().getId());
            BigDecimal partialPrice = pV.getPrice().multiply(document.getDocumentProperties().getPagesBD());
            partialPrice = partialPrice.multiply(document.getDocumentProperties().getCopiesBD());
            // one item

            InvoiceItem invoiceItem = invoiceItemMapper.map(document, entity, document.getProduct(), partialPrice);
            invoiceItemList.add(invoiceItemService.create(invoiceItem));

            total = total.add(partialPrice);
        }

        invoiceItemList.add(handleTax(total, entity));
        invoiceItemList.add(handleService(total, entity));

        entity.setInvoiceItems(invoiceItemList);
    }

    private InvoiceItem handleTax(BigDecimal total, Invoice entity) {
        InvoiceItem taxItem;
        if (total.compareTo(BigDecimal.valueOf(0.10)) < 0) {
            BigDecimal tax = BigDecimal.valueOf(0.01);
            taxItem = invoiceItemMapper.map(null, entity, productService.getTaxProduct(), tax);
        } else {
            float taxRate = Float.parseFloat(appConfig.getTaxRate());
            BigDecimal tax = total.multiply(BigDecimal.valueOf(taxRate));
            tax = tax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
            taxItem = invoiceItemMapper.map(null, entity, productService.getTaxProduct(), tax);
        }
        return invoiceItemService.create(taxItem);
    }

    private InvoiceItem handleService(BigDecimal total, Invoice entity) {
        InvoiceItem serviceItem;
        if (total.compareTo(BigDecimal.valueOf(0.10)) < 0) {
            BigDecimal service = BigDecimal.valueOf(0.01);
            serviceItem = invoiceItemMapper.map(null, entity, productService.getServiceProduct(), service);
        } else {
            float serviceRate = Float.parseFloat(appConfig.getServiceRate());
            BigDecimal service = total.multiply(BigDecimal.valueOf(serviceRate));
            service = service.setScale(2, BigDecimal.ROUND_HALF_EVEN);
            serviceItem = invoiceItemMapper.map(null, entity, productService.getServiceProduct(), service);
        }
        return invoiceItemService.create(serviceItem);
    }

    @Override
    protected Invoice preUpdate(Invoice pEntity, Invoice entity) {

        if (isChanged(pEntity.getStatus(), entity.getStatus())) {
            pEntity.setStatus(entity.getStatus());
        }

        return pEntity;
    }

    public Invoice update(Invoice invoice, Long id) {
        invoice.setId(id);
        return super.update(invoice);
    }

    @Override
    public Invoice getById(Long id) {
        return super.getById(id);
    }

    public void pay(Invoice pInvoice) {

        Invoice invoice = new Invoice();
        invoice.setStatus(Invoice.Status.PAID);
        pInvoice = this.update(invoice, pInvoice.getId());

        for (InvoiceItem invoiceItem : pInvoice.getInvoiceItems()) {
            User to;
            if (invoiceItem.getProduct().getId().equals(1L) || invoiceItem.getProduct().getId().equals(2L)) {
                to = userService.getById(1L);
            } else {
                to = pInvoice.getOrder().getVendor().getUser();
            }
            walletService.transaction(
                    to.getId(),
                    pInvoice.getOrder().getUser().getId(),
                    invoiceItem.getAmount());
        }

        // generate payment
        Payment payment = new Payment();
        payment.setAmount(pInvoice.getAmount());
        payment.setInvoice(pInvoice);
        paymentService.create(payment);
    }

    /*public Page<Invoice> getByUserId(Long id, Pageable pageable) {
        failIfVendor();
        User user = userService.getById(id);
        failAccessUnless(isSame(user), "find");
        return invoiceRepository.findByUser(user, pageable);
    }*/

    @Transactional
    public void cancelInvoice(Long id) {
        Invoice pInvoice = getById(id);
        if (Invoice.Status.PAID.equals(pInvoice.getStatus())) {
            Invoice invoice = new Invoice();
            revert(id);
            invoice.setStatus(Invoice.Status.CANCELLED);
            this.update(invoice, id);
        }
    }

    @Transactional
    public Invoice refund(Long id) {
        Invoice pInvoice = getById(id);
        if (Invoice.Status.PAID.equals(pInvoice.getStatus())) {
            Invoice invoice = new Invoice();
            revert(id);
            invoice.setStatus(Invoice.Status.REFUND);
            invoice = this.update(invoice, id);

            activityService.createActivityTemplate(
                    pInvoice.getOrder().getUser().getId(),
                    Activity.Type.NOTIFICATION,
                    "Successfully refund.",
                    "We have successfuly refund credit to your wallet account. Thank you for using our service :)");
            return invoice;
        }
        return pInvoice;
    }

    @Transactional
    private void revert(Long id) {
        Invoice pInvoice = getById(id);

        if (Invoice.Status.PAID.equals(pInvoice.getStatus())) {
            // return back the money :P
            for (InvoiceItem invoiceItem : pInvoice.getInvoiceItems()) {
                User from;
                if (invoiceItem.getProduct().getId().equals(1L) || invoiceItem.getProduct().getId().equals(2L)) {
                    from = userService.getById(1L);
                } else {
                    from = pInvoice.getOrder().getVendor().getUser();
                }
                walletService.transaction(
                        pInvoice.getOrder().getUser().getId(),
                        from.getId(),
                        invoiceItem.getAmount());
            }

            // revert payment if paid
            for (Payment payment : pInvoice.getPayments()) {
                paymentService.revertPayment(payment.getId());
            }
        }
    }

}
