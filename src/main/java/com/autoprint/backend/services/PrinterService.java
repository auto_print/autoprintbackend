package com.autoprint.backend.services;

import com.autoprint.backend.AppConfig;
import com.autoprint.backend.entity.Printer;
import com.autoprint.backend.entity.Vendor;
import com.autoprint.backend.exception.EzeepFailedException;
import com.autoprint.backend.ezeep.NotificationUtils;
import com.autoprint.backend.repo.PrinterRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrinterService extends BaseEntityService<Printer, Long> {

    @Autowired
    private PrinterRepository printerRepository;
    @Autowired
    private VendorService vendorService;
    @Autowired
    private AppConfig appConfig;

    @Override
    protected PagingAndSortingRepository<Printer, Long> getRepository() {
        return printerRepository;
    }

    @Override
    protected void preCreate(Printer printer) {
        failIfUser();

        if (getAuthenticatedUser().isSuper()) {
            Vendor vendor = vendorService.getById(printer.getVendor().getId());
            printer.setVendor(vendor);
        } else {
            printer.setVendor(vendorService.getByUserId((Long) getAuthenticatedUser().getId()));
        }

        String ezeepPrinterId = "";
        if (appConfig.getAppModeProd().equals(appConfig.getAppMode())) {

            // TODO: do ezeep thingy

            if (StringUtils.isBlank(ezeepPrinterId)) {
                // TODO: verify rollback
                throw new EzeepFailedException("Failed to save document to Ezeep Cloud");
            }
        } else {
            NotificationUtils.EzeepDisableNotification(this.getClass());
        }

        printer.setEzeepPrinterId(ezeepPrinterId);
        normalize(printer);
    }

    public Printer create(Printer Printer) {
        return super.create(Printer);
    }

    protected void postCreate(Printer printer) {
        super.postCreate(printer);
    }

    @Override
    protected void normalize(Printer entity) {
        entity.setStatus(Printer.Status.ACTIVE);
    }

    @Override
    protected Printer preUpdate(Printer pEntity, Printer entity) {

        if (isChanged(pEntity.getName(), entity.getName())) {
            pEntity.setName(entity.getName());
        }

        if (isChanged(pEntity.getStatus(), entity.getStatus())) {
            pEntity.setStatus(entity.getStatus());
        }

        return pEntity;
    }

    public Printer update(Printer printer, Long id) {
        printer.setId(id);
        return super.update(printer);
    }

    protected void postUpdate(Printer printer) {
        super.postUpdate(printer);
    }

    public Printer getById(Long id) {
        return super.getById(id);
    }

    public Page<Printer> findAllByVendorId(Long id, Pageable pageable) {
        Vendor vendor = vendorService.getById(id);
        return printerRepository.findByVendor(vendor, pageable);
    }

    public Printer getAvailablePrinterOfVendor(Vendor vendor) {
        List<Printer> listOfPrinter = printerRepository.findByVendorAndStatus(vendor, Printer.Status.ACTIVE);
        // TODO: specification of which printer?
        return listOfPrinter.get(0);
    }
}
