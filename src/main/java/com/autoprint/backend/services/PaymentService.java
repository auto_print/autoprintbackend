package com.autoprint.backend.services;

import com.autoprint.backend.entity.Payment;
import com.autoprint.backend.repo.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

@Service
public class PaymentService extends BaseEntityService<Payment, Long> {

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    protected PagingAndSortingRepository<Payment, Long> getRepository() {
        return paymentRepository;
    }

    @Override
    protected void preCreate(Payment entity) {
        failIfNull(entity.getAmount(), "amount");
        failIfNull(entity.getInvoice(), "invoice");
        normalize(entity);
    }

    @Override
    public Payment create(Payment payment) {
        return super.create(payment);
    }

    @Override
    protected void normalize(Payment entity) {
        entity.setStatus(Payment.Status.ACTIVE);
    }

    public Payment update(Payment payment, Long id) {
        payment.setId(id);
        return super.update(payment);
    }

    @Override
    protected Payment preUpdate(Payment pEntity, Payment entity) {

        if (isChanged(pEntity.getStatus(), entity.getStatus())) {
            pEntity.setStatus(entity.getStatus());
        }

        return pEntity;
    }

    @Override
    public Payment getById(Long id) {
        return super.getById(id);
    }

    public void revertPayment(Long id) {
        Payment payment = new Payment();
        payment.setStatus(Payment.Status.REVERTED);
        this.update(payment, id);
    }
}
