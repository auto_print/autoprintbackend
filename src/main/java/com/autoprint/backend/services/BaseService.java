package com.autoprint.backend.services;

import com.autoprint.backend.api.security.UserPrincipal;
import com.autoprint.backend.api.security.IAuthenticationFacade;
import com.autoprint.backend.core.security.UserAware;
import com.autoprint.backend.exception.IllegalUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

public class BaseService {

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    public UserAware getAuthenticatedUser() {
        Authentication authentication = authenticationFacade.getAuthentication();
        if (authentication != null) {
            Object principalObj = authentication.getPrincipal();

            if (principalObj == null) {
                throw new IllegalStateException("Principal is unexpectedly undefined!");
            }

            if (principalObj instanceof UserPrincipal) {
                return ((UserPrincipal) principalObj).getUser();
            } else {
                // log
            }
        }
        throw new IllegalUserException("User are not recognized!");
    }

    public void failIfIdInvalid(Long id, Class<?> entityClass) {
        checkNotNull(id);
        if (id <= 0) {
            throw new IllegalArgumentException(format("%s invalid ID", entityClass.getSimpleName()));
        }
    }

    public void failIfSuper() {
        if (getAuthenticatedUser().isSuper()) {
            throw new UnsupportedOperationException("Unsupported for superuser");
        }
    }

    public void failIfVendor() {
        if (getAuthenticatedUser().isVendor()) {
            throw new UnsupportedOperationException("Unsupported for vendor");
        }
    }

    public void failIfUser() {
        if (getAuthenticatedUser().isUser()) {
            throw new UnsupportedOperationException("Unsupported for user");
        }
    }

    /**
     * Returns true is the given user is identical to authenticated user.
     * <p>
     * TODO: DRY for superuser
     */
    protected boolean isSame(UserAware user1) {
        UserAware authUser = getAuthenticatedUser();
        if (user1 == null || authUser == null) return false;
        return user1.getId().equals(authUser.getId());
    }

    /**
     * Returns true if both users are identical.
     */
    protected boolean isSame(UserAware user1, UserAware user2) {
        if (user1 == null || user2 == null) return false;
        return user1.getId().equals(user2.getId());
    }
}
