package com.autoprint.backend.services;

import com.autoprint.backend.core.entity.HasDateCreated;
import com.autoprint.backend.core.entity.HasDateUpdated;
import com.autoprint.backend.core.entity.IEntity;
import com.autoprint.backend.exception.PermissionDeniedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;

public abstract class BaseEntityService<E extends IEntity<ID>, ID extends Serializable> extends BaseService {

    // todo: ugly
    private final Class<E> TYPE;

    public BaseEntityService() {
        Type type = getClass().getGenericSuperclass();

        while (!(type instanceof ParameterizedType)) {
            if (type instanceof ParameterizedType) {
                type = ((Class<?>) ((ParameterizedType) type).getRawType()).getGenericSuperclass();
            } else {
                type = ((Class<?>) type).getGenericSuperclass();
            }
        }

        this.TYPE = (Class<E>) ((ParameterizedType) type).getActualTypeArguments()[0];
    }

    protected abstract PagingAndSortingRepository<E, ID> getRepository();

    protected abstract void preCreate(E entity);

    protected abstract void normalize(E entity);

    @Transactional
    public E create(E entity) {
        preCreate(entity);

        if (entity instanceof HasDateCreated) {
            ((HasDateCreated) entity).setDateCreated(LocalDateTime.now());
        }
        if (entity instanceof HasDateUpdated) {
            ((HasDateUpdated) entity).setLastUpdated(LocalDateTime.now());
        }

        E pEntity = getRepository().save(entity);
        postCreate(pEntity);
        return pEntity;
    }

    protected void postCreate(E entity) {
    }

    protected abstract E preUpdate(E pEntity, E entity);

    @Transactional
    public E update(E changeSet) {
        assert changeSet != null;
        failIfIdInvalid(changeSet.getId());

        E before = this.getById(changeSet.getId());

        E pEntity = this.getById(changeSet.getId());

        // Validation and update of the changed attributes
        pEntity = preUpdate(pEntity, changeSet);

        if (pEntity instanceof HasDateUpdated) {
            ((HasDateUpdated) pEntity).setLastUpdated(LocalDateTime.now());
        }

        pEntity = getRepository().save(pEntity);

        postUpdate(pEntity);
        return pEntity;
    }

    protected void postUpdate(E pEntity) {
    }

    @Transactional(readOnly = true)
    public E getById(ID id) {
        failIfIdInvalid(id);
        return failIfNotFound(id);
    }

    // utils

    protected void failIfBlank(String value, String args) {
        if (isBlank(value)) {
            throw new IllegalArgumentException(format("%s %s cannot be null/blank", TYPE.getSimpleName(), args));
        }
    }

    protected void failIfBlank(String value, String args, Class<?> clazz) {
        if (isBlank(value)) {
            throw new IllegalArgumentException(format("%s %s cannot be null/blank", clazz.getSimpleName(), args));
        }
    }

    protected E failIfNotFound(ID id) {
        return this.getRepository().findById(id)
                .orElseThrow(() -> new EntityNotFoundException(format("%s (ID %s) not found", TYPE.getSimpleName(), id)));
    }

    protected void failIfIdInvalid(ID id) {
        if (id instanceof Number) {
            if (((Number) id).longValue() <= 0) {
                throw new IllegalArgumentException(format("%s invalid ID", TYPE.getSimpleName()));
            }
        } else if (id instanceof String) {
            if (isBlank((String) id)) {
                throw new IllegalArgumentException(format("%s invalid ID", TYPE.getSimpleName()));
            }
        } else {
            throw new IllegalArgumentException(format("%s invalid ID", TYPE.getSimpleName()));
        }
    }

    protected void failConstraintUnless(boolean expression, String reason) {
        if (!expression) {
            throw new IllegalArgumentException(reason);
        }
    }

    protected void failInvalidUnless(boolean expression, String name) {
        if (!expression)
            throw new IllegalArgumentException(format("%s %s is invalid", TYPE.getSimpleName(), name));
    }

    protected void failAccessUnless(boolean expression, String action) {
        if (!expression) {
            throw new PermissionDeniedException(format("Permission denied when %s %s", action, TYPE.getSimpleName()));
        }
    }

    protected void failIfNull(Object value, String args) {
        if (value == null) {
            throw new IllegalArgumentException(format("%s %s cannot be null", TYPE.getSimpleName(), args));
        }
    }

    protected void failIfAlphabet(String value, String args) {
        failIfAlphabet(value, args, false);
    }

    protected void failIfAlphabet(String value, String args, Boolean allowBlankField) {

        if (allowBlankField) {
            if (StringUtils.isNotBlank(value)) {
                if (!StringUtils.isNumeric(value)) {
                    throw new IllegalArgumentException(format("%s %s cannot be alphabet", TYPE.getSimpleName(), args));
                }
            }
        } else {
            if (!StringUtils.isNumeric(value)) {
                throw new IllegalArgumentException(format("%s %s cannot be alphabet", TYPE.getSimpleName(), args));
            }
        }
    }

    protected void failIfEmpty(Object value, String args) {
        if (value instanceof List) {
            List l = (List) value;
            if (l.isEmpty()) {
                throw new IllegalArgumentException(String.format("%s %s cannot be empty", TYPE.getSimpleName(), args));
            }
        } else if (value instanceof Set) {
            Set s = (Set) value;
            if (s.isEmpty()) {
                throw new IllegalArgumentException(String.format("%s %s cannot be empty", TYPE.getSimpleName(), args));
            }
        } else {
            throw new IllegalArgumentException("Value is not a list or set!");
        }
    }

    protected boolean isChanged(Object persistentObj, Object incoming) {
        return isChanged(persistentObj, incoming, false);
    }

    protected boolean isChanged(Object persistentObj, Object incoming, Boolean allowBlankField) {

        if (!allowBlankField) {
            if (incoming instanceof String && StringUtils.isBlank((String) incoming)) return false;
        }

        if (persistentObj == null && incoming == null) return false;
        if (persistentObj == null && incoming != null) return true;
        // If incoming change is NULL, assume no change!
        if (persistentObj != null && incoming == null) return false;

        if (persistentObj.getClass().equals(incoming.getClass())) {
            if (persistentObj instanceof IEntity) {
                return ((IEntity) persistentObj).getId() != ((IEntity) incoming).getId();
            } else {
                return !persistentObj.equals(incoming);
            }
        }
        // FIXME: Hackish way to deal with util.date vs. mysql.timestamp
        else if (incoming instanceof Date) {
            return !(((Timestamp) persistentObj).getTime() == ((Date) incoming).getTime());
        } else {
            // TODO: maybe returning false?
            throw new UnsupportedOperationException();
        }
    }
}
