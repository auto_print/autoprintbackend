package com.autoprint.backend.services;

import com.autoprint.backend.api.dto.WalletOperationDto;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.entity.Wallet;
import com.autoprint.backend.exception.AmountNotEnoughException;
import com.autoprint.backend.repo.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class WalletService extends BaseEntityService<Wallet, Long> {

    @Autowired
    private WalletRepository walletRepository;
    @Autowired
    private UserService userService;

    @Override
    protected PagingAndSortingRepository<Wallet, Long> getRepository() {
        return walletRepository;
    }

    @Override
    protected void preCreate(Wallet entity) {
        if (entity.getUser() == null) {
            if (getAuthenticatedUser().isUser()) {
                User user = userService.getById((Long) getAuthenticatedUser().getId());
                entity.setUser(user);
            } else {
                throw new IllegalStateException("User must be define!!");
            }
        }
        normalize(entity);
    }

    public Wallet create(Wallet wallet) {
        return super.create(wallet);
    }

    @Override
    protected void normalize(Wallet entity) {
        entity.setAmount(BigDecimal.ZERO);
    }

    @Override
    protected Wallet preUpdate(Wallet pEntity, Wallet entity) {
        throw new UnsupportedOperationException("Update wallet not supported yet!");
    }

    public Wallet update(Wallet wallet, Long id) {
        wallet.setId(id);
        return super.update(wallet);
    }

    @Transactional
    public void operation(WalletOperationDto dto) {
        String operation = dto.operation.toLowerCase();
        if (Wallet.ADD.equals(operation)) {
            add(dto.addId, dto.amount);
        } else if (Wallet.MINUS.equals(operation)) {
            add(dto.minusId, dto.amount);
        } else if (Wallet.TRASACTION.equals(operation)) {
            transaction(dto.addId, dto.minusId, dto.amount);
        } else {
            throw new UnsupportedOperationException("Unsupported operation!");
        }
    }

    @Transactional
    public void transaction(Long addId, Long minusId, BigDecimal amount) {
        minus(minusId, amount);
        add(addId, amount);
    }

    @Transactional
    public void add(Long id, BigDecimal amount) {
        Wallet wallet = getByUserId(id);
        wallet.setAmount(wallet.getAmount().add(amount));
        // event
        walletRepository.save(wallet);
    }

    @Transactional
    public void minus(Long id, BigDecimal amount) {
        Wallet wallet = getByUserId(id);
        if (wallet.getAmount().compareTo(amount) < 0) {
            throw new AmountNotEnoughException(String.format("Amount for walletId %s not enough!", id));
        }
        wallet.setAmount(wallet.getAmount().subtract(amount));
        // event
        walletRepository.save(wallet);
    }

    @Transactional(readOnly = true)
    public Wallet getById(Long id) {
        return super.getById(id);
    }

    @Transactional(readOnly = true)
    public Wallet getByUserId(Long userId) {
        return walletRepository.findByUserId(userId);
    }
}
