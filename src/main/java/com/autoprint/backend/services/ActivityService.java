package com.autoprint.backend.services;

import com.autoprint.backend.api.dto.ReportDataDto;
import com.autoprint.backend.api.mapper.ActivityMapper;
import com.autoprint.backend.entity.Activity;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.repo.ActivityRepository;
import com.autoprint.backend.utils.SecureUtils;
import com.autoprint.backend.utils.TimeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class ActivityService extends BaseEntityService<Activity, Long> {

    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private ActivityMapper activityMapper;

    @Override
    protected PagingAndSortingRepository<Activity, Long> getRepository() {
        return activityRepository;
    }

    @Override
    protected void preCreate(Activity entity) {
        if (entity.getUser() == null) {
            if (getAuthenticatedUser().isUser() || getAuthenticatedUser().isVendor()) {
                User user = userService.getById((Long) getAuthenticatedUser().getId());
                entity.setUser(user);
            } else {
                throw new IllegalStateException("User must be define!!");
            }
        }

        String code = String.format("R-%s", SecureUtils.generateTokenAlphaAlphabet(8));
        entity.setReferenceId(code);

        normalize(entity);
    }

    @Transactional
    public Activity create(Activity activity) {
        return super.create(activity);
    }

    @Override
    protected void normalize(Activity entity) {
        if (entity.getType() == null) {
            entity.setType(Activity.Type.REPORT);
        }

        if (entity.getType() == Activity.Type.REPORT) {
            entity.setResolve(false);
        }
    }

    public Activity update(Activity activity, Long id) {
        activity.setId(id);
        return super.update(activity);
    }

    @Override
    protected Activity preUpdate(Activity pEntity, Activity entity) {

        if (isChanged(pEntity.getResolve(), entity.getResolve())) {
            pEntity.setResolve(entity.getResolve());
        }
        return pEntity;
    }

    @Transactional
    public Activity response(Activity activity) {
        activity.setType(Activity.Type.NOTIFICATION);
        return create(activity);
    }

    @Override
    public Activity getById(Long id) {
        return super.getById(id);
    }

    @Transactional(readOnly = true)
    public Page<Activity> getByUserId(Long id, Pageable pageable) {
        return activityRepository.findByUserIdAndType(id, Activity.Type.NOTIFICATION, pageable);
    }

    @Transactional(readOnly = true)
    public Page<Activity> get(Specification specification, Pageable pageable) {
        return activityRepository.findAll(specification, pageable);
    }

    public Activity createActivityTemplate(Long userId, Activity.Type type, String title, String content) {
        return this.create(activityMapper.template(userId, type, title, content));
    }

    public ReportDataDto getReportData() {

        LocalDateTime now = TimeUtils.getStartOfMonth();
        LocalDateTime lastMonth = TimeUtils.getLastMonth();

        Long totalLastMonthReport = activityRepository.countReportByLastMonths(lastMonth, now, Activity.Type.REPORT);
        Long totalUnresolveReport = activityRepository.countresolveReport(Activity.Type.REPORT);

        return activityMapper.mapReportDataDto(totalLastMonthReport, totalUnresolveReport);
    }

}
