package com.autoprint.backend.services;

import com.autoprint.backend.entity.Invoice;
import com.autoprint.backend.entity.InvoiceItem;
import com.autoprint.backend.repo.InvoiceItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class InvoiceItemService extends BaseEntityService<InvoiceItem, Long> {


    @Autowired
    private InvoiceItemRepository invoiceItemRepository;

    @Override
    protected PagingAndSortingRepository getRepository() {
        return invoiceItemRepository;
    }

    @Override
    public InvoiceItem create(InvoiceItem invoiceItem) {
        return super.create(invoiceItem);
    }

    @Override
    protected void preCreate(InvoiceItem entity) {
        failIfNull(entity.getProduct(), "product");
        failIfNull(entity.getInvoice(), "invoice");
        failIfNull(entity.getAmount(), "amount");
    }

    @Override
    protected void normalize(InvoiceItem entity) {

    }

    @Override
    protected InvoiceItem preUpdate(InvoiceItem pEntity, InvoiceItem entity) {
        throw new UnsupportedOperationException("Update operation not supported yet!");
    }
}
