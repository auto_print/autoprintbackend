package com.autoprint.backend;

import com.autoprint.backend.ezeep.EzeepService;
import com.autoprint.backend.ezeep.NotificationUtils;
import com.autoprint.backend.ezeep.services.EzeepExternalDocumentService;
import com.autoprint.backend.ezeep.services.EzeepExternalPrinterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Component
public class ContextStartedListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private EzeepService ezeepService;
    @Autowired
    private EzeepExternalPrinterService ezeepExternalPrinterService;
    @Autowired
    private EzeepExternalDocumentService ezeepExternalDocumentService;
    @Autowired
    private AppConfig appConfig;

    @Transactional
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        if (appConfig.getAppModeProd().equals(appConfig.getAppMode())) {
            try {
                ezeepService.setAccessToken();
                //ezeepPrinterService.getPrinterById("f51546de-605f-4a39-ad40-2e054594c70e");
                //ezeepDocumentService.create("Test document");
                //ezeepExternalDocumentService.upload("");
            } catch (IOException e) {
                System.out.println("Ezeep auth failed to retrieved!");
            }
        } else {
            NotificationUtils.EzeepDisableNotification(this.getClass());
        }

        if (appConfig.getAppModeDev().equals(appConfig.getAppMode())) {
            NotificationUtils.S3DisableNotification(this.getClass());
        }
    }
}
