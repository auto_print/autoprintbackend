package com.autoprint.backend.exception;

public class AmountNotEnoughException extends RuntimeException {
    public AmountNotEnoughException(String msg) {
        super(msg);
    }
}
