package com.autoprint.backend.exception;

import javax.persistence.EntityExistsException;

public class EmailExistException extends EntityExistsException {

    public EmailExistException(String msg) {
        super(msg);
    }
}
