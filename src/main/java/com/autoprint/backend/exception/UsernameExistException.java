package com.autoprint.backend.exception;

import javax.persistence.EntityExistsException;

public class UsernameExistException extends EntityExistsException {

    public UsernameExistException(String msg) {
        super(msg);
    }
}
