package com.autoprint.backend.exception;

public class EzeepFailedException extends RuntimeException {
    public EzeepFailedException(String msg) {
        super(msg);
    }
}
