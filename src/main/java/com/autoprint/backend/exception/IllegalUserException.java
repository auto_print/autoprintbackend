package com.autoprint.backend.exception;

public class IllegalUserException extends RuntimeException {

    public IllegalUserException(String msg) {
        super(msg);
    }
}
