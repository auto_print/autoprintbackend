package com.autoprint.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;

import java.util.HashMap;
import java.util.Map;

public enum ResponseErrorCode {

    //Errors for Authentication (401)

    //Errors for Authorization/Permission (403)

    //Errors for Access Denied (403)
    ACCESS_DENIED(10101, HttpStatus.FORBIDDEN),
    PERMISSION_DENIED(10102, HttpStatus.FORBIDDEN),
    ILLEGAL_ACCESS(10102, HttpStatus.FORBIDDEN);

    HttpStatus httpStatus;
    int code;

    ResponseErrorCode(int code, HttpStatus status) {
        this.code = code;
        this.httpStatus = status;
    }

    public static ResponseErrorCode valueOf(int code) {
        ResponseErrorCode[] values = values();
        for (ResponseErrorCode value : values) {
            if (value.code == code) {
                return value;
            }
        }
        throw new IllegalArgumentException(ErrorConstants.VALUE_FOR_THIS_CODE_IS_NOT_AVAILABLE);
    }

    /**
     * This helper method is used to prepare exception error code map
     *
     * @return
     */
    public static Map<String, ResponseErrorCode> getExceptionErrorCodes() {
        Map<String, ResponseErrorCode> responseErrorCodeMap = new HashMap<>();
        responseErrorCodeMap.put(AccessDeniedException.class.getName(), ResponseErrorCode.ACCESS_DENIED);
        responseErrorCodeMap.put(PermissionDeniedException.class.getName(), ResponseErrorCode.PERMISSION_DENIED);
        responseErrorCodeMap.put(IllegalAccessException.class.getName(), ResponseErrorCode.ILLEGAL_ACCESS);

        return responseErrorCodeMap;
    }

    public int getCode() {
        return code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
