package com.autoprint.backend.exception;

public class ExternalAPIException extends RuntimeException {

    private final String rawAPIResponse;

    public ExternalAPIException(String msg, String rawAPIResponse) {
        super(msg);
        this.rawAPIResponse = rawAPIResponse;
    }

    public String getRawAPIResponse() {
        return rawAPIResponse;
    }
}
