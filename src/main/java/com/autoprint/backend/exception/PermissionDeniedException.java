package com.autoprint.backend.exception;

public class PermissionDeniedException extends RuntimeException {
    public PermissionDeniedException(String msg) {
        super(msg);
    }
}
