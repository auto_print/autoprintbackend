package com.autoprint.backend.api_test.unit_test;

import com.autoprint.backend.api.dto.*;
import com.autoprint.backend.api_test.BaseAPIIntegrationTest;
import com.autoprint.backend.entity.EmailVerification;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.fixture.UserBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.HttpStatus.OK;

public class UserAPITest extends BaseAPIIntegrationTest {

    @Before
    public void setup() {
        headers.add(AUTHORIZATION, getWebAdmin().getAuthToken());
    }

    @Test
    public void createTest() {
        UserCreateDto createDto = new UserBuilder().buildCreateDto();
        ResponseEntity<UserDto> response = restTemplate.exchange("/user", POST, new HttpEntity(createDto, headers), UserDto.class);
        assertEquals(OK, response.getStatusCode());
    }

    @Test
    public void updateTest() {

        UserCreateDto createDto = new UserBuilder().buildCreateDto();
        ResponseEntity<UserDto> response1 = restTemplate.exchange("/user", POST, new HttpEntity(createDto, headers), UserDto.class);
        assertEquals(OK, response1.getStatusCode());
        UserDto u1 = response1.getBody();

        // clear auth first!
        headers.remove(AUTHORIZATION);
        assert u1 != null;
        headers.add(AUTHORIZATION, u1.token);
        UserUpdateDto updateDto = new UserBuilder().buildUpdateDto();
        ResponseEntity<UserDto> response2 = restTemplate.exchange("/user/" + u1.id, PUT, new HttpEntity(updateDto, headers), UserDto.class);
        assertEquals(OK, response2.getStatusCode());
        u1 = response2.getBody();

        // assert
        assert u1 != null;
        assertEquals(updateDto.fullname, u1.fullname);
    }

    @Test
    public void forgotPassword() {
        UserCreateDto createDto = new UserBuilder().buildCreateDto();
        ResponseEntity<UserDto> response1 = restTemplate.exchange("/user", POST, new HttpEntity(createDto, headers), UserDto.class);
        assertEquals(OK, response1.getStatusCode());

        UserDto ud1 = response1.getBody();

        ForgotPasswordRequestDto request1 = new ForgotPasswordRequestDto();
        assert ud1 != null;
        request1.email = ud1.email;
        ResponseEntity<Void> response2 = restTemplate.exchange("/user/forget/password", POST, new HttpEntity(request1, headers), Void.class);
        assertEquals(OK, response2.getStatusCode());

        User dummyUser = new User();
        dummyUser.setId(ud1.id);
        EmailVerification emailVerification = emailVerificationRepository.findFirstByUserAndVerifiedOrderByDateCreatedDesc(dummyUser, false);

        VerifyCodeRequestDto request2 = new VerifyCodeRequestDto();
        request2.code = emailVerification.getCode();

        ResponseEntity<VerifyCodeResponseDto> response3 = restTemplate.exchange("/user/verify/code", POST, new HttpEntity(request2, headers), VerifyCodeResponseDto.class);
        assertEquals(OK, response3.getStatusCode());

        assertEquals(true, Objects.requireNonNull(response3.getBody()).verified);

        NewPasswordRequestDto request3 = new NewPasswordRequestDto();
        request3.newPassword = "newpassword";

        ResponseEntity<UserDto> response4 = restTemplate.exchange("/user/" + response3.getBody().userId + "/new/password", POST, new HttpEntity(request3, headers), UserDto.class);
        assertEquals(OK, response4.getStatusCode());

        LoginRequestDto request4 = new LoginRequestDto();
        request4.username = ud1.username;
        request4.password = "newpassword";
        ResponseEntity<LoginResponseDto> response5 = restTemplate.exchange("/login", POST, new HttpEntity(request4, headers), LoginResponseDto.class);
        assertEquals(OK, response5.getStatusCode());

        assertEquals(ud1.id.toString(), Objects.requireNonNull(response5.getBody()).id);
    }
}
