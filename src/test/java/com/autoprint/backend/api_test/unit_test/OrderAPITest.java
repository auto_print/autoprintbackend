package com.autoprint.backend.api_test.unit_test;

import com.autoprint.backend.api.dto.OrderCreateDto;
import com.autoprint.backend.api.dto.OrderDto;
import com.autoprint.backend.api.dto.UserCreateDto;
import com.autoprint.backend.api.dto.UserDto;
import com.autoprint.backend.api_test.BaseAPIIntegrationTest;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.fixture.OrderBuilder;
import com.autoprint.backend.fixture.UserBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.OK;

public class OrderAPITest extends BaseAPIIntegrationTest {

    private User u1;

    @Before
    public void setup() {
        u1 = UserBuilder.sample().build();
        u1 = userRepository.save(u1);
    }

    @Test
    public void createOrder() {
        headers.add(AUTHORIZATION, u1.getAuthToken());

        // create order
        OrderCreateDto createDto = OrderBuilder.sample().buildCreateDto();
        ResponseEntity<OrderDto> response = restTemplate.exchange("/order", POST, new HttpEntity(createDto, headers), OrderDto.class);
        assertEquals(OK, response.getStatusCode());
    }
}
