package com.autoprint.backend.api_test.unit_test;

import com.autoprint.backend.api.dto.*;
import com.autoprint.backend.api_test.BaseAPIIntegrationTest;
import com.autoprint.backend.fixture.VendorBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.HttpStatus.OK;

public class VendorAPITest extends BaseAPIIntegrationTest {

    @Before
    public void setup() {
        headers.add(AUTHORIZATION, getWebAdmin().getAuthToken());
    }

    @Test
    public void createTest() {
        VendorCreateDto createDto = new VendorBuilder().buildCreateDto();
        ResponseEntity<VendorDto> response = restTemplate.exchange("/vendor", POST, new HttpEntity(createDto, headers), VendorDto.class);
        assertEquals(OK, response.getStatusCode());
    }

    @Test
    public void updateTest() {

        VendorCreateDto createDto = new VendorBuilder().buildCreateDto();
        ResponseEntity<VendorDto> response1 = restTemplate.exchange("/vendor", POST, new HttpEntity(createDto, headers), VendorDto.class);
        assertEquals(OK, response1.getStatusCode());
        VendorDto v1 = response1.getBody();

        // clear auth first!
        headers.remove(AUTHORIZATION);
        headers.add(AUTHORIZATION, v1.user.token);
        VendorUpdateDto updateDto = new VendorBuilder().buildUpdateDto();
        ResponseEntity<VendorDto> response2 = restTemplate.exchange("/vendor/" + v1.id, PUT, new HttpEntity(updateDto, headers), VendorDto.class);
        assertEquals(OK, response2.getStatusCode());
        v1 = response2.getBody();

        // assert
        assert v1 != null;
        assertEquals(updateDto.vendorname, v1.vendorname);
    }
}
