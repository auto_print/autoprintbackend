package com.autoprint.backend.api_test.unit_test;

import com.autoprint.backend.api.dto.UserCreateDto;
import com.autoprint.backend.api.dto.UserDto;
import com.autoprint.backend.api.dto.WalletOperationDto;
import com.autoprint.backend.api_test.BaseAPIIntegrationTest;
import com.autoprint.backend.fixture.UserBuilder;
import com.autoprint.backend.fixture.WalletBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.OK;

public class WalletTest extends BaseAPIIntegrationTest {

    private UserDto ud1;

    @Before
    public void createTest() {

        headers.add(AUTHORIZATION, getWebAdmin().getAuthToken());

        UserCreateDto createDto = new UserBuilder().buildCreateDto();
        ResponseEntity<UserDto> response = restTemplate.exchange("/user", POST, new HttpEntity(createDto, headers), UserDto.class);
        ud1 = response.getBody();
    }

    @Test
    public void addTest() {

        headers.set(AUTHORIZATION, ud1.token);

        WalletOperationDto operationDto = WalletBuilder.sample().setAddId(ud1.id).setAmount(BigDecimal.valueOf(10.00)).buildOperationDto();
        ResponseEntity<?> response = restTemplate.exchange("/wallet/operation", POST, new HttpEntity(operationDto, headers), Void.class);
        assertEquals(OK, response.getStatusCode());
    }

    @Test
    public void minusTest() {

        headers.set(AUTHORIZATION, ud1.token);

        WalletOperationDto operationDto1 = WalletBuilder.sample().setAddId(ud1.id).setAmount(BigDecimal.valueOf(10.00)).buildOperationDto();
        ResponseEntity<?> response1 = restTemplate.exchange("/wallet/operation", POST, new HttpEntity(operationDto1, headers), Void.class);
        assertEquals(OK, response1.getStatusCode());

        WalletOperationDto operationDto2 = WalletBuilder.sample().setMinusId(ud1.id).setAmount(BigDecimal.valueOf(5.00)).buildOperationDto();
        ResponseEntity<?> response2 = restTemplate.exchange("/wallet/operation", POST, new HttpEntity(operationDto2, headers), Void.class);
        assertEquals(OK, response2.getStatusCode());
    }
}
