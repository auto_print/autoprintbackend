package com.autoprint.backend.api_test.unit_test;

import com.autoprint.backend.api.dto.ProductVendorCreateDto;
import com.autoprint.backend.api.dto.ProductVendorDto;
import com.autoprint.backend.api.dto.VendorCreateDto;
import com.autoprint.backend.api.dto.VendorDto;
import com.autoprint.backend.api_test.BaseAPIIntegrationTest;
import com.autoprint.backend.entity.ProductVendor;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.entity.Vendor;
import com.autoprint.backend.fixture.ProductVendorBuilder;
import com.autoprint.backend.fixture.UserBuilder;
import com.autoprint.backend.fixture.VendorBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.OK;

public class ProductVendorAPITest extends BaseAPIIntegrationTest {

    private User u1;
    private Vendor v1;

    @Before
    public void setup() {
        u1 = UserBuilder.sample().build();
        u1 = userRepository.save(u1);

        v1 = VendorBuilder.sample().withUser(u1).build();
        v1 = vendorRepository.save(v1);
        headers.add(AUTHORIZATION, u1.getAuthToken());
    }

    @Test
    public void createTest() {
        ProductVendorCreateDto createDto = ProductVendorBuilder.sample().withVendor(v1.getId()).buildCreate();
        ResponseEntity<ProductVendorDto> response = restTemplate.exchange("/product/vendor", POST, new HttpEntity(createDto, headers), ProductVendorDto.class);
        assertEquals(OK, response.getStatusCode());
    }
}
