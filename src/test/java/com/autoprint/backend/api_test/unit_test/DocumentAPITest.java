package com.autoprint.backend.api_test.unit_test;

import com.autoprint.backend.api.dto.DocumentCreateDto;
import com.autoprint.backend.api.dto.DocumentDto;
import com.autoprint.backend.api_test.BaseAPIIntegrationTest;
import com.autoprint.backend.entity.Order;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.fixture.DocumentBuilder;
import com.autoprint.backend.fixture.OrderBuilder;
import com.autoprint.backend.fixture.UserBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.OK;

public class DocumentAPITest extends BaseAPIIntegrationTest {

    private User u1;
    private Order o1;

    @Before
    public void setup() {
        // user
        u1 = UserBuilder.sample().build();
        u1 = userRepository.save(u1);

        // order
        o1 = OrderBuilder.sample().withUser(u1).build();
        o1 = orderRepository.save(o1);
    }

    @Test
    public void createOrder() {
        headers.add(AUTHORIZATION, u1.getAuthToken());

        // create document
        DocumentCreateDto createDto = DocumentBuilder.sample().withOrder(o1.getId()).buildCreateDto();
        ResponseEntity<DocumentDto> response = restTemplate.exchange("/document", POST, new HttpEntity(createDto, headers), DocumentDto.class);
        assertEquals(OK, response.getStatusCode());
    }
}
