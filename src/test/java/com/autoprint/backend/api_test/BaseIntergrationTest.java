package com.autoprint.backend.api_test;

import com.autoprint.backend.config.SystemUserConfig;
import com.autoprint.backend.entity.EmailVerification;
import com.autoprint.backend.repo.*;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@Rollback
@RunWith(SpringRunner.class)
@TestPropertySource("classpath:application-test.properties")
public abstract class BaseIntergrationTest {

    // repo
    @Autowired
    protected AdminRepository adminRepository;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected VendorRepository vendorRepository;
    @Autowired
    protected OrderRepository orderRepository;
    @Autowired
    protected EmailVerificationRepository emailVerificationRepository;


    @Autowired
    private SystemUserConfig systemUserConfig;

    @Before
    public void _setupBefore() {
        systemUserConfig.init();
    }
}
