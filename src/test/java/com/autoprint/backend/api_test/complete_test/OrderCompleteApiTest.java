package com.autoprint.backend.api_test.complete_test;

import com.autoprint.backend.api.dto.*;
import com.autoprint.backend.api_test.BaseAPIIntegrationTest;
import com.autoprint.backend.fixture.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.OK;

public class OrderCompleteApiTest extends BaseAPIIntegrationTest {

    private UserDto ud1;
    private VendorDto vd1;
    private OrderDto orderDto;
    private BigDecimal amount;

    @Before
    public void setup() {

        headers.add(AUTHORIZATION, getWebAdmin().getAuthToken());

        // create user
        UserCreateDto createDto1 = new UserBuilder().buildCreateDto();
        ResponseEntity<UserDto> response1 = restTemplate.exchange("/user", POST, new HttpEntity(createDto1, headers), UserDto.class);
        ud1 = response1.getBody();

        // create vendor
        VendorCreateDto createDto2 = new VendorBuilder().buildCreateDto();
        ResponseEntity<VendorDto> response2 = restTemplate.exchange("/vendor", POST, new HttpEntity(createDto2, headers), VendorDto.class);
        vd1 = response2.getBody();

        headers.set(AUTHORIZATION, vd1.user.token);

        // create printer
        PrinterCreateDto createDto3 = PrinterBuilder.sample().withVendor(vd1.id).buildCreate();
        ResponseEntity<PrinterDto> response3 = restTemplate.exchange("/printer", POST, new HttpEntity(createDto3, headers), PrinterDto.class);

        // create product
        ProductVendorCreateDto createDto4 = ProductVendorBuilder.sample().withVendor(vd1.id).buildCreate();
        ResponseEntity<ProductVendorDto> response4 = restTemplate.exchange("/product/vendor", POST, new HttpEntity(createDto4, headers), ProductVendorDto.class);

        headers.set(AUTHORIZATION, ud1.token);

        // create order
        OrderCreateDto createDto5 = OrderBuilder.sample().buildCreateDto();
        ResponseEntity<OrderDto> response5 = restTemplate.exchange("/order", POST, new HttpEntity(createDto5, headers), OrderDto.class);
        orderDto = response5.getBody();

        // create document
        DocumentCreateDto createDto6 = DocumentBuilder.sample().withOrder(orderDto.id).buildCreateDto();
        ResponseEntity<DocumentDto> response6 = restTemplate.exchange("/document", POST, new HttpEntity(createDto6, headers), DocumentDto.class);
    }

    @Test
    public void preconfirmTest() {
        // preconfim
        ResponseEntity<OrderPreConfirmDto> response = restTemplate.exchange("/order/" + orderDto.id + "/pre/confirm", POST, new HttpEntity(headers), OrderPreConfirmDto.class);
        assertEquals(OK, response.getStatusCode());
        amount = Objects.requireNonNull(response.getBody()).amount;
    }

    @Test
    public void confirmTest() {
        preconfirmTest();
        // confirm
        ResponseEntity<OrderDto> response2 = restTemplate.exchange("/order/" + orderDto.id + "/confirm?accept=true", POST, new HttpEntity(headers), OrderDto.class);
        assertEquals(OK, response2.getStatusCode());
    }

    @Test
    public void payTest() {
        confirmTest();
        topupHelper();
        // pay
        ResponseEntity<?> response4 = restTemplate.exchange("/order/" + orderDto.id + "/pay?amount=" + amount, POST, new HttpEntity(headers), Void.class);
        assertEquals(OK, response4.getStatusCode());
    }

    @Test
    public void cancelTest() {
        payTest();

        ResponseEntity<?> response5 = restTemplate.exchange("/order/" + orderDto.id + "/cancel", POST, new HttpEntity(headers), Void.class);
        assertEquals(OK, response5.getStatusCode());
    }

    @Test
    public void reorder() {
        cancelTest();

        ResponseEntity<OrderDto> response6 = restTemplate.exchange("/order/" + orderDto.id + "/reorder", POST, new HttpEntity(headers), OrderDto.class);
        assertEquals(OK, response6.getStatusCode());
    }

    private void topupHelper() {
        // topup
        WalletOperationDto operationDto = WalletBuilder.sample().setAddId(ud1.id).setAmount(BigDecimal.valueOf(10.00)).buildOperationDto();
        ResponseEntity<?> response3 = restTemplate.exchange("/wallet/operation", POST, new HttpEntity(operationDto, headers), Void.class);
        assertEquals(OK, response3.getStatusCode());
    }
}
