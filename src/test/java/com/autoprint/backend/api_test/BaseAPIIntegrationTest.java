package com.autoprint.backend.api_test;

import com.autoprint.backend.entity.Admin;
import com.autoprint.backend.entity.SystemUser;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public abstract class BaseAPIIntegrationTest extends BaseIntergrationTest {

    @Autowired
    protected TestRestTemplate restTemplate;

    protected HttpHeaders headers;
    protected Admin superadmin;

    @Before
    public void _setupBefore() {
        headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        // todo: fix?
        superadmin = adminRepository.findById(1L).orElseThrow(IllegalStateException::new);

        SystemUser.getInstance().runAs();
    }

    protected Admin getWebAdmin() {
        return adminRepository.findByUsername("web");
    }
}
