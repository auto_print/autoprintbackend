package com.autoprint.backend.fixture;

import com.autoprint.backend.api.dto.UserCreateDto;
import com.autoprint.backend.api.dto.WalletOperationDto;
import com.autoprint.backend.entity.Wallet;

import java.math.BigDecimal;

public class WalletBuilder {

    private Long id = 0L;
    private Long addId;
    private Long minusId;
    private BigDecimal amount;

    public static WalletBuilder sample() {
        return new WalletBuilder();
    }

    public WalletOperationDto buildOperationDto() {

        if (amount == null) {
            throw new IllegalStateException("Must have amount to update wallet!");
        }

        WalletOperationDto walletOperationDto = new WalletOperationDto();
        walletOperationDto.amount = amount;

        if (addId != null && minusId != null) {
            walletOperationDto.addId = addId;
            walletOperationDto.minusId = minusId;
            walletOperationDto.operation = Wallet.TRASACTION;
        } else if (addId != null) {
            walletOperationDto.addId = addId;
            walletOperationDto.operation = Wallet.ADD;
        } else if (minusId != null) {
            walletOperationDto.minusId = minusId;
            walletOperationDto.operation = Wallet.MINUS;
        } else {
            throw new IllegalStateException("Must have operation to update wallet!");
        }
        return walletOperationDto;
    }

    public WalletBuilder setAddId(Long addId) {
        this.addId = addId;
        return this;
    }

    public WalletBuilder setMinusId(Long minusId) {
        this.minusId = minusId;
        return this;
    }

    public WalletBuilder setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }
}
