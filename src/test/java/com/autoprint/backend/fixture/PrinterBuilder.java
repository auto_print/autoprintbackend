package com.autoprint.backend.fixture;

import com.autoprint.backend.api.dto.PrinterCreateDto;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class PrinterBuilder {

    private Long id = 0L;
    private String name = randomAlphabetic(10);
    private Long vendorId;

    public static PrinterBuilder sample() {
        return new PrinterBuilder();
    }

    public PrinterCreateDto buildCreate() {

        if (vendorId == null) {
            throw new IllegalStateException("Must have vendor to build printer!");
        }

        PrinterCreateDto printerCreateDto = new PrinterCreateDto();
        printerCreateDto.name = name;
        printerCreateDto.vendorId = vendorId;

        return printerCreateDto;
    }

    public PrinterBuilder withVendor(Long vendorId) {
        this.vendorId = vendorId;
        return this;
    }
}
