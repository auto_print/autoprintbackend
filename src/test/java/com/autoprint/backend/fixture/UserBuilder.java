package com.autoprint.backend.fixture;

import com.autoprint.backend.api.dto.UserCreateDto;
import com.autoprint.backend.api.dto.UserUpdateDto;
import com.autoprint.backend.entity.User;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

public class UserBuilder {

    private Long id = 0L;
    private String username = randomAlphabetic(10);
    private String password = randomAlphabetic(10);
    private String fullname = randomAlphabetic(10);
    private String phoneNumber = "601" + randomNumeric(8);
    private String email = randomAlphabetic(10).toLowerCase() + "@" + randomAlphabetic(5).toLowerCase() + ".com";
    private String token = randomAlphabetic(10);
    private boolean isActive = true;

    private boolean isVendor = false;

    private String imageBase64 = randomAlphabetic(10);

    public static UserBuilder sample() {
        return new UserBuilder();
    }

    public User build() {
        User user = new User();
        user.setEmail(email);
        user.setFullname(fullname);
        user.setPassword(password);
        user.setUsername(username);
        user.setActive(isActive);

        if (isVendor) {
            user.setToken("VENDOR_" + token);
        } else {
            user.setToken("USER_" + token);
        }

        user.setVendor(isVendor);

        return user;
    }

    public UserCreateDto buildCreateDto() {
        UserCreateDto userCreateDto = new UserCreateDto();
        userCreateDto.email = email;
        userCreateDto.fullname = fullname;
        userCreateDto.password = password;
        userCreateDto.username = username;
        return userCreateDto;
    }

    public UserUpdateDto buildUpdateDto() {
        UserUpdateDto userUpdateDto = new UserUpdateDto();
        userUpdateDto.imageBase64 = imageBase64;
        userUpdateDto.phoneNumber = phoneNumber;
        userUpdateDto.email = email;
        userUpdateDto.fullname = fullname;
        userUpdateDto.username = username;
        return userUpdateDto;
    }

    public UserBuilder isVendor() {
        this.isVendor = true;
        return this;
    }

}
