package com.autoprint.backend.fixture;

import com.autoprint.backend.api.dto.ProductVendorCreateDto;

import java.math.BigDecimal;

public class ProductVendorBuilder {

    private Long id = 0L;
    private Long vendorId;
    private Long productId = 100L;
    private BigDecimal price = BigDecimal.valueOf(0.10);

    public static ProductVendorBuilder sample() {
        return new ProductVendorBuilder();
    }

    public ProductVendorCreateDto buildCreate() {

        if (vendorId == null && productId == null) {
            throw new IllegalStateException("Must have vendor/product to build product_vendor!");
        }

        ProductVendorCreateDto productVendorCreateDto = new ProductVendorCreateDto();
        productVendorCreateDto.price = price;
        productVendorCreateDto.productId = productId;
        productVendorCreateDto.vendorId = vendorId;

        return productVendorCreateDto;
    }

    public ProductVendorBuilder withVendor(Long vendorId) {
        this.vendorId = vendorId;
        return this;
    }

    public ProductVendorBuilder withProduct(Long productId) {
        this.productId = productId;
        return this;
    }
}
