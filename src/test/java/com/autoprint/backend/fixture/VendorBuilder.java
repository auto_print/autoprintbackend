package com.autoprint.backend.fixture;

import com.autoprint.backend.api.dto.VendorCreateDto;
import com.autoprint.backend.api.dto.VendorUpdateDto;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.entity.Vendor;
import com.autoprint.backend.utils.SecureUtils;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

public class VendorBuilder {

    private Long id = 0L;
    private String username = randomAlphabetic(10);
    private String password = randomAlphabetic(10);
    private String fullname = randomAlphabetic(10);
    private String vendorname = randomAlphabetic(10);
    private Double longitude = SecureUtils.generateDecimal(0);
    private Double latitude = SecureUtils.generateDecimal(1);
    private String phoneNumber = "601" + randomNumeric(8);
    private String email = randomAlphabetic(10).toLowerCase() + "@" + randomAlphabetic(5).toLowerCase() + ".com";
    private String token = randomAlphabetic(10);
    private boolean isActive = true;

    // hack
    private User user;

    private String imageBase64 = randomAlphabetic(10);

    public static VendorBuilder sample() {
        return new VendorBuilder();
    }

    public Vendor build() {

        if (user == null) {
            throw new IllegalStateException("Must have user to build vendor!");
        }

        Vendor vendor = new Vendor();
        vendor.setActive(isActive);
        vendor.setUser(user);
        vendor.setVendorname(vendorname);
        return vendor;
    }

    public VendorCreateDto buildCreateDto() {
        VendorCreateDto vendorCreateDto = new VendorCreateDto();
        vendorCreateDto.vendorname = vendorname;
        vendorCreateDto.email = email;
        vendorCreateDto.fullname = fullname;
        vendorCreateDto.password = password;
        vendorCreateDto.username = username;
        vendorCreateDto.latitude = latitude;
        vendorCreateDto.longitude = longitude;
        return vendorCreateDto;
    }

    public VendorUpdateDto buildUpdateDto() {
        VendorUpdateDto vendorUpdateDto = new VendorUpdateDto();
        vendorUpdateDto.vendorname = vendorname;
        vendorUpdateDto.imageBase64 = imageBase64;
        vendorUpdateDto.phoneNumber = phoneNumber;
        vendorUpdateDto.email = email;
        vendorUpdateDto.fullname = fullname;
        vendorUpdateDto.username = username;
        vendorUpdateDto.longitude = longitude;
        vendorUpdateDto.latitude = latitude;
        return vendorUpdateDto;
    }

    public VendorBuilder withUser(User user) {
        this.user = user;
        return this;
    }
}
