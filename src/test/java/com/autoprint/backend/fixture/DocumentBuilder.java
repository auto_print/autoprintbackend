package com.autoprint.backend.fixture;

import com.autoprint.backend.api.dto.DocumentCreateDto;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class DocumentBuilder {

    private Long id = 0L;
    private String filename = String.format("%s.txt", randomAlphabetic(10));
    private String data = "aGVsbG8=";
    private Long orderId;

    //different section
    private String pages = "1";
    private Long copies = 1L;
    private String pageOrientation;
    private String colour;
    private String duplex;

    //extra
    private String coverType;
    private String coverColour;

    public static DocumentBuilder sample() {
        return new DocumentBuilder();
    }

    public DocumentCreateDto buildCreateDto() {

        if (orderId == null) {
            throw new IllegalStateException("Must have order to build document!");
        }

        DocumentCreateDto documentCreateDto = new DocumentCreateDto();
        documentCreateDto.filename = filename;
        documentCreateDto.data = data;
        documentCreateDto.orderId = orderId;

        documentCreateDto.pages = pages;
        documentCreateDto.copies = copies;
        documentCreateDto.pageOrientation = pageOrientation;
        documentCreateDto.colour = colour;
        documentCreateDto.duplex = duplex;

        documentCreateDto.coverType = coverType;
        documentCreateDto.coverColour = coverColour;

        return documentCreateDto;
    }

    public DocumentBuilder withOrder(Long orderId) {
        this.orderId = orderId;
        return this;
    }
}
