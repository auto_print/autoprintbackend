package com.autoprint.backend.fixture;

import com.autoprint.backend.api.dto.OrderCreateDto;
import com.autoprint.backend.entity.Order;
import com.autoprint.backend.entity.User;
import com.autoprint.backend.utils.SecureUtils;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class OrderBuilder {

    private Long id = 0L;
    private Double longitude = SecureUtils.generateDecimal(0);
    private Double latitude = SecureUtils.generateDecimal(1);
    private Order.Status status = Order.Status.PENDING;
    private User user;
    private String referenceId = randomAlphabetic(10);

    public static OrderBuilder sample() {
        return new OrderBuilder();
    }

    public Order build() {
        if (user == null) {
            throw new IllegalStateException("Must have user to build order!");
        }
        Order order = new Order();
        order.setLongitude(longitude);
        order.setLatitude(latitude);
        order.setStatus(status);
        order.setUser(user);
        order.setReferenceId(referenceId);
        return order;
    }

    public OrderCreateDto buildCreateDto() {
        OrderCreateDto orderCreateDto = new OrderCreateDto();
        orderCreateDto.latitude = latitude;
        orderCreateDto.longitude = longitude;
        return orderCreateDto;
    }

    public OrderBuilder withUser(User user) {
        this.user = user;
        return this;
    }
}
