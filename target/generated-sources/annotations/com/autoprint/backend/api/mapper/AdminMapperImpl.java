package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.AdminCreateDto;
import com.autoprint.backend.api.dto.AdminDto;
import com.autoprint.backend.entity.Admin;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-10T20:01:12+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 11.0.5 (JetBrains s.r.o)"
)
@Component
public class AdminMapperImpl implements AdminMapper {

    @Override
    public Admin toAdmin(AdminCreateDto dto) {

        Admin admin = new Admin();

        if ( dto != null ) {
            admin.setUsername( dto.username );
            admin.setPassword( dto.password );
        }

        return admin;
    }

    @Override
    public Admin toAdmin(AdminDto dto) {

        Admin admin = new Admin();

        if ( dto != null ) {
            admin.setId( dto.id );
            admin.setUsername( dto.username );
            admin.setPassword( dto.password );
            admin.setToken( dto.token );
        }

        return admin;
    }

    @Override
    public AdminDto toAdminDto(Admin dto) {

        AdminDto adminDto = new AdminDto();

        if ( dto != null ) {
            adminDto.id = dto.getId();
            adminDto.username = dto.getUsername();
            adminDto.password = dto.getPassword();
            adminDto.token = dto.getToken();
        }

        return adminDto;
    }
}
