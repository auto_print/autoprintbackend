package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.VendorDto;
import com.autoprint.backend.entity.Vendor;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-10T20:01:12+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 11.0.5 (JetBrains s.r.o)"
)
@Component
public class VendorMapperImpl implements VendorMapper {

    @Autowired
    private UserMapper userMapper;

    @Override
    public Vendor toVendor(VendorDto vendorDto) {

        Vendor vendor = new Vendor();

        if ( vendorDto != null ) {
            vendor.setId( vendorDto.id );
            vendor.setUser( userMapper.toUser( vendorDto.user ) );
            vendor.setVendorname( vendorDto.vendorname );
            vendor.setLongitude( vendorDto.longitude );
            vendor.setLatitude( vendorDto.latitude );
        }

        return vendor;
    }

    @Override
    public VendorDto toVendorDto(Vendor vendor) {

        VendorDto vendorDto = new VendorDto();

        if ( vendor != null ) {
            vendorDto.id = vendor.getId();
            vendorDto.vendorname = vendor.getVendorname();
            vendorDto.user = userMapper.toUserDto( vendor.getUser() );
            vendorDto.longitude = vendor.getLongitude();
            vendorDto.latitude = vendor.getLatitude();
        }

        return vendorDto;
    }
}
