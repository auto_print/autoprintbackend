package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.PrinterCreateDto;
import com.autoprint.backend.api.dto.PrinterDto;
import com.autoprint.backend.entity.Printer;
import com.autoprint.backend.entity.Vendor;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-10T20:01:12+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 11.0.5 (JetBrains s.r.o)"
)
@Component
public class PrinterMapperImpl implements PrinterMapper {

    @Override
    public Printer toPrinter(PrinterCreateDto printerCreateDto) {

        Printer printer = new Printer();

        if ( printerCreateDto != null ) {
            printer.setVendor( printerCreateDtoToVendor( printerCreateDto ) );
            printer.setName( printerCreateDto.name );
        }

        return printer;
    }

    @Override
    public Printer toPrinter(PrinterDto printerDto) {

        Printer printer = new Printer();

        if ( printerDto != null ) {
            printer.setId( printerDto.id );
            printer.setName( printerDto.name );
            printer.setStatus( printerDto.status );
        }

        return printer;
    }

    @Override
    public PrinterDto toPrinterDto(Printer printer) {

        PrinterDto printerDto = new PrinterDto();

        if ( printer != null ) {
            printerDto.id = printer.getId();
            printerDto.name = printer.getName();
            printerDto.status = printer.getStatus();
        }

        return printerDto;
    }

    protected Vendor printerCreateDtoToVendor(PrinterCreateDto printerCreateDto) {

        Vendor vendor = new Vendor();

        if ( printerCreateDto != null ) {
            vendor.setId( printerCreateDto.vendorId );
        }

        return vendor;
    }
}
