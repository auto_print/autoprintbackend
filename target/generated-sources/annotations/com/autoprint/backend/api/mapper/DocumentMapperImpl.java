package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.DocumentCreateDto;
import com.autoprint.backend.api.dto.DocumentDto;
import com.autoprint.backend.entity.Document;
import com.autoprint.backend.entity.Order;
import com.autoprint.backend.entity.Product;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-10T20:01:12+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 11.0.5 (JetBrains s.r.o)"
)
@Component
public class DocumentMapperImpl implements DocumentMapper {

    @Override
    public Document toDocument(DocumentCreateDto documenCreatetDto) {

        Document document = new Document();

        if ( documenCreatetDto != null ) {
            document.setProduct( documentCreateDtoToProduct( documenCreatetDto ) );
            document.setOrder( documentCreateDtoToOrder( documenCreatetDto ) );
            document.setFilename( documenCreatetDto.filename );
            document.setFiletype( documenCreatetDto.filetype );
            document.setPages( documenCreatetDto.pages );
            document.setData( documenCreatetDto.data );
        }

        return document;
    }

    @Override
    public Document toDocument(DocumentDto documentDto) {

        Document document = new Document();

        if ( documentDto != null ) {
            document.setId( documentDto.id );
            document.setFilename( documentDto.filename );
            document.setFiletype( documentDto.filetype );
            document.setPages( documentDto.pages );
        }

        return document;
    }

    @Override
    public DocumentDto toDocumentDto(Document document) {

        DocumentDto documentDto = new DocumentDto();

        if ( document != null ) {
            documentDto.id = document.getId();
            documentDto.filename = document.getFilename();
            documentDto.filetype = document.getFiletype();
            documentDto.pages = document.getPages();
        }

        return documentDto;
    }

    protected Product documentCreateDtoToProduct(DocumentCreateDto documentCreateDto) {

        Product product = new Product();

        if ( documentCreateDto != null ) {
            product.setId( documentCreateDto.productId );
        }

        return product;
    }

    protected Order documentCreateDtoToOrder(DocumentCreateDto documentCreateDto) {

        Order order = new Order();

        if ( documentCreateDto != null ) {
            order.setId( documentCreateDto.orderId );
        }

        return order;
    }
}
