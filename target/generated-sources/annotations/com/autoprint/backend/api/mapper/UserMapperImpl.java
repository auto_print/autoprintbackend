package com.autoprint.backend.api.mapper;

import com.autoprint.backend.api.dto.UserCreateDto;
import com.autoprint.backend.api.dto.UserDto;
import com.autoprint.backend.api.dto.UserUpdateDto;
import com.autoprint.backend.entity.User;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-10T20:01:12+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 11.0.5 (JetBrains s.r.o)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Autowired
    private WalletMapper walletMapper;

    @Override
    public User toUser(UserDto userDto) {

        User user = new User();

        if ( userDto != null ) {
            user.setId( userDto.id );
            user.setUsername( userDto.username );
            user.setFullname( userDto.fullname );
            user.setToken( userDto.token );
            user.setEmail( userDto.email );
            user.setWallet( walletMapper.toWallet( userDto.wallet ) );
        }

        return user;
    }

    @Override
    public User toUser(UserCreateDto userCreateDto) {

        User user = new User();

        if ( userCreateDto != null ) {
            user.setUsername( userCreateDto.username );
            user.setPassword( userCreateDto.password );
            user.setFullname( userCreateDto.fullname );
            user.setEmail( userCreateDto.email );
        }

        return user;
    }

    @Override
    public User toUser(UserUpdateDto userUpdateDto) {

        User user = new User();

        if ( userUpdateDto != null ) {
            user.setUsername( userUpdateDto.username );
            user.setFullname( userUpdateDto.fullname );
            user.setEmail( userUpdateDto.email );
        }

        return user;
    }

    @Override
    public UserDto toUserDto(User user) {

        UserDto userDto = new UserDto();

        if ( user != null ) {
            userDto.id = user.getId();
            userDto.username = user.getUsername();
            userDto.email = user.getEmail();
            userDto.token = user.getToken();
            userDto.fullname = user.getFullname();
            userDto.wallet = walletMapper.toWalletDto( user.getWallet() );
        }

        return userDto;
    }
}
